
#include <girt/girt.h>

typedef struct {
	girt_proxy *proxy;
	girt_prereaders *prereaders;
	girt_resolver *resolver;
	girt_filters *inbound_filters;
	girt_filters *outbound_filters;
	struct timeval *prereaders_timeout;
	struct timeval *prereaders_retry_interval;

	// datagram buffer
    pthread_key_t bufkey;
    ssize_t buflen;
} girt_local;
			
void _girt_proxy_free(girt_local *local);
static void local_state_check_callback(evutil_socket_t fd, short int what, void *arg);
static void local_end_check_callback(evutil_socket_t fd, short int what, void *arg);
static void local_event_callback(struct bufferevent *bev, short events, void *arg);
static void local_write_callback(struct bufferevent *bev, void *arg);
static void local_read_callback(struct bufferevent *bev, void *arg);
static void local_bufferevent_replace_link(struct bufferevent *bev, girt_link *link);

static void local_datagram_flow_start(girt_local *local, girt_flow *flow);

girt_local *
_girt_proxy_init(girt_proxy *proxy, UNUSED girt_cfg *proxy_config)
{
	girt_enter();
	girt_local *local = girt_malloc(sizeof(*local));
	defer { girt_if_fail { _girt_proxy_free(local); } }

	local->proxy = proxy;

	// @config <proxy:default> prereaders <prereaders>
	girt_cfg *prereaders_config = girt_config_get_table(proxy_config, "prereaders");
	defer { girt_config_free_table(prereaders_config); }
	if (girt_config_root_table_count(prereaders_config) <= 0) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "No prereaders configured\n");
	} else {
		// @config <prereaders> timeout period 5s
		// @config-desc Maximum time to wait for data to preread on a connection
		local->prereaders_timeout = girt_config_get_timeval(prereaders_config, "timeout", "5s");
		// @config <prereaders> retry-interval period 100
		// @config-desc Polling interval when prereading on a connection
		local->prereaders_retry_interval = girt_config_get_timeval(prereaders_config, "retry-interval", "100");
		local->prereaders = girt_prereaders_init(proxy, prereaders_config);
		if (local->prereaders == NULL) {
			girt_fail(NULL);
		}
	}

	// @config <proxy:default> resolver <resolver>
	// @config-desc The name resolver to use (if any)
	girt_cfg *resolver_config = girt_config_get_table(proxy_config, "resolver");
	defer { girt_config_free_table(resolver_config); }
	if (girt_config_root_table_count(resolver_config) <= 0) {
		girt_loc_print(INFO, girt_proxy_name(proxy), "No resolver configured\n");
	} else {
		// @config <resolver> type string "evdns"
		// @config-desc The type of the resolver which is used as the plug-in name
		char *type = girt_config_get_string(resolver_config, "type", "evdns");
		defer { free(type); }
		if (type != NULL) {
			local->resolver = girt_resolver_init(proxy, resolver_config, type, "orig.http.url", "dest.addr", "dest.resolver.status");
			if (local->resolver == NULL) {
				girt_fail(NULL);
			}
		}
	}

	// @config <proxy:default> filters <proxyfilters>
	girt_cfg *filters_config = girt_config_get_table(proxy_config, "filters");
	defer { girt_config_free_table(filters_config); }
	if (girt_config_root_table_count(filters_config) <= 0) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "No filters configured\n");
	} else {
		// @config <proxyfilters> in <filters>
		girt_cfg *inbound_config = girt_config_get_table(filters_config, "in");
		defer { girt_config_free_table(inbound_config); }
		if (girt_config_root_table_count(inbound_config) <= 0) {
			girt_loc_print(ERROR, girt_proxy_name(proxy), "No inbound chain for filters\n");
		} else {
			local->inbound_filters = girt_filters_init(proxy, inbound_config);
			if (local->inbound_filters == NULL) {
				girt_fail(NULL);
			}
		}

		// @config <proxyfilters> out <filters>
		girt_cfg *outbound_config = girt_config_get_table(filters_config, "out");
		defer { girt_config_free_table(outbound_config); }
		if (girt_config_root_table_count(outbound_config) <= 0) {
			girt_loc_print(ERROR, girt_proxy_name(proxy), "No outbound chain for filters\n");
		} else {
			local->outbound_filters = girt_filters_init(proxy, outbound_config);
			if (local->outbound_filters == NULL) {
				girt_fail(NULL);
			}
		}
	}

	if (girt_threadbuf_make(&local->bufkey)) {
		girt_fail(NULL);
	}

	return local;
}

void
_girt_proxy_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_prereaders_free(local->prereaders);
	free(local->prereaders_timeout);
	free(local->prereaders_retry_interval);

	girt_resolver_free(local->resolver);

	girt_filters_free(local->inbound_filters);
	girt_filters_free(local->outbound_filters);

	memset(local, 0, sizeof(*local));

	free(local);
}

girt_wrk *
_girt_proxy_workers(UNUSED girt_local *local)
{
	return NULL; // use default workers from root context
}

void
_girt_proxy_flow_start(girt_local *local, girt_flow *flow)
{
	girt_flow_attrs(put_ptr, flow, "proxy.default", local, NULL);

	if (girt_flow_attrs(get_tag, flow, "datagram")) {
		// datagrams are hard
		local_datagram_flow_start(local, flow);
	} else {
		// streams are easy
		girt_flow_attrs_notify_enable(flow, "orig.closed", GIRT_ATTR_TAG, local_end_check_callback);
		girt_flow_attrs_notify_enable(flow, "dest.closed", GIRT_ATTR_TAG, local_end_check_callback);

		girt_flow_immediate(flow, local_state_check_callback);
	}
girt_flow_dump(flow);
}

void
_girt_proxy_flow_send(UNUSED girt_local *local, girt_flow *flow, struct evbuffer *datagram)
{
	if (!girt_flow_attrs(get_tag, flow, "datagram")) {
		return;
	}

	// girt_flow_printf(ERROR, flow, "%s: %lu\n", "_girt_proxy_flow_send", evbuffer_get_length(datagram));

	// inject 'datagram' into stream input
	evbuffer_add_buffer(girt_flow_attrs(get_ptr, flow, "orig.output"), datagram);
}

static void
local_prereaders_retry_callback(evutil_socket_t fd, short int what, void *arg)
{
	local_state_check_callback(fd, what, arg);
}

static void
local_prereaders_timeout_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;

	*(int *)girt_flow_attrs(get_ptr, flow, "proxy.state") = 0xff;

	girt_flow_cancel_event_by_callback(flow, local_prereaders_retry_callback);
	girt_flow_print(INFO, flow, "Prereader timed out");
	girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Timed out", -1);
	girt_flow_attrs(set_tag, flow, "orig.closed");
}

static int
local_run_prereaders(girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;

	girt_flow_schedule(flow, "proxy.prereaders.timeout", local->prereaders_timeout, local_prereaders_timeout_callback);

	int result = girt_prereaders_peek(local->prereaders, "orig.addr.name", flow);
	girt_flow_printf(DEBUG, flow, "Preread result %d\n", result);

	if (result != GIRT_PREREADER_AGAIN) {
		girt_flow_cancel_event_by_name(flow, "proxy.prereaders.timeout");
	}

	switch (result) {
	case GIRT_PREREADER_DONE:
	case GIRT_PREREADER_FINAL:
		*state = success_state;
		loop = 1;
		break;
	case GIRT_PREREADER_AGAIN:
		girt_flow_reschedule(flow, "proxy.prereaders.retry", local->prereaders_retry_interval, local_prereaders_retry_callback);
		break;
	case GIRT_PREREADER_ERROR:
		*state = 0xff;
		girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Internal error", -1);
		girt_flow_attrs(set_tag, flow, "orig.closed");
		break;
	case GIRT_PREREADER_CLOSED:
		*state = 0xff;
		girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Client closed", -1);
		girt_flow_attrs(set_tag, flow, "orig.closed");
		break;
	}

	return loop;
}
		
static int
local_resolve_name(UNUSED girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;
	if (local->resolver != NULL && girt_flow_attrs(get_ptr, flow, "dest.addr.raw") == NULL) {
		*state = success_state;

		girt_flow_attrs_notify_enable(flow, "dest.resolver.status", GIRT_ATTR_PTR, local_state_check_callback);
		girt_resolver_resolve(local->resolver, flow);
	} else {
		*state = success_state + 1; // skip
		loop = 1;
	}

	return loop;
}

static int
local_handle_resolver_result(UNUSED girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;
	int result = 0;
	char *msg = girt_flow_attrs(get_str_int, flow, "dest.resolver.status", &result);

	if (result > 0) {
		*state = success_state;
		loop = 1;
	} else {
		*state = 0xff;
		girt_flow_printf(INFO, flow, "%s", msg);
		girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Unknown host", -1);
		girt_flow_attrs(set_tag, flow, "orig.closed");
	}

	return loop;
}

static int
local_connect_to_dest(UNUSED girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;

	*state = success_state;

	struct bufferevent *dest_bev = bufferevent_socket_new(girt_flow_event_base(flow), -1, BEV_OPT_DEFER_CALLBACKS);
	girt_flow_attrs(put_ptr, flow, "dest.bev", dest_bev, girt_bufferevent_free_pp);
	girt_link *null_link = girt_link_init(flow, "null.link", NULL, NULL, 0);
	bufferevent_setcb(dest_bev, local_read_callback, local_write_callback, local_event_callback, null_link);

	girt_flow_attrs_notify_enable(flow, "dest.connect.status", GIRT_ATTR_PTR, local_state_check_callback);

	size_t addrlen = 0;
	struct sockaddr *addr = girt_flow_attrs(get_ptr_len, flow, "dest.addr.raw", &addrlen);
	bufferevent_socket_connect(dest_bev, addr, addrlen);
	girt_flow_attrs(set_int, flow, "dest.addr.name", bufferevent_getfd(dest_bev));

	return loop;
}

static int
local_check_connect_status(UNUSED girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;

	int status = girt_flow_attrs(get_int, flow, "dest.connect.status");

	if (status > 0) {
		loop = 1;
		*state = success_state;
	} else
	if (status < 0) {
		*state = 0xff;
	} else {
		girt_flow_attrs_notify_enable(flow, "dest.connect.status", GIRT_ATTR_PTR, local_state_check_callback);
	}

	return loop;
}

static int
local_enable_connection(UNUSED girt_local *local, girt_flow *flow, int *state, int success_state)
{
	int loop = 0;
	*state = success_state;

	struct bufferevent *dest_bev = girt_flow_attrs(get_ptr, flow, "dest.bev");
	bufferevent_enable(dest_bev, EV_READ|EV_WRITE);

	struct bufferevent *orig_bev = girt_flow_attrs(get_ptr, flow, "orig.bev");
	bufferevent_enable(orig_bev, EV_READ|EV_WRITE);

	return loop;
}

static void
local_state_check_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;
	girt_local *local = girt_flow_attrs(get_ptr, flow, "proxy.default");

	int loop = 0;
	int *state = girt_flow_attrs(get_or_put_ptr, flow, "proxy.state", sizeof(int), girt_free_pp);

	do {
		loop = 0;
		int old_state = *state;
		switch (*state) {
		case 0:
			loop = local_run_prereaders(local, flow, state, 1);
			break;
		case 1:
			loop = local_resolve_name(local, flow, state, 2);
			break;
		case 2:
			loop = local_handle_resolver_result(local, flow, state, 3);
			break;
		case 3:
			loop = local_connect_to_dest(local, flow, state, 4);
			break;
		case 4:
			loop = local_check_connect_status(local, flow, state, 5);
			break;
		case 5:
			loop = local_run_prereaders(local, flow, state, 6);
			break;
		case 6:
			loop = local_enable_connection(local, flow, state, 7);
			break;
		case 7:
			break;
		case 0xff:
 			break;
		}

		girt_flow_printf(DEBUG, flow, "State was %d, now %d\n", old_state, *state);

		// Safety
		if (loop && *state == old_state) {
			girt_flow_printf(ERROR, flow, "Loop is true and state (%d) did not progress\n", *state);
			break;
		}

	} while (loop);
}

static void
local_end_check_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;
	girt_local *local = girt_flow_attrs(get_ptr, flow, "proxy.default");

	if (girt_flow_attrs(get_tag, flow, "orig.closed") &&
			(girt_flow_attrs(get_tag, flow, "dest.closed") || girt_flow_attrs(get_int, flow, "dest.connect.status") <= 0)) {
		girt_proxy_flow_free(local->proxy, flow);
	}
}

// Handle reads of both inbound and outbound and forward to the next
// 'interested party' which may be a filter or the ultimate destination.
static void
local_read_callback(struct bufferevent *bev, UNUSED void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);

	struct evbuffer *input = bufferevent_get_input(bev);
	struct evbuffer *output = girt_link_evbuf(link);

girt_flow_printf(ERROR, flow, "%s: %lu to %s\n", "local_read_callback", evbuffer_get_length(input), girt_link_name(link));

	// Count bytes
	if (bev == girt_flow_attrs(get_ptr, flow, "orig.bev")) {
		girt_flow_attrs(add_u64, flow, "orig.bytes-read", evbuffer_get_length(input));
	} else
	if (bev == girt_flow_attrs(get_ptr, flow, "dest.bev")) {
		girt_flow_attrs(add_u64, flow, "dest.bytes-read", evbuffer_get_length(input));
	}

	/* Copy all the data from the input buffer to the output buffer. */
	evbuffer_add_buffer(output, input);
}

static void
local_write_callback(UNUSED struct bufferevent *bev, UNUSED void *arg)
{
	// girt_flow *flow = arg;

	/* This callback is invoked when there is data to read on bev. */
//	struct evbuffer *input = bufferevent_get_input(bev);
//	struct evbuffer *output = bufferevent_get_output(bev);

	/* Copy all the data from the input buffer to the output buffer. */
//	evbuffer_add_buffer(output, input);
}

static void
local_event_callback(UNUSED struct bufferevent *bev, short events, void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);
	girt_local *local = girt_flow_attrs(get_ptr, flow, "proxy.default");
	

	struct bufferevent *orig_bev = girt_flow_attrs(get_ptr, flow, "orig.bev");
	struct bufferevent *dest_bev = girt_flow_attrs(get_ptr, flow, "dest.bev");
	int is_orig = ((orig_bev != NULL && bev == orig_bev) || (dest_bev == NULL || bev != dest_bev));
	struct bufferevent *other_bev = is_orig ? dest_bev : orig_bev;
	int other_closed = girt_flow_attrs(get_tag, flow, is_orig ? "dest.closed" : "orig.closed");

	if (events & BEV_EVENT_EOF) {
		girt_flow_attrs(set_tag, flow, is_orig ? "orig.closed" : "dest.closed");
		if (!other_closed && other_bev != NULL) {
			struct evbuffer *inbuf, *outbuf;
			inbuf = bufferevent_get_input(bev);
			outbuf = bufferevent_get_output(other_bev);

			if (evbuffer_get_length(inbuf) > 0) {
				evbuffer_add_buffer(outbuf, inbuf);
			} else {
				/* if the other end is still open and doesn't
				 * have data to send, close it, otherwise its
				 * writecb will close it after writing what's
				 * left in the output buffer. */
				if (evbuffer_get_length(outbuf) == 0) {
					girt_flow_attrs(set_tag, flow, is_orig ? "dest.closed" : "orig.closed");
				}
			}
		}
	}

	if (events & BEV_EVENT_CONNECTED) {
		if (!is_orig) {
			girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Connection established", 1);
			girt_flow_attrs(put_u64, flow, "time.connect", girt_current_millis());
			orig_bev = bufferevent_socket_new(girt_flow_event_base(flow), girt_flow_attrs(get_int, flow, "orig.addr.name"), BEV_OPT_DEFER_CALLBACKS);
			girt_flow_attrs(put_ptr, flow, "orig.bev", orig_bev, girt_bufferevent_free_pp);


			girt_link *link_out = girt_filters_flow_start(local->outbound_filters, girt_link_init_bev_evbuf(flow, "orig.link",
				dest_bev, bufferevent_get_output(dest_bev), NULL, 1));
			bufferevent_setcb(orig_bev, local_read_callback, local_write_callback, local_event_callback, link_out);

			girt_link *link_in = girt_filters_flow_start(local->inbound_filters, girt_link_init_bev_evbuf(flow, "dest.link",
				orig_bev, bufferevent_get_output(orig_bev), NULL, 0));
			local_bufferevent_replace_link(dest_bev, link_in);
		}
	}

    if (events & BEV_EVENT_ERROR) {
		int status = girt_flow_attrs(get_int, flow, "dest.connect.status");
		if (status == 0) {
			girt_flow_attrs(put_lit_int, flow, "dest.connect.status", "Connection failed", -1);
			girt_flow_attrs(set_tag, flow, "orig.closed");
			girt_flow_printf(INFO, flow, "Connection failed: (%d) %s\n", errno, strerror(errno));
		} else
		if (!other_closed && other_bev != NULL && evbuffer_get_length(bufferevent_get_output(other_bev)) == 0) {
			girt_flow_attrs(set_tag, flow, is_orig ? "dest.closed" : "orig.closed");
        }
	}
}

static void
local_bufferevent_replace_link(struct bufferevent *bev, girt_link *link)
{
	bufferevent_data_cb readcb;
	bufferevent_data_cb writecb;
	bufferevent_event_cb eventcb;
	void *dummy;

	bufferevent_getcb(bev, &readcb, &writecb, &eventcb, &dummy);

	bufferevent_setcb(bev, readcb, writecb, eventcb, link);
}


// event read callback. copy from bev to dest fd (read callback) arg is link
static void
local_datagram_dest_read_fd_callback(evutil_socket_t dest_fd, short what, void *arg)
{
	if ((what & EV_READ) == 0) {
		return;
	}
	girt_link *link = arg;
	girt_local *local = girt_link_arg(link);

	// datagram socket was 'connected' by listener, so we know what the
	// source address is and shouldn't need to use recvfrom. However, it doesn't
	// work and we get 'CONNREFUSED'. But recvfrom works but gives a blank address
	// so let's just go with that.
	void *buf = girt_threadbuf_get(local->bufkey, local->buflen = 0x10000);
	if (buf == NULL) {
		return;
	}

	struct sockaddr_storage _srcaddr = {0};
	struct sockaddr *srcaddr = (struct sockaddr *)&_srcaddr;
	socklen_t srcaddrlen = sizeof(_srcaddr);
	ssize_t pktlen = recvfrom(dest_fd, buf, local->buflen, 0, srcaddr, &srcaddrlen);

	if (pktlen > 0) {
		evbuffer_add_reference(girt_link_evbuf(link), girt_memdup(buf, pktlen), pktlen, girt_evbuffer_ref_free, girt_link_flow(link));
		// char *name = girt_sockaddr_to_addrname(srcaddr, NULL, NULL);
		// girt_flow_printf(INFO, girt_link_flow(link), "Received datagram from %s, len %ld", name, pktlen);
		// free(name);
	} else
	if (pktlen < 0) {// && errno != EAGAIN && errno != EWOULDBLOCK) {
		girt_flow_printf(INFO, girt_link_flow(link), "Datagram receive failed: %s", strerror(errno));
	}
}

static void
local_datagram_write_fd_callback(struct evbuffer *evbuf, UNUSED const struct evbuffer_cb_info *info, void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);
	int from_orig = girt_link_from_orig(link);

	int fd = girt_flow_attrs(get_int, flow, from_orig ? "dest.addr.name" : "orig.addr.name");

	struct evbuffer_iovec iov[1];
	while (evbuffer_peek(evbuf, -1, NULL, iov, 1) > 0) {
		size_t tolen = 0;
		struct sockaddr *to = girt_flow_attrs(get_ptr_len, flow, from_orig ? "dest.addr.raw" : "orig.addr.raw", &tolen);

		// char *name = girt_flow_attrs(get_str, flow, from_orig ? "dest.addr.name" : "orig.addr.name");
		// girt_flow_printf(INFO, flow, "Sending datagram to %s len %ld", name, (ssize_t)iov[0].iov_len);

		ssize_t sent = 0;
		if (!from_orig) {
			// to orig, need to spoof source address
			girt_message *reply = girt_flow_attrs(get_ptr, flow, "message.reply");
			if (reply != NULL) {
				sent = girt_message_send(girt_message_set_data(reply, iov[0].iov_base, iov[0].iov_len), fd, 0);
				if (sent == -1) {
					girt_flow_printf(INFO, flow, "Datagram send to %s failed: %s", from_orig ? "dest" : "orig", girt_message_error(reply));
					sent = -2;
				}
			} else {
				girt_flow_print(WARN, flow, "No canned UDP reply message, cannot spoof source address");
				sent = sendto(fd, iov[0].iov_base, iov[0].iov_len, 0, to, tolen);
			}
		} else {
			sent = sendto(fd, iov[0].iov_base, iov[0].iov_len, 0, to, tolen);
		}

		evbuffer_drain(evbuf, iov[0].iov_len);
		if (sent == -1) {
//			if (errno != EAGAIN && errno != EWOULDBLOCK) {
				girt_flow_printf(INFO, flow, "Datagram send to %s failed: %s", from_orig ? "dest" : "orig", strerror(errno));
//			}
		} else
		if (sent >= 0 && sent != (ssize_t)iov[0].iov_len) {
			girt_flow_printf(INFO, flow, "Only sent %ld of %lu bytes of datagram to %s",
				sent, iov[0].iov_len, from_orig ? "dest" : "orig");
		}
	}
}

static void
local_datagram_flow_start(girt_local *local, girt_flow *flow)
{
	// dest end
	struct bufferevent **dest = girt_flow_attrs(push_ptr_len, flow, "filter.bevpairs",
		 NULL, 2*sizeof(struct bufferevent *),
		girt_bufferevent_pair_free_pp
	);
	girt_check_oom_result(bufferevent_pair_new(girt_flow_event_base(flow), 0/*BEV_OPT_THREADSAFE*/, dest));
	girt_flow_attrs(put_ptr, flow, "dest.bev", dest[0], NULL);

	// read from fd goes to dest bev #1 output
	int dest_fd = girt_flow_attrs(get_int, flow, "dest.addr.name");
	struct event *dest_fd_ev = event_new(girt_flow_event_base(flow), dest_fd, EV_READ | EV_PERSIST,
			local_datagram_dest_read_fd_callback,
			girt_link_init_bev_evbuf(flow, "dest.link.1.out", dest[1], bufferevent_get_output(dest[1]), local, 0)
		);

	girt_flow_attrs(push_ptr, flow, "dest.event", dest_fd_ev, girt_event_free_pp);
	event_add(dest_fd_ev, NULL);

	// write to fd comes from dest bev #1 input
	struct evbuffer *dest_evbuf = girt_flow_attrs(push_ptr, flow, "dest.evbuf", evbuffer_new(), girt_evbuffer_free_pp);
	girt_link *destfdarg = girt_link_init_bev_evbuf(flow, "dest.link.fd.out", NULL, dest_evbuf, local, 1);
	evbuffer_add_cb(dest_evbuf, local_datagram_write_fd_callback, destfdarg);
	// evbuffer_defer_callbacks(dest_evbuf, girt_flow_event_base(flow));
	bufferevent_setcb(dest[1], local_read_callback, NULL, NULL, destfdarg);
	bufferevent_enable(dest[1], EV_READ);

	// orig end
	struct bufferevent **orig = girt_flow_attrs(push_ptr_len, flow, "filter.bevpairs",
		 NULL, 2*sizeof(struct bufferevent *),
		girt_bufferevent_pair_free_pp
	);
	girt_check_oom_result(bufferevent_pair_new(girt_flow_event_base(flow), 0/*BEV_OPT_THREADSAFE*/, orig));
	girt_flow_attrs(put_ptr, flow, "orig.bev", orig[1], NULL);
	girt_flow_attrs(put_ptr, flow, "orig.output", bufferevent_get_output(orig[0]), NULL);

	// write to fd comes from orig bev #0 input
	struct evbuffer *orig_evbuf = girt_flow_attrs(push_ptr, flow, "orig.evbuf", evbuffer_new(), girt_evbuffer_free_pp);
	girt_link *origfdarg = girt_link_init_bev_evbuf(flow, "orig.link.fd.out", NULL, orig_evbuf, local, 0);
	evbuffer_add_cb(orig_evbuf, local_datagram_write_fd_callback, origfdarg);
	// evbuffer_defer_callbacks(dest_evbuf, girt_flow_event_base(flow));
	bufferevent_setcb(orig[0], local_read_callback, NULL, NULL, origfdarg);
	bufferevent_enable(orig[0], EV_READ);


	// set outbound link (with possible filters) to dest
	girt_link *link_out = girt_filters_flow_start(local->outbound_filters, girt_link_init_bev_evbuf(
		flow, "dest.link", dest[0], bufferevent_get_output(dest[0]), NULL, 1));
	bufferevent_setcb(orig[1], local_read_callback, local_write_callback, local_event_callback, link_out);
	bufferevent_enable(orig[1], EV_READ|EV_WRITE);

	// set inbound link (with possible filters) to orig
	girt_link *link_in = girt_filters_flow_start(local->inbound_filters, girt_link_init_bev_evbuf(
		flow, "orig.link", orig[1], bufferevent_get_output(orig[1]), NULL, 0));
	bufferevent_setcb(dest[0], local_read_callback, local_write_callback, local_event_callback, link_in);
	bufferevent_enable(dest[0], EV_READ|EV_WRITE);
}

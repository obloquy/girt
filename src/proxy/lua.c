#include <girt/girt.h>

typedef struct {
	pthread_key_t bufkey;
	size_t max_buffer_len;
	girt_wrk *workers; // private worker pool

	girt_proxy *proxy;
} girt_local;

#include "config.h"

#if HAVE_LUA5_3_LUA_H
#include <lua5.3/lua.h>
#include <lua5.3/lauxlib.h>
#include <lua5.3/lualib.h>
#else
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#endif

static void local_end_check_callback(evutil_socket_t fd, short int what, void *arg);
static void local_event_callback(struct bufferevent *bev, short events, void *arg);
static void local_write_callback(struct bufferevent *bev, void *arg);
static void local_read_callback(struct bufferevent *bev, void *arg);

static ssize_t local_writev_orig(girt_flow *flow, struct iovec *iov, int n);
static void local_lua_init(girt_flow *flow);
static void local_lua_free(void **vpp);
static int l_girt_print(lua_State *L);
#ifndef HAVE_LUAL_TOLSTRING
static const char *luaL_tolstring (lua_State *L, int idx, size_t *len);
#define LUA_OK 0
#endif

#define USE_LUA_REGISTRY_FOR_FLOW 1

girt_local *
_girt_proxy_init(girt_proxy *proxy, UNUSED girt_cfg *proxy_config)
{
	girt_local *local = girt_malloc(sizeof(*local));

	local->proxy = proxy;
	// @config <proxy:lua> max-buffer-len i64 0x4000
	// @config-desc The maximum size of a Lua command
	local->max_buffer_len = girt_config_get_i64(proxy_config, "max-buffer-len", 0x4000);

	local->workers = girt_workers_init(girt_proxy_root(proxy));

	girt_threadbuf_make(&local->bufkey);

	return local;
}

void
_girt_proxy_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_workers_free(local->workers);

	memset(local, 0, sizeof(*local));

	free(local);
}

girt_wrk *
_girt_proxy_workers(UNUSED girt_local *local)
{
	return local->workers; // use private worker pool
}

void
_girt_proxy_flow_start(girt_local *local, girt_flow *flow)
{
	girt_flow_attrs(put_ptr, flow, "proxy.default", local, NULL);

	if (girt_flow_attrs(get_tag, flow, "datagram")) {
		// datagrams are hard
//		local_datagram_flow_start(local, flow);
	} else {
		local_lua_init(flow);
		// streams are easy
		girt_flow_attrs_notify_enable(flow, "orig.closed", GIRT_ATTR_TAG, local_end_check_callback);

		// Ignore the worker assigned to the flow and use the listener event base instead.
		// That way if Lua spins it won't tie up any legitimate work.
		struct bufferevent *orig_bev = bufferevent_socket_new(
//				girt_listener_event_base(girt_proxy_listener(local->proxy)),
				girt_flow_event_base(flow),
				girt_flow_attrs(get_int, flow, "orig.addr.name"),
				BEV_OPT_DEFER_CALLBACKS
		);
		girt_flow_attrs(put_ptr, flow, "orig.bev", orig_bev, girt_bufferevent_free_pp);
		girt_link *null_link = girt_link_init_bev_evbuf(flow, "null.link", NULL, NULL, local, 1);
		bufferevent_setcb(orig_bev, local_read_callback, local_write_callback, local_event_callback, null_link);
		bufferevent_enable(orig_bev, EV_READ|EV_WRITE);
	}
girt_flow_dump(flow);
}

void
_girt_proxy_flow_send(UNUSED girt_local *local, girt_flow *flow, UNUSED struct evbuffer *datagram)
{
	if (!girt_flow_attrs(get_tag, flow, "datagram")) {
		return;
	}

	// girt_flow_printf(ERROR, flow, "%s: %lu\n", "_girt_proxy_flow_send", evbuffer_get_length(datagram));

	// inject 'datagram' into stream input
//	evbuffer_add_buffer(girt_flow_attrs(get_ptr, flow, "orig.output"), datagram);
}

static void
local_end_check_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;
	girt_local *local = girt_flow_attrs(get_ptr, flow, "proxy.default");

	if (girt_flow_attrs(get_tag, flow, "orig.closed")) {
		girt_proxy_flow_free(local->proxy, flow);
	}
}

static void
local_read_callback(struct bufferevent *bev, UNUSED void *arg)
{
	girt_link *link = arg;
	girt_local *local = girt_link_arg(link);
	girt_flow *flow = girt_link_flow(link);

	lua_State *L = girt_flow_attrs(get_ptr, flow, "lua.state");

	struct evbuffer *input = bufferevent_get_input(bev);
	girt_flow_attrs(add_u64, flow, "orig.bytes-read", evbuffer_get_length(input));

	struct evbuffer *partial = girt_flow_attrs(get_ptr, flow, "lua.partial");
	if (partial == NULL) {
		partial = girt_flow_attrs(put_ptr, flow, "lua.partial", evbuffer_new(), girt_evbuffer_free_pp);
	}
	evbuffer_add_buffer(partial, input);

	struct evbuffer *buffer = girt_flow_attrs(get_ptr, flow, "lua.buffer");
	if (buffer == NULL) {
		buffer = girt_flow_attrs(put_ptr, flow, "lua.buffer", evbuffer_new(), girt_evbuffer_free_pp);
	}

	char *line;
	while ((line = evbuffer_readln(partial, NULL, EVBUFFER_EOL_CRLF)) != NULL) {
		girt_printf(INFO, "%s\n", line);
		if (!strcmp(line, "--quit") || !strcmp(line, "--exit")) {
			girt_flow_attrs(set_tag, flow, "orig.closed");
		} else
		if (!strcmp(line, "--go")) {
			size_t len = evbuffer_get_length(buffer);
			char *buf = girt_threadbuf_get(local->bufkey, len);
			evbuffer_remove(buffer, buf, len);
			if (luaL_loadbuffer(L, buf, len, NULL) != LUA_OK) {
				char *msg = (char *)lua_tostring(L, 1);
				struct iovec iov[3];
				int n = 0;
				if (!strncmp(msg, "[string \"?\"]", 12)) {
					iov[n].iov_base = "line"; iov[n++].iov_len = 4;
					msg += 12;
				}
				iov[n].iov_base = msg, iov[n++].iov_len = strlen(msg);
				iov[n].iov_base = "\n", iov[n++].iov_len = 1;
				local_writev_orig(flow, iov, n);
			} else
			if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
				lua_pushstring(L, "\n");
				l_girt_print(L);
			}
		} else {
			size_t len = strlen(line);
			if (len > 0 && (len > 1 || line[0] != '\\')) {
				if (line[len-1] == '\\') {
					evbuffer_add(buffer, line, len-1);
				} else {
					evbuffer_add_printf(buffer, "%s\n", line);
				}
				len = evbuffer_get_length(buffer);
				if (len > local->max_buffer_len) {
					girt_flow_printf(ERROR, flow, "Lua buffer too large (%lu > %lu)", len, local->max_buffer_len);
					evbuffer_drain(buffer, len);
				}
			}
		}
		free(line);
	}
}

static void
local_write_callback(UNUSED struct bufferevent *bev, UNUSED void *arg)
{
	// girt_flow *flow = arg;

	/* This callback is invoked when there is data to read on bev. */
//	struct evbuffer *input = bufferevent_get_input(bev);
//	struct evbuffer *output = bufferevent_get_output(bev);

	/* Copy all the data from the input buffer to the output buffer. */
//	evbuffer_add_buffer(output, input);
}

static void
local_event_callback(UNUSED struct bufferevent *bev, short events, void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);

	if (events & (BEV_EVENT_EOF|BEV_EVENT_ERROR)) {
girt_print(INFO, "EOF\n");
		girt_flow_attrs(set_tag, flow, "orig.closed");
	}
}

#ifndef HAVE_LUAL_TOLSTRING
static const char *
luaL_tolstring (lua_State *L, int idx, size_t *len)
{
	if (!luaL_callmeta(L, idx, "__tostring")) {
		int t = lua_type(L, idx);
		switch (t) {
			case LUA_TNIL:
				lua_pushliteral(L, "nil");
				break;
			case LUA_TSTRING:
			case LUA_TNUMBER:
				lua_pushvalue(L, idx);
				break;
			case LUA_TBOOLEAN:
				if (lua_toboolean(L, idx))
					lua_pushliteral(L, "true");
				else
					lua_pushliteral(L, "false");
				break;
			default:
				lua_pushfstring(L, "%s: %p", lua_typename(L, t),
																		 lua_topointer(L, idx));
				break;
		}
	}
	return lua_tolstring(L, -1, len);
}
#endif

static int
l_girt_print(lua_State *L) {
	int nargs = lua_gettop(L);
	struct iovec iov[nargs];
	int n = 0;

#if USE_LUA_REGISTRY_FOR_FLOW
	lua_pushliteral(L, "command_flow");
	lua_gettable(L, LUA_REGISTRYINDEX);
	girt_flow *flow = (girt_flow *)lua_touserdata(L, -1);
	lua_pop(L, 1); // remove the userdata
#else
	int t = lua_getglobal(L, "command_flow"); 
	girt_flow *flow = (girt_flow *)lua_touserdata(L, -1);
	lua_pop(L, 1); // remove the userdata
#endif
	for (int i = 1; i <= nargs; ++i) {
		iov[n].iov_base = (void *)luaL_tolstring(L, i, &iov[n].iov_len);
		n += 1;
		lua_pop(L, 1); // remove the string
	}

	local_writev_orig(flow, iov, n);

	return 0;
}

static const struct luaL_Reg girtlib[] = {
	{"print", l_girt_print},
	{NULL, NULL} /* end of array */
};

static void
local_lua_init(girt_flow *flow)
{
	// Start our Lua run-time state and load the standard libraries.
	lua_State *L = luaL_newstate();
	girt_flow_attrs(put_ptr, flow, "lua.state", L, local_lua_free);
	luaL_openlibs(L);

	lua_getglobal(L, "_G");
#ifndef HAVE_LUAL_TOLSTRING
	luaL_register(L, NULL, girtlib); // for Lua versions < 5.2
#else
	luaL_setfuncs(L, girtlib, 0);	// for Lua versions 5.2 or greater
#endif
	lua_pop(L, 1); // remove _G

#if USE_LUA_REGISTRY_FOR_FLOW
	lua_pushliteral(L, "command_flow");
	lua_pushlightuserdata(L, flow);
	lua_settable(L, LUA_REGISTRYINDEX);
#else
	lua_pushlightuserdata(L, flow);
	lua_setglobal(L, "command_flow"); 
#endif
}

static void
local_lua_free(void **vpp)
{
	if (vpp == NULL) {
		return;
	}

	lua_State *L = (lua_State *)*vpp;

	lua_close(L);

	*vpp = NULL;
}

static ssize_t
local_writev_orig(girt_flow *flow, struct iovec *iov, int n)
{
	int fd = girt_flow_attrs(get_int, flow, "orig.addr.name");

	ssize_t total = 0;
	for (int i = 0; i < n; i++) {
		total += iov[i].iov_len;
	}

	ssize_t sent = writev(fd, iov, n);

	if (sent < 0) {
		girt_flow_printf(INFO, flow, "Send to orig failed: %s", strerror(errno));
	} else {
		girt_flow_attrs(add_u64, flow, "orig.bytes-written", sent);
		if (sent != total) {
			girt_flow_printf(INFO, flow, "Only sent %ld of %lu bytes to orig", sent, total);
		}
	}

	return sent;
}

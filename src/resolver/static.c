
#include <girt/girt.h>

typedef struct {
	struct addrinfo *ai;
} girt_local;

girt_local *
_girt_resolver_init(UNUSED girt_resolver *resolver, UNUSED girt_cfg *resolver_config)
{
	girt_local *local = girt_malloc(sizeof(*local));

	girt_lsnsock *s = girt_listener_socket(girt_proxy_listener(girt_resolver_proxy(resolver)));

	// @config <resolver:static> - <addrinfo>
	// @config-def port The listener port number
	// @config-desc The static address to which flows should be directed
	local->ai = girt_config_get_addrinfo(resolver_config, NULL, 0, girt_lsnsock_family(s), NULL, girt_lsnsock_portname(s));

	return local;
}

void
_girt_resolver_free(UNUSED girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_config_free_addrinfo(local->ai);

	memset(local, 0, sizeof(*local));

	free(local);
}

void
_girt_resolver_resolve(UNUSED char *host, UNUSED char *port, girt_resolver_result_fn callback, girt_resolverarg *arg)
{
	girt_local *local = girt_resolver_local(girt_resolverarg_resolver(arg));

	callback(0, local->ai, arg);
}


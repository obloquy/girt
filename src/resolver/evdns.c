
#include <girt/girt.h>

typedef struct girt_local girt_local;

girt_local *
_girt_resolver_init(UNUSED girt_resolver *resolver, UNUSED girt_cfg *resolver_config)
{
	return girt_malloc(1);
}

void
_girt_resolver_free(UNUSED girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_free(local);
}

void
_girt_resolver_resolve(char *host, char *port, girt_resolver_result_fn callback, girt_resolverarg *arg)
{
	girt_flow *flow = girt_resolverarg_flow(arg);
//	girt_local *local = girt_resolver_local(girt_resolverarg_resolver(arg));

	struct evutil_addrinfo hints = {
		.ai_family = girt_flow_attrs(get_int, flow, "listen.family"),
		.ai_socktype = girt_flow_attrs(get_int, flow, "listen.type"),
		.ai_protocol = girt_flow_attrs(get_int, flow, "listen.proto"),
		.ai_flags = AI_ADDRCONFIG
	};

	evdns_getaddrinfo(girt_flow_evdns_base(flow), host, port, &hints, callback, arg);
}


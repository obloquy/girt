#include <girt/girt.h>

typedef struct {
	void	*buf;
	size_t	len;
} girt_thread_buf;

static void threadbuf_free(void *ptr)
{
	girt_thread_buf *tbuf = ptr;
	if (tbuf != NULL) {
		free(tbuf->buf);
		free(tbuf);
	}
}

int
girt_threadbuf_make(pthread_key_t *key)
{
	int err = pthread_key_create(key, threadbuf_free);
 	if (err != 0) {
		girt_printf(ERROR, "Failed to create pthread key for buffer: %s\n", strerror(err));
	}

	return err;
}

size_t
girt_threadbuf_len(pthread_key_t key)
{
	girt_thread_buf *tbuf = pthread_getspecific(key);

	return tbuf == NULL ? 0 : tbuf->len;
}

void *
girt_threadbuf_get(pthread_key_t key, size_t len)
{
	girt_thread_buf *tbuf = pthread_getspecific(key);
	if (tbuf == NULL) {
		tbuf = girt_malloc(sizeof(*tbuf));
		int err = pthread_setspecific(key, tbuf);
		if (err != 0) {
			free(tbuf);
			if (err == ENOMEM) {
				girt_oom();
			}
			girt_printf(ERROR, "Failed to set pthread specific data for buffer: %s\n", strerror(err));
			return NULL;
		}
	}
	if (len > tbuf->len) {
		tbuf->buf = girt_realloc(tbuf->buf, tbuf->len = len);
	}

	return tbuf->buf;
}

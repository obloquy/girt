#include <girt/girt.h>

#include <private/filter.h>

PLUGIN_FUNCS(
		filter,
		{
			girt_plugin_context *(*init)(girt_filter *filter, girt_cfg *filter_config);
			void (*read)(struct bufferevent *bev, void *arg);
			void (*event)(struct bufferevent *bev, short events, void *arg);
			void (*free)(girt_plugin_context *context);
		},
		"init", "read", "event", "free"
)

static void filter_read_callback(struct bufferevent *bev, void *arg);
static void filter_event_callback(struct bufferevent *bev, short events, void *arg);

girt_filter *
girt_filter_init(girt_proxy *proxy, girt_cfg *filter_config, char *type)
{
	girt_enter();
	girt_filter *filter = girt_malloc(sizeof(*filter));
	defer { girt_if_fail { girt_filter_free(filter); } }

	filter->proxy = proxy;

	filter->plugin = girt_plugins_load_filter(girt_root_plugins(girt_filter_root(filter)), type);
	if (filter->plugin == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to load filter\n");
		girt_fail(NULL);
	}
	filter->context = girt_plugin_filter_funcs(filter->plugin)->init(filter, filter_config);
	if (filter->context == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to initialise filter\n");
		girt_fail(NULL);
	}

	girt_return(filter);
}

void
girt_filter_free(girt_filter *filter)
{
	if (filter == NULL) {
		return;
	}

	if (filter->plugin != NULL && filter->context != NULL) {
		girt_plugin_filter_funcs(filter->plugin)->free(filter->context);
	}

	memset(filter, 0, sizeof(*filter));

	free(filter);
}

girt_link *
girt_filter_flow_start(UNUSED girt_filter *filter, girt_link *next_hop)
{
	girt_enter();
	girt_flow *flow = girt_link_flow(next_hop);

	struct bufferevent **bevs = girt_flow_attrs(push_ptr_len, flow, "filter.bevpairs", NULL, 2*sizeof(struct bufferevent *), girt_bufferevent_pair_free_pp);
	girt_check_oom_result(bufferevent_pair_new(girt_flow_event_base(flow), 0/*BEV_OPT_THREADSAFE*/, bevs));

	/* Use next hop to set up read callbacks on bev #2 */
	bufferevent_setcb(bevs[1], filter_read_callback, NULL, filter_event_callback, next_hop);
	bufferevent_enable(bevs[1], EV_READ);

	/* Filter used bev #1 for read/write */
	bufferevent_setcb(bevs[0],
		girt_plugin_filter_funcs(filter->plugin)->read,
		NULL, // No write cb, we just write from the read 
		girt_plugin_filter_funcs(filter->plugin)->event, 
		girt_link_init(flow, girt_flow_attrs(mkkey, flow, "filter", girt_plugin_name(filter->plugin), "link.0.out"),
			bufferevent_get_output(bevs[0]), filter->context, girt_link_from_orig(next_hop))
	);
	bufferevent_enable(bevs[0], EV_READ);

	girt_return(girt_link_init(flow, girt_flow_attrs(mkkey, flow, "filter", girt_plugin_name(filter->plugin), "link.1.out"),
		bufferevent_get_output(bevs[1]), NULL, girt_link_from_orig(next_hop)));
}

static void
filter_read_callback(struct bufferevent *bev, void *arg)
{
	// girt_link *link = arg;
	//girt_printf(ERROR, "%s: %lu to %s\n", "filter_read_callback", evbuffer_get_length(bufferevent_get_input(bev)), girt_link_name(link));

	/* Copy all the data from the input buffer to the output buffer. */
	evbuffer_add_buffer(girt_link_evbuf((girt_link *)arg), bufferevent_get_input(bev));
}

static void
filter_event_callback(UNUSED struct bufferevent *bev, short events, UNUSED void *arg)
{
	girt_printf(ERROR, "EVENT %04x on filter bev#2\n", events);
}

girt_root *
girt_filter_root(girt_filter *filter)
{
	return girt_proxy_root(filter->proxy);
}

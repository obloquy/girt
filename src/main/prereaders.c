#include <girt/girt.h>

#include <private/prereaders.h>

girt_prereaders *
girt_prereaders_init(girt_proxy *proxy, girt_cfg *prereaders_config)
{
	girt_enter();
	girt_prereaders *prereaders = girt_malloc(sizeof(*prereaders));
	defer { girt_if_fail { girt_prereaders_free(prereaders); } }

	kv_init(prereaders->prereaders);

	// @config <prereaders> buffer-size i64 0x4000
	// @config-desc the Maximum amount of data that can be pre-read. It should be large enough to a typical set of HTTP headers.
	prereaders->buflen = girt_config_get_i64(prereaders_config, "buffer-size", 0x4000);

	// @config <prereaders> chain <prereader>...
	// @config-desc An array of prereaders to aplly to each new connection
	girt_cfg_vec *prereader_configs = girt_config_get_table_array(prereaders_config, "chain");
	defer { girt_config_free_table_array(prereader_configs); }
	if (kv_size(*prereader_configs) == 0) {
		girt_print(ERROR, "Missing or empty chain for prereader\n");
		girt_fail(NULL);
	}

	for (size_t i = 0; i < kv_size(*prereader_configs); i++) {
		girt_cfg *prereader_config = kv_A(*prereader_configs, i);
		// @config <prereader> type string
		// @config-desc The type for each prereader in the chain which will be used as the plug-in name
		char *type = girt_config_get_string(prereader_config, "type", NULL);
		defer { free(type); }
		if (type == NULL) {
			girt_print(ERROR, "No type for prereader\n");
			girt_fail(NULL);
		}
		kv_push(girt_prereader *, prereaders->prereaders, girt_prereader_init(proxy, prereader_config, type));
	}

	if (girt_threadbuf_make(&prereaders->bufkey) != 0) {
		girt_fail(NULL);
	}

	return prereaders;
}

void
girt_prereaders_free(girt_prereaders *prereaders)
{
	if (prereaders == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(prereaders->prereaders); i++) {
		girt_prereader_free(kv_A(prereaders->prereaders, i));
	}

	kv_destroy(prereaders->prereaders);

	memset(prereaders, 0, sizeof(*prereaders));

	free(prereaders);
}

int
girt_prereaders_peek(girt_prereaders *prereaders, char *fd_name, girt_flow *flow)
{
	int fd = girt_flow_attrs(get_int, flow, fd_name);
	if (fd < 0) {
		return GIRT_PREREADER_CLOSED;
	}

	ssize_t nread = 0;
	char *buf = NULL;

	girt_prereader_state *state = girt_flow_attrs(get_or_put_ptr, flow,
		"prereaders.states", (kv_size(prereaders->prereaders)+1)*sizeof(int), girt_free_pp
	);
	girt_prereader_state *last_read = state++;

	for (size_t i = 0; i < kv_size(prereaders->prereaders); i++) {
		if (state[i] == GIRT_PREREADER_AGAIN) {
			if (buf == NULL) {
				buf = girt_threadbuf_get(prereaders->bufkey, prereaders->buflen);
				if ((nread = recv(fd, buf, prereaders->buflen, MSG_PEEK)) == 0) {
					return state[i] = GIRT_PREREADER_CLOSED;
				}
				if (nread <= *last_read) {
					return state[i] = GIRT_PREREADER_AGAIN;
				}
			}

			state[i] = girt_prereader_peek(kv_A(prereaders->prereaders, i), buf, nread, flow);

			if (state[i] > 0) {
				ssize_t nused = recv(fd, buf, state[i], 0);
				if (nused == 0) {
					return state[i] = GIRT_PREREADER_CLOSED;
				} else
				if (nused < 0) {
					girt_flow_printf(INFO, flow, "Prereader failed to skip %d bytes: %s", state[i], strerror(errno));
					return state[i] = GIRT_PREREADER_ERROR;
				} else
				if (nused != state[i]) {
					girt_flow_printf(INFO, flow, "Prereader only skipped %ld of %d bytes", nused, state[i]);
					return state[i] = GIRT_PREREADER_ERROR;
				}

				buf += nused;
				*last_read = (nread -= nused);

				state[i] = GIRT_PREREADER_DONE;
				if (nread <= 0) {
					return GIRT_PREREADER_DONE;
				}
			} else
			if (state[i] == GIRT_PREREADER_ERROR || state[i] == GIRT_PREREADER_AGAIN) {
				return state[i];
			} else
			if (state[i] == GIRT_PREREADER_FINAL) {
				return GIRT_PREREADER_DONE;
			}
		}
	}

	return GIRT_PREREADER_DONE;
}

#include <girt/girt.h>

#include <private/attrs.h>

static girt_attr *attrs_get(girt_attrs *attrs, char *name);
static girt_attr *attrs_put(girt_attrs *attrs, girt_attr *attr);
static girt_attr *attrs_get_or_init(girt_attrs *attrs, char *name, girt_attr_type type);
static void attrs_set_notify(girt_attrs *attrs, char *prefix, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc);
static void attrs_notify(girt_attrs *attrs, girt_attr *attr);

static girt_attr *attr_init(char *name, girt_attr_type type);
static void attr_set_notify(girt_attr *attr, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc);
static void attr_free(girt_attr *attr);
static void attr_clear_value_vec(girt_attr_type type, girt_attr_value_vec *vec);
static void attr_clear_value(girt_attr_type type, girt_attr_value *val);
static void attr_clear(girt_attr *attr);

#define NAME_OK(n) (n != NULL && n[0] != '\0')

#define attr_val_at(a, i) kv_A(a->vec, i)
#define attr_nval(a) (a != NULL ? kv_size(a->vec) : 0)

girt_attrs *
girt_attrs_init(girt_attrs_notifier_fn notifier)
{
	girt_attrs *attrs = girt_malloc(sizeof(*attrs));

	attrs->hash = girt_check_oom_ptr(kh_init(attrs));
	attrs->notify = girt_check_oom_ptr(kh_init(notify));
	kv_init(attrs->vec);

	attrs->notifier = notifier;

	return attrs;
}

void
girt_attrs_free(girt_attrs *attrs)
{
	if (attrs == NULL) {
		return;
	}

	for (khiter_t k = kh_begin(attrs->notify); k != kh_end(attrs->notify); ++k) {
		if (kh_exist(attrs->notify, k)) {
			kv_destroy(kh_value(attrs->notify, k));
			free((void *)kh_key(attrs->notify, k));
			kh_del(notify, attrs->notify, k); 
		}
	}
	kh_destroy(notify, attrs->notify);

	kh_destroy(attrs, attrs->hash);

	for (ssize_t i = kv_size(attrs->vec)-1; i >= 0; i--) {
		attr_free(kv_A(attrs->vec, i));
	}
	kv_destroy(attrs->vec);

	memset(attrs, 0, sizeof(*attrs));

	free(attrs);
}

static void
attrs_compact(girt_attrs *attrs)
{
	int n_freed = 0;
	for (size_t i = 0; i < kv_size(attrs->vec); i++) {
		if (n_freed > 0) {
			kv_A(attrs->vec, i) = kv_A(attrs->vec, i+n_freed);
		}
		girt_attr *attr = kv_A(attrs->vec, i);
		if (attr == NULL) {
			n_freed += 1;
		} else
		if (attr->type == GIRT_ATTR_FREE) {
			free(attr);
			n_freed += 1;
		}
	}

	attrs->vec.n -= n_freed;
}

int
girt_attrs_exists(girt_attrs *attrs, char *name)
{
	return NAME_OK(name) ? 0 : (kh_get(attrs, attrs->hash, name) != kh_end(attrs->hash));
}

char *
girt_attrs_mkkey_v(girt_attrs *attrs, ...)
{
	va_list args;
	va_start(args, attrs);

	char *key = attrs->keybuf;
	size_t buflen = sizeof(attrs->keybuf);
	size_t len = 0;

	char *keypart = va_arg(args, char *);
	if (keypart != NULL) {
		len += girt_strncpy(key+len, buflen-len, keypart);

		while ((keypart = va_arg(args, char *)) != NULL) {
			len += girt_strncpy(key+len, buflen-len, ".");
			if (keypart[0] == '%') {
				switch(keypart[1]) {
					case 'p': {
						void *ptr = va_arg(args, char *);
						if (ptr == NULL) goto done;
						len += snprintf(key+len, buflen-len, "%p", ptr);
						break;
					}
				}
			} else {
				len += girt_strncpy(key+len, buflen-len, keypart);
			}
		}
	} else {
		key[0] = '\0';
	}

done:
	return key;
}

void *
girt_attrs_push_ptr_len_type_int(girt_attrs *attrs, char *name, void *ptr, size_t len, int type, int i, girt_attr_free_fn free, girt_attr_close_fn close)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_PTR);
	if (attr != NULL) {
		if (ptr == NULL && len > 0 && free != NULL) {
			ptr = girt_malloc(len);
		}
		kv_push(girt_attr_value, attr->vec, ((girt_attr_value){
			.u.ptr.close = close,
			.u.ptr.type = type,
			.u.ptr.ptr = ptr,
			.u.ptr.len = len,
			.u.ptr.free = free,
			.i = i,
		}));

		attrs_notify(attrs, attr);

		return ptr;
	}

	return NULL;
}

void *
girt_attrs_get_or_put_ptr(girt_attrs *attrs, char *name, size_t len, girt_attr_free_fn free)
{
	if (free == NULL) {
		free = girt_free_pp;
	}

	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_PTR);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_ptr_len_type_int(attrs, name, girt_malloc(len), len, 0, 0, free, NULL);
		}

		if (attr_val_at(attr, 0).u.ptr.ptr != NULL && attr_val_at(attr, 0).u.ptr.type == 0 && attr_val_at(attr, 0).u.ptr.free != NULL && attr_val_at(attr, 0).u.ptr.len != len) {
			attr_clear_value(attr->type, &attr_val_at(attr, 0));

			attr_val_at(attr, 0).u.ptr.ptr = girt_malloc(len);
			attr_val_at(attr, 0).u.ptr.free = free;
			attr_val_at(attr, 0).u.ptr.len = len;

			attrs_notify(attrs, attr);
		}

		return attr_val_at(attr, 0).u.ptr.ptr;
	}

	return NULL;
}

void *
girt_attrs_put_ptr_len_type_int(girt_attrs *attrs, char *name, void *ptr, size_t len, int type, int i, girt_attr_free_fn free, girt_attr_close_fn close)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_PTR);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_ptr_len_type_int(attrs, name, ptr, len, type, i, free, close);
		}

		if (attr_val_at(attr, 0).u.ptr.ptr != NULL) {
			attr_clear_value(attr->type, &attr_val_at(attr, 0));
		}
		attr_val_at(attr, 0).u.ptr.ptr = ptr;
		attr_val_at(attr, 0).u.ptr.len = len;
		attr_val_at(attr, 0).u.ptr.type = type;
		attr_val_at(attr, 0).u.ptr.free = free;
		attr_val_at(attr, 0).u.ptr.close = close;
		attr_val_at(attr, 0).i = i;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.ptr.ptr;
	}

	return NULL;
}

void *
girt_attrs_get_ptr_len_int_at(girt_attrs *attrs, char *name, size_t *lptr, int *iptr, size_t idx)
{
	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && attr->type == GIRT_ATTR_PTR && idx < attr_nval(attr))
	{
		if (lptr) {
			*lptr = attr_val_at(attr, idx).u.ptr.len;
		}
		if (iptr) {
			*iptr = attr_val_at(attr, idx).i;
		}
		return attr_val_at(attr, idx).u.ptr.ptr;
	}

	if (lptr) {
		*lptr = 0;
	}
	if (iptr) {
		*iptr = 0;
	}

	return NULL;
}

char *
girt_attrs_get_str_int_at(girt_attrs *attrs, char *name, int *iptr, size_t idx)
{
	char *str = NULL;
	if (iptr) {
		*iptr = 0;
	}

	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && idx < attr_nval(attr)) {
		switch (attr->type) {
		case GIRT_ATTR_FREE:
		case GIRT_ATTR_TAG:
		case GIRT_ATTR_I64:
		case GIRT_ATTR_U64:
			break;
		case GIRT_ATTR_PTR: {
			int type = attr_val_at(attr, idx).u.ptr.type;
			if (type == GIRT_ATTR_PTR_STR || type == GIRT_ATTR_PTR_HANDLE) {
				if (attr_val_at(attr, idx).u.ptr.ptr == NULL &&
						(attr_val_at(attr, idx).u.ptr.free == NULL || attr_val_at(attr, idx).u.ptr.free == girt_free_pp) &&
						attr_val_at(attr, idx).i != 0) {
					attr_val_at(attr, idx).u.ptr.ptr = girt_asprintf("%d", attr_val_at(attr, idx).i);
					attr_val_at(attr, idx).u.ptr.free = girt_free_pp;
				}
				str = (char *)attr_val_at(attr, idx).u.ptr.ptr;
			}
			break;
		}
		case GIRT_ATTR_KSTR: {
			str = ks_str(attr_val_at(attr, idx).u.kstr);
			break;
		}
		}
		if (iptr) {
			*iptr = attr_val_at(attr, idx).i;
		}
	}

	return str;
}

kstring_t *
girt_attrs_put_kstr(girt_attrs *attrs, char *name, char *str)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_KSTR);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			kv_push(girt_attr_value, attr->vec, ((girt_attr_value){
				.u.kstr = {{0}},
			}));
		} else {
			attr_val_at(attr, 0).u.kstr->l = 0;
		}

		if (str != NULL) {
			kputs(str, attr_val_at(attr, 0).u.kstr);
		}

		attr_val_at(attr, 0).i = 0;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.kstr;
	}

	return NULL;
}

kstring_t *
girt_attrs_get_kstr(girt_attrs *attrs, char *name)
{
	girt_attr *attr = attrs_get(attrs, name);

	return (attr == NULL || attr_nval(attr) == 0 || attr->type != GIRT_ATTR_KSTR) ? NULL : attr_val_at(attr, 0).u.kstr;
}

uint64_t
girt_attrs_push_u64(girt_attrs *attrs, char *name, uint64_t u64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_U64);
	if (attr != NULL) {
		kv_push(girt_attr_value, attr->vec, ((girt_attr_value){
			.u.u64 = u64,
		}));

		attrs_notify(attrs, attr);

		return u64;
	}

	return 0UL;
}

uint64_t
girt_attrs_put_u64(girt_attrs *attrs, char *name, uint64_t u64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_U64);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_u64(attrs, name, u64);
		}

		attr_val_at(attr, 0).u.u64 = u64;
		attr_val_at(attr, 0).i = 0;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.u64;
	}

	return 0UL;
}

uint64_t
girt_attrs_add_u64(girt_attrs *attrs, char *name, uint64_t u64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_U64);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_u64(attrs, name, u64);
		}

		attr_val_at(attr, 0).u.u64 += u64;
		attr_val_at(attr, 0).i = 0;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.u64;
	}

	return 0UL;
}

uint64_t
girt_attrs_get_u64_at(girt_attrs *attrs, char *name, size_t idx)
{
	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && idx < attr_nval(attr)) {
		return attr_val_at(attr, idx).u.u64;
	}

	return 0UL;
}

int64_t
girt_attrs_push_i64(girt_attrs *attrs, char *name, int64_t i64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_I64);
	if (attr != NULL) {
		kv_push(girt_attr_value, attr->vec, ((girt_attr_value){
			.u.i64 = i64,
		}));

		attrs_notify(attrs, attr);

		return i64;
	}

	return 0L;
}

int64_t
girt_attrs_put_i64(girt_attrs *attrs, char *name, int64_t i64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_I64);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_i64(attrs, name, i64);
		}

		attr_val_at(attr, 0).u.i64 = i64;
		attr_val_at(attr, 0).i = 0;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.i64;
	}

	return 0L;
}

int64_t
girt_attrs_add_i64(girt_attrs *attrs, char *name, int64_t i64)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_I64);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			return girt_attrs_push_i64(attrs, name, i64);
		}

		attr_val_at(attr, 0).u.i64 += i64;
		attr_val_at(attr, 0).i = 0;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).u.i64;
	}

	return 0L;
}

int64_t
girt_attrs_get_i64_at(girt_attrs *attrs, char *name, size_t idx)
{
	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && idx < attr_nval(attr)) {
		return attr_val_at(attr, idx).u.i64;
	}

	return 0UL;
}

int
girt_attrs_set_int_at(girt_attrs *attrs, char *name, int i, size_t idx)
{
	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && idx < attr_nval(attr)) {
		attr_val_at(attr, idx).i = i;

		attrs_notify(attrs, attr);

		return attr_val_at(attr, idx).i;
	}

	return 0;
}

int
girt_attrs_get_int(girt_attrs *attrs, char *name)
{
	girt_attr *attr = attrs_get(attrs, name);

	return (attr == NULL || attr_nval(attr) == 0) ? 0 : attr_val_at(attr, 0).i;
}

int
girt_attrs_set_tag_int(girt_attrs *attrs, char *name, int i)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, GIRT_ATTR_TAG);
	if (attr != NULL) {
		if (attr_nval(attr) == 0) {
			kv_push(girt_attr_value, attr->vec, ((girt_attr_value){
				.i = i
			}));
		} else {
			attr_val_at(attr, 0).i = i;
		}

		attrs_notify(attrs, attr);

		return attr_val_at(attr, 0).i;
	}

	return 0;
}

int
girt_attrs_get_tag(girt_attrs *attrs, char *name)
{
	int not = 0;
	if (name != NULL) {
		while (*name == '!') {
			not = !not;
			name++;
		}
	}

	girt_attr *attr = attrs_get(attrs, name);
	if (attr != NULL && attr->type != GIRT_ATTR_TAG) {
		return 0;
	}

	int set = (attr != NULL && attr_nval(attr) > 0 && attr_val_at(attr, 0).i != 0);

	return not ? !set : set;
}

__attribute__ ((sentinel))
int
girt_attrs_has_any_tag_v(girt_attrs *attrs, ...)
{
	va_list args;
	va_start(args, attrs);

	char *name;
	while ((name = va_arg(args, char *)) != NULL) {
		if (girt_attrs_get_tag(attrs, name)) {
			return 1;
		}
	}

	return 0;
}

__attribute__ ((sentinel))
int
girt_attrs_has_all_tags_v(girt_attrs *attrs, ...)
{
	va_list args;
	va_start(args, attrs);

	char *name;
	while ((name = va_arg(args, char *)) != NULL) {
		if (!girt_attrs_get_tag(attrs, name)) {
			return 0;
		}
	}

	return 1;
}

char *
girt_attrs_get_name_ref(girt_attrs *attrs, char *name)
{
	girt_attr *attr = attrs_get(attrs, name);

	return attr != NULL ? attr->name : "<not found>";
}

void
girt_attrs_set_or_init_notify_loc(girt_attrs *attrs, char *name,
	girt_attr_type type, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc)
{
	girt_attr *attr = attrs_get_or_init(attrs, name, type);
	if (attr != NULL && attr->type == type) {
		attr_set_notify(attr, callback, arg, flags, loc);
	}
}

void
girt_attrs_set_notify_loc(girt_attrs *attrs, char *name, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc)
{
	if (name[strlen(name)-1] == '.') {
		attrs_set_notify(attrs, name, callback, arg, flags, loc);
	} else {
		attr_set_notify(attrs_get(attrs, name), callback, arg, flags, loc);
	}
}

void
girt_attrs_notify(girt_attrs *attrs, char *name)
{
	attrs_notify(attrs, attrs_get(attrs, name));
}

size_t
girt_attrs_get_nval(girt_attrs *attrs, char *name)
{
	girt_attr *attr = attrs_get(attrs, name);
	return attr == NULL ? 0 : attr_nval(attr);
}

int
girt_attrs_delete(girt_attrs *attrs, char *name)
{
	if (name == NULL || name[0] == '\0') {
		return 0;
	}

	khiter_t k = kh_get(attrs, attrs->hash, name);
	if (k != kh_end(attrs->hash)) {
		girt_attr *attr = kh_value(attrs->hash, k);
		kh_del(attrs, attrs->hash, k);
		attr_clear(attr);
		attrs_compact(attrs);
		return 1;
	}

	return 0;
}

static char *
attr_type_name(girt_attr_type type)
{
	switch (type) {
	case GIRT_ATTR_FREE:	return "free";
	case GIRT_ATTR_TAG:		return "tag";
	case GIRT_ATTR_PTR:		return "ptr";
	case GIRT_ATTR_KSTR:	return "kstr";
	case GIRT_ATTR_U64:		return "u64";
	case GIRT_ATTR_I64:		return "i64";
	}

	return "???";
}

static int
attr_dump_compare(const void *a, const void *b)
{
	girt_attr * const *aattr = a;
	girt_attr * const *battr = b;

	int cmp = strcmp((*aattr)->name, (*battr)->name);

	if (cmp != 0) {
		if (!strcmp((*aattr)->name, "id")) {
			return -1;
		} else
		if (!strcmp((*battr)->name, "id")) {
			return 1;
		} else {
			if ((*aattr)->type != (*battr)->type) {
				if ((*aattr)->type == GIRT_ATTR_TAG) {
					return -1;
				} else
				if ((*battr)->type == GIRT_ATTR_TAG) {
					return 1;
				}
			}
		}
	}

	return cmp;
}

void
girt_attrs_dump(girt_attrs *attrs, girt_str_vec *vec)
{
	kvec_t(girt_attr *) tmp;
	kv_init(tmp);

	kv_copy(girt_attr *, tmp, attrs->vec);
	qsort(&kv_A(tmp, 0), kv_size(tmp), sizeof(girt_attr *), attr_dump_compare);

	for (size_t i = 0; i < kv_size(tmp); i++) {
		girt_attr *attr = kv_A(tmp, i);
		girt_str_vec_printf(vec, "%-4s: %s\n",
			attr_type_name(attr->type), attr->name
		);
		for (size_t j = 0; j < kv_size(attr->notify); j++) {
			if ((kv_A(attr->notify, j).flags & GIRT_ATTR_NOTIFY_ENABLED) != 0) {
				girt_str_vec_printf(vec, "\t\t>      %s:%d (%s)\n",
					kv_A(attr->notify, j).loc.file, kv_A(attr->notify, j).loc.line, kv_A(attr->notify, j).loc.function
				);
			}
		}
		for (size_t j = 0; j < attr_nval(attr); j++) {
			char ival[40];
			size_t ival_len = snprintf(ival, sizeof(ival), "%d", attr_val_at(attr, j).i);
			if (attr_val_at(attr, j).i > 9) {
				snprintf(ival+ival_len, sizeof(ival)-ival_len, "=0x%x", attr_val_at(attr, j).i);
			}

			switch (attr->type) {
			case GIRT_ATTR_FREE:
				break;
			case GIRT_ATTR_TAG:
					girt_str_vec_printf(vec, "\t\t-      [%s]\n", ival);
				break;
			case GIRT_ATTR_PTR:
				if (attr_val_at(attr, j).u.ptr.type == GIRT_ATTR_PTR_STR) {
					girt_str_vec_printf(vec, "\t\t- %-4s '%s' [%s]\n",
						attr_val_at(attr, j).u.ptr.free == NULL ? "lit" : "str",
						(char *)attr_val_at(attr, j).u.ptr.ptr,
						ival
					);
				} else
				if (attr_val_at(attr, j).u.ptr.type == GIRT_ATTR_PTR_ALIAS) {
					girt_str_vec_printf(vec, "\t\t- link '%s'\n", (char *)attr_val_at(attr, j).u.ptr.ptr);
				} else
				if (attr_val_at(attr, j).u.ptr.type == GIRT_ATTR_PTR_HANDLE) {
					girt_str_vec_printf(vec, "\t\t- hndl '%s' [%s]\n", (char *)attr_val_at(attr, j).u.ptr.ptr, ival);
				} else {
					girt_str_vec_printf(vec, "\t\t- void (%lu) %p\n", attr_val_at(attr, j).u.ptr.len, attr_val_at(attr, j).u.ptr.ptr);
					if (attr_val_at(attr, j).u.ptr.len > 0) {
						girt_hexdump(vec, attr_val_at(attr, j).u.ptr.ptr, attr_val_at(attr, j).u.ptr.len,
							"\t\t-      ", "   ", '.'
						);
					}
				}
				break;
			case GIRT_ATTR_KSTR:
				girt_str_vec_printf(vec, "\t\t-      '%s'\n", ks_str(attr_val_at(attr, j).u.kstr));
				break;
			case GIRT_ATTR_U64:
				girt_str_vec_printf(vec, "\t\t-      %lu\n", attr_val_at(attr, j).u.u64);
				break;
			case GIRT_ATTR_I64:
				girt_str_vec_printf(vec, "\t\t-      %ld\n", attr_val_at(attr, j).u.i64);
				break;
			}
		}
	}

	kv_destroy(tmp);
}

void
girt_attrs_dump_fp(girt_attrs *attrs, FILE *fp)
{
	girt_str_vec *vec = girt_str_vec_init();

	girt_attrs_dump(attrs, vec);

	for (size_t i = 0; i < kv_size(*vec); i++) {
		fprintf(fp, "%s", kv_A(*vec, i));
	}

	girt_str_vec_free(vec);
}

static girt_attr *
attrs_put(girt_attrs *attrs, girt_attr *attr)
{
	int ret;
	khiter_t k = kh_put(attrs, attrs->hash, attr->name, &ret);
	if (ret) {
		// new
		kh_value(attrs->hash, k) = attr;
		kv_push(girt_attr *, attrs->vec, attr);
	} else {
		// exists
		attr_clear(kh_value(attrs->hash, k));
		memcpy(kh_value(attrs->hash, k), attr, sizeof(*attr));
		free(attr);
		attr = kh_value(attrs->hash, k);
	}

	return attr;
}

static girt_attr *
attrs_get(girt_attrs *attrs, char *name)
{
	if (!NAME_OK(name)) {
		return NULL;
	}

	khiter_t k = kh_get(attrs, attrs->hash, name);

	if (k != kh_end(attrs->hash)) {
		girt_attr *attr = kh_value(attrs->hash, k);
		if (attr->type == GIRT_ATTR_PTR && attr_nval(attr) > 0 && attr_val_at(attr, 0).u.ptr.type == GIRT_ATTR_PTR_ALIAS) {
			k = kh_get(attrs, attrs->hash, attr_val_at(attr, 0).u.ptr.ptr);
			attr = (k != kh_end(attrs->hash) ? kh_value(attrs->hash, k) : NULL);
		}
		return attr;
	}

	return NULL;
}

static girt_attr *
attrs_get_or_init(girt_attrs *attrs, char *name, girt_attr_type type)
{
	if (!NAME_OK(name)) {
		return NULL;
	}

	girt_attr *attr;
	khiter_t k = kh_get(attrs, attrs->hash, name);
	if (k != kh_end(attrs->hash)) {
		attr = kh_value(attrs->hash, k);
	} else {
		attr = attrs_put(attrs, attr_init(name, type));
	}

	return attr;
}

static girt_attr *
attr_init(char *name, girt_attr_type type)
{
	girt_attr *attr;

	size_t len = sizeof(girt_attr)+strlen(name);
	attr = girt_malloc(len);

	kv_init(attr->vec);

	kv_init(attr->notify);

	strcpy(attr->name, name);
	attr->type = type;

	return attr;
}

static void
notify_push(girt_attr_notify_vec *notify, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc)
{
	// direst and persist together invites infinite recursion, so turn off direct
	if ((flags & (GIRT_ATTR_NOTIFY_PERSIST|GIRT_ATTR_NOTIFY_DIRECT)) == (GIRT_ATTR_NOTIFY_PERSIST|GIRT_ATTR_NOTIFY_DIRECT)) {
		flags &= ~GIRT_ATTR_NOTIFY_DIRECT;
	}

	for (size_t i = 0; i < kv_size(*notify); i++) {
		if (kv_A(*notify, i).callback == callback) {
			kv_A(*notify, i).arg = arg;
			kv_A(*notify, i).flags = flags;
			if (loc != NULL) {
				kv_A(*notify, i).loc = *loc;
			}
			return;
		}
	}

	kv_push(girt_attr_notify, *notify, ((girt_attr_notify){ .flags = flags, .callback = callback, .arg = arg, .loc = *loc }));
}

static void
attrs_set_notify(girt_attrs *attrs, char *prefix, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc)
{
	prefix = girt_strdup(prefix);

	int ret;
	khiter_t k = kh_put(notify, attrs->notify, prefix, &ret);
	if (ret) {
		// new
		kv_init(kh_value(attrs->notify, k));
	} else {
		// exists
		free(prefix);
	}

	notify_push(&kh_value(attrs->notify, k), callback, arg, flags, loc);
}

static void
attr_set_notify(girt_attr *attr, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc)
{
	if (attr == NULL) {
		return;
	}

	notify_push(&attr->notify, callback, arg, flags, loc);
}

static void
notify_all(girt_attrs *attrs, girt_attr_notify_vec *notify, char *name)
{
	for (size_t i = 0; i < kv_size(*notify); i++) {
		if ((kv_A(*notify, i).flags & GIRT_ATTR_NOTIFY_ENABLED) != 0 && kv_A(*notify, i).callback) {
			if ((kv_A(*notify, i).flags & GIRT_ATTR_NOTIFY_PERSIST) == 0) {
				kv_A(*notify, i).flags = 0;
			}
			attrs->notifier(kv_A(*notify, i).arg,
				(kv_A(*notify, i).flags & GIRT_ATTR_NOTIFY_DIRECT) != 0 ? NULL : &(struct timeval){0,0},
				kv_A(*notify, i).callback,
				name,
				&kv_A(*notify, i).loc
			);
		}
	}
}

static void
attrs_notify(girt_attrs *attrs, girt_attr *attr)
{
	if (attr == NULL || attrs->notifier == NULL) {
		return;
	}

	// attr notify
	notify_all(attrs, &attr->notify, attr->name);

	// attr name prefix notify
	char prefix[strlen(attr->name)+1];
	strcpy(prefix, attr->name);
	char *last_dot = strrchr(prefix, '.');
	if (last_dot != NULL) {
		*last_dot = '\0';
	}

	khiter_t k = kh_get(notify, attrs->notify, prefix);
	if (k != kh_end(attrs->notify)) {
		notify_all(attrs, &kh_value(attrs->notify, k), prefix);
	}
}

static void
attr_clear_value(girt_attr_type type, girt_attr_value *val)
{
	if (type == GIRT_ATTR_FREE) {
		return;
	}

	if (type == GIRT_ATTR_PTR) {
		if (val->u.ptr.free != NULL) {
			val->u.ptr.free(&val->u.ptr.ptr);
		}
		if (val->u.ptr.type == GIRT_ATTR_PTR_HANDLE && val->u.ptr.close != NULL) {
			val->u.ptr.close(&val->i);
		}
	} else
	if (type == GIRT_ATTR_KSTR) {
		free(ks_release(val->u.kstr));
	}

	memset(val, 0, sizeof(*val));
}

static void
attr_clear_value_vec(girt_attr_type type, girt_attr_value_vec *vec)
{
	if (type == GIRT_ATTR_FREE) {
		return;
	}

	for (size_t i = 0; i < kv_size(*vec); i++) {
		attr_clear_value(type, &kv_A(*vec, i));
	}
}

static void
attr_clear(girt_attr *attr)
{
	if (attr == NULL || attr->type == GIRT_ATTR_FREE) {
		return;
	}

	attr_clear_value_vec(attr->type, &attr->vec);

	kv_destroy(attr->vec);

	kv_destroy(attr->notify);

	memset(attr, 0, sizeof(*attr)-1);
}

static void
attr_free(girt_attr *attr)
{
	attr_clear(attr);

	free(attr);
}

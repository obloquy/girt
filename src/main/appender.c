#include <girt/girt.h>

#include <private/appender.h>

PLUGIN_FUNCS(
		appender,
		{
			girt_plugin_context *(*init)(girt_appender *appender, girt_cfg *appender_config, log4c_appender_t *log4c_appender);
			int (*open)(log4c_appender_t *appender);
			int (*append)(log4c_appender_t *appender, const log4c_logging_event_t *event);
			int (*close)(log4c_appender_t *appender);
			void (*free)(girt_plugin_context *context);
		},
		"init", "open", "append", "close", "free"
)

girt_appender *
girt_appender_init(girt_log *logger, girt_cfg *appender_config, char *name)
{
	girt_enter();
	girt_appender *appender = girt_malloc(sizeof(*appender));
	defer { girt_if_fail { girt_appender_free(appender); } }

	appender->logger = logger;

	appender->appender = log4c_appender_get(name);
	appender->appender_type = girt_malloc(sizeof(*appender->appender_type));

	// @config <appender> type string "stdio"
	// @config-desc The type of the appender which is used as the plug-in name
	appender->appender_type->name = girt_config_get_string(appender_config, "type", "stdio");
	if (appender->appender_type->name == NULL) {
		girt_printf(ERROR, "No type for appender '%s'\n", name);
		girt_fail(NULL);
	}

	appender->plugin = girt_plugins_load_appender(girt_root_plugins(girt_logger_root(logger)), appender->appender_type->name);
	if (appender->plugin  == NULL) {
		girt_printf(ERROR, "Failed to load appender '%s'\n", name);
		girt_fail(NULL);
	}
	appender->context = girt_plugin_appender_funcs(appender->plugin)->init(appender, appender_config, appender->appender);
	if (appender->context == NULL) {
		girt_printf(ERROR, "Failed to initialise appender '%s'\n", name);
		girt_fail(NULL);
	}

	appender->appender_type->open = girt_plugin_appender_funcs(appender->plugin)->open;
	appender->appender_type->append = girt_plugin_appender_funcs(appender->plugin)->append;
	appender->appender_type->close = girt_plugin_appender_funcs(appender->plugin)->close;

	log4c_appender_type_set(appender->appender_type);

	log4c_appender_set_type(appender->appender, appender->appender_type);

	// @config <appender> formatter string The appender's name
	// @config-desc The name of the formatter to use with the appender
 	char *formatter = girt_config_get_string(appender_config, "formatter", name);
	defer { free(formatter); }
	if (formatter != NULL) {
		log4c_appender_set_layout(appender->appender, log4c_layout_get(formatter));
	}

	girt_return(appender);
}

void
girt_appender_free(girt_appender *appender)
{
	if (appender->plugin != NULL && appender->context != NULL) {
		girt_plugin_appender_funcs(appender->plugin)->free(appender->context);
	}

	free((char *)appender->appender_type->name);
	free(appender->appender_type);

	memset(appender, 0, sizeof(*appender));

	free(appender);
}

girt_log *
girt_appender_logger(girt_appender *appender)
{
	return appender->logger;
}

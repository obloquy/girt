#include <girt/girt.h>

#include <private/flow.h>

int girt_logger_level_FLOW = GIRT_LOGGER_INFO;

static void flow_log_dest_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg);
static void flow_log_end(girt_flow *flow);
static void flow_attr_notifier(void *arg, struct timeval *tmo, event_callback_fn callback, char *attr_name, girt_location *loc);

girt_flow *
girt_flow_init(evutil_socket_t fd, struct sockaddr *addr, int len, girt_lsnsock *socket, girt_worker *worker)
{
	girt_flow *flow = girt_malloc(sizeof(*flow));

    ulid_make_urandom(&flow->id);
    ulid_string(&flow->id, flow->idstr);

	flow->attrs = girt_attrs_init(flow_attr_notifier);
	flow->events = girt_check_oom_ptr(kh_init(events));

	girt_flow_attrs_notify_direct(flow, "dest.addr.name", GIRT_ATTR_PTR, flow_log_dest_callback);

	girt_flow_attrs(put_lit, flow, "id", flow->idstr);
	girt_flow_attrs(put_u64, flow, "time.start", girt_current_millis());
	girt_flow_set_sockaddr_fd(flow, "orig.addr", addr, len, fd);
	girt_flow_attrs(put_lit, flow, "listen.port", girt_lsnsock_portname(socket));
	girt_flow_attrs(put_lit_int, flow, "listen.type", girt_lsnsock_typename(socket), girt_lsnsock_type(socket));
	girt_flow_attrs(put_lit_int, flow, "listen.proto", girt_lsnsock_protoname(socket), girt_lsnsock_proto(socket));
	girt_flow_attrs(put_lit_int, flow, "listen.family", girt_lsnsock_familyname(socket), girt_lsnsock_family(socket));
	girt_flow_attrs(put_lit, flow, "listen.addr.name", girt_lsnsock_addrname(socket));

	flow->worker = worker;

	return flow;
}

void
girt_flow_free(girt_flow *flow)
{
	if (flow == NULL) {
		return;
	}

	flow_log_end(flow);

//	girt_flow_attrs(dump_fp, flow, stderr);

	for (khiter_t k = kh_begin(flow->events); k != kh_end(flow->events); ++k) {
		if (kh_exist(flow->events, k)) {
			girt_flow_event *event = kh_key(flow->events, k);
			event_free(event->ev);
			free(event);
			kh_del(events, flow->events, k); 
		}
	}
	kh_destroy(events, flow->events);

	girt_attrs_free(flow->attrs);

	free(flow);
}

static void
flow_free_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow_free((girt_flow *)arg);
}

void
girt_flow_free_async(girt_flow *flow)
{
	girt_flow_immediate(flow, flow_free_callback);
}

static void
flow_log_dest_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;

	if (flow->dest_logged) {
		return;
	}

	char *dest = girt_flow_attrs(get_str, flow, "dest.addr.name");
	if (dest != NULL) {
		girt_flow_printf(INFO, flow, "ADDR dst=%s", dest);
		flow->dest_logged = 1;
	} else {
		// re-arm
		girt_flow_attrs_notify_enable(flow, "dest.addr.name", GIRT_ATTR_PTR, flow_log_dest_callback);
	}
}

void
girt_flow_log_start(girt_flow *flow)
{
	if (flow->start_logged) {
		return;
	}

	kstring_t msg[1] = {{0}};

	ksprintf(msg, "START src=%s", 
		girt_flow_attrs(get_str, flow, "orig.addr.name")
	);

	char *dest = girt_flow_attrs(get_str, flow, "dest.addr.name");
	if (dest != NULL) {
		ksprintf(msg, " dst=%s", dest);
		flow->dest_logged = 1;
	}

	char *listen = girt_flow_attrs(get_str, flow, "listen.addr.name");
	if (!strncmp(listen, "0.0.0.0", 7)) {
		listen += 7;
	}
	ksprintf(msg, " lsn=%s/%s", listen, girt_flow_attrs(get_str, flow, "listen.proto"));

	girt_flow_print(INFO, flow, ks_str(msg));

	free(ks_release(msg));

	flow->start_logged = 1;
}

static void
flow_log_end(girt_flow *flow)
{
	girt_flow_log_start(flow);

	kstring_t msg[1] = {{0}};

	uint64_t start = girt_flow_attrs(get_u64, flow, "time.start");
	uint64_t connect = girt_flow_attrs(get_u64, flow, "time.connect");
	uint64_t end = girt_current_millis();

	if (connect == 0) {
		ksprintf(msg, "END pre=%.3lf", (end - start) / 1000.0);
	} else {
		ksprintf(msg, "END pre=%.3lf time=%.3lf", (connect - start) / 1000.0, (end - connect) / 1000.0);
	}

	uint64_t bytes_out = girt_flow_attrs(get_u64, flow, "orig.bytes-read");
	if (bytes_out == 0) {
		bytes_out = girt_flow_attrs(get_u64, flow, "dest.bytes-written");
	}

	uint64_t bytes_in = girt_flow_attrs(get_u64, flow, "dest.bytes-read");
	if (bytes_in == 0) {
		bytes_in = girt_flow_attrs(get_u64, flow, "orig.bytes-written");
	}

	ksprintf(msg, " out=%lu in=%lu", bytes_out, bytes_in);

	girt_flow_print(INFO, flow, ks_str(msg));

	free(ks_release(msg));
}

void
girt_flow_set_sockaddr_fd(girt_flow *flow, char *prefix, struct sockaddr *addr, socklen_t addrlen, int fd)
{
	girt_flow_attrs(put_ptr_len_type, flow, girt_flow_attrs(mkkey, flow, prefix, "raw"),
		girt_memdup(addr, addrlen), addrlen, GIRT_ATTR_PTR_SOCKADDR, girt_free_pp
	);
	girt_flow_attrs(put_ptr_len_type_int, flow, girt_flow_attrs(mkkey, flow, prefix, "name"),
		girt_sockaddr_to_addrname(addr, NULL, NULL), 0, GIRT_ATTR_PTR_HANDLE, fd, girt_free_pp, girt_close_p
	);
}

struct event_base *
girt_flow_event_base(girt_flow *flow)
{
	return girt_worker_event_base(flow->worker);
}

struct evdns_base *
girt_flow_evdns_base(girt_flow *flow)
{
	return girt_worker_evdns_base(flow->worker);
}

static void
flow_recallback(evutil_socket_t fd, short int what, void *arg)
{
	girt_flow_event *event = arg;
	girt_flow *flow = event->flow;
	event_callback_fn callback = event->callback;

	if (!event->reuse) {
		event_free(event->ev);
		kh_del(events, event->flow->events, kh_get(events, event->flow->events, event));
		free(event);
	}

	callback(fd, what, flow);
}

girt_flow_event *
girt_flow_schedule_flags(girt_flow *flow, char *name, struct timeval *tmo, event_callback_fn callback, int flags)
{
	girt_flow_event *event = NULL;

	if (name != NULL) {
		event = girt_flow_attrs(get_ptr, flow, name);
		if (event != NULL) {
			if (event->callback != callback) {
				girt_flow_cancel_event(flow, event);
				event = NULL;
			} else
			if (!event->reuse) {
				tmo = NULL;
			}
		}
	}

	if (event == NULL) {
		event = girt_malloc(sizeof(*event));

		event->flow = flow;
		event->callback = callback;
		event->persist = ((flags & GIRT_FLOW_EVENT_PERSIST) != 0);
		event->reuse = (event->persist || ((flags & GIRT_FLOW_EVENT_REUSE) != 0));

		event->ev = girt_check_oom_ptr(event_new(girt_flow_event_base(flow), -1, event->persist ? EV_PERSIST : 0, flow_recallback, event));

		int ret;
		kh_put(events, flow->events, event, &ret);
	}

	event_add(event->ev, tmo);

	return event;
}

void
girt_flow_cancel_event(girt_flow *flow, girt_flow_event *event)
{
	if (event == NULL) {
		return;
	}

	// check event is still valid before using it
	khiter_t k = kh_get(events, flow->events, event);
	if (k != kh_end(flow->events)) {
		event_free(event->ev);
		kh_del(events, flow->events, k);
		free(event);
	}
}

void
girt_flow_cancel_event_by_callback(girt_flow *flow, event_callback_fn callback)
{
	for (khiter_t k = kh_begin(flow->events); k != kh_end(flow->events); ++k) {
		if (kh_exist(flow->events, k)) {
			girt_flow_event *event = kh_key(flow->events, k);
			if (event->callback == callback) {
				event_free(event->ev);
				free(event);
				kh_del(events, flow->events, k); 
			}
		}
	}
}

static void
flow_attr_notifier(void *arg, struct timeval *tmo, event_callback_fn callback, char *attr_name, girt_location *loc)
{
	girt_flow *flow = arg;

	if (tmo != NULL) {
		if (loc != NULL) {
			girt_flow_printf(DEBUG, flow, "Attr '%s' triggered notify event to '%s' (%s:%d)",
				attr_name, loc->function, loc->file, loc->line
			);
		}
		girt_flow_schedule(flow, NULL, tmo, callback);
	} else {
		if (loc != NULL) {
			girt_flow_printf(DEBUG, flow, "Attr '%s' triggered direct call to '%s' (%s:%d)",
				attr_name, loc->function, loc->file, loc->line
			);
		}
		callback(-1, 0, flow);
	}
}

char *
girt_flow_idstr(girt_flow *flow)
{
	return flow->idstr;
}

girt_attrs *
girt_flow_get_attrs(girt_flow *flow)
{
	return flow->attrs;
}

girt_worker *
girt_flow_worker(girt_flow *flow)
{
	return flow->worker;
}

void
girt_flow_dump(girt_flow *flow)
{
	girt_str_vec *vec = girt_str_vec_init();

	girt_attrs_dump(flow->attrs, vec);

	for (size_t i = 0; i < kv_size(*vec); i++) {
		girt_flow_print(DEBUG, flow, kv_A(*vec, i));
	}

	girt_str_vec_free(vec);
}


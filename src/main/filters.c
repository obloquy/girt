#include <girt/girt.h>

#include <private/filters.h>

girt_filters *
girt_filters_init(girt_proxy *proxy, girt_cfg *filters_config)
{
	girt_enter();
	girt_filters *filters = girt_malloc(sizeof(*filters));
	defer { girt_if_fail { girt_filters_free(filters); } }

	filters->proxy = proxy;

	kv_init(filters->filters);

	// @config <filters> chain <filter>...
	// @config-desc An array of filters through which data will be processed in turn
	girt_cfg_vec *filter_configs = girt_config_get_table_array(filters_config, "chain");
	defer { girt_config_free_table_array(filter_configs); }
	if (kv_size(*filter_configs) == 0) {
		girt_print(ERROR, "Missing or empty chain for filters\n");
		girt_fail(NULL);
	}

	for (size_t i = 0; i < kv_size(*filter_configs); i++) {
		girt_cfg *filter_config = kv_A(*filter_configs, i);
		// @config <filter> type string
		// @config-desc The type for each filter in the chain which will be used as the plug-in name
		char *type = girt_config_get_string(filter_config, "type", NULL);
		defer { free(type); }
		if (type == NULL) {
			girt_print(ERROR, "No type for filter\n");
			girt_fail(NULL);
		}
		kv_push(girt_filter *, filters->filters, girt_filter_init(proxy, filter_config, type));
	}

	girt_return(filters);
}

void
girt_filters_free(girt_filters *filters)
{
	if (filters == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(filters->filters); i++) {
		girt_filter_free(kv_A(filters->filters, i));
	}

	kv_destroy(filters->filters);

	memset(filters, 0, sizeof(*filters));

	free(filters);
}

girt_link *
girt_filters_flow_start(girt_filters *filters, girt_link *last_hop)
{
	girt_link *next_hop = last_hop;

	if (filters != NULL) {
		for (ssize_t i = (ssize_t)kv_size(filters->filters)-1; i >= 0; i--) {
			next_hop = girt_filter_flow_start(kv_A(filters->filters, i), next_hop);
		}
	}

	return next_hop;
}

girt_proxy *
girt_filters_proxy(girt_filters *filters)
{
	return filters->proxy;
}

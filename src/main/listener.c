#include <girt/girt.h>

#include <private/listener.h>

PLUGIN_FUNCS(
		listener,
		{
			girt_plugin_context *(*init)(girt_listener *listener, girt_cfg *listener_config);
			void *(*listen)(void *context);
			void (*free)(girt_plugin_context *context);
		},
		"init", "listen", "free"
)

static void *listener_callback(void *arg);

girt_listener *
girt_listener_init(girt_proxy *proxy, girt_cfg *listener_config)
{
	girt_enter();
	girt_listener *listener = girt_malloc(sizeof(*listener));
	listener->pthread_result = -1;
	defer { girt_if_fail { girt_listener_free(listener); } }

	listener->proxy = proxy;
	listener->evbase = girt_check_oom_ptr(event_base_new());

	girt_lsnsock *s = listener->socket = girt_lsnsock_init(listener, listener_config);
	if (s == NULL) {
		girt_fail(NULL);
	}

	listener->plugin = girt_plugins_load_listener(girt_root_plugins(girt_proxy_root(proxy)), girt_lsnsock_typename(s));
	if (listener->plugin == NULL) {
		girt_loc_print(ERROR, girt_listener_name(listener), "Failed to load listener\n");
		girt_fail(NULL);
	}
	listener->context = girt_plugin_listener_funcs(listener->plugin)->init(listener, listener_config);
	if (listener->context == NULL) {
		girt_loc_print(ERROR, girt_listener_name(listener), "Failed to initialise listener\n");
		girt_fail(NULL);
	}

	// @config <listener> thread <pthread_attr>
	// @config-desc The thread attributes for the listener
	pthread_attr_t *attr = girt_config_get_pthread_attr(listener_config, "thread");
	defer { girt_config_free_pthread_attr(attr); }

	int err = listener->pthread_result = pthread_create(&listener->tid, attr, listener_callback, listener);
	if (err != 0) {
		girt_loc_printf(ERROR, girt_listener_name(listener), "Failed to start listener: %s\n", strerror(err));
		girt_fail(NULL);
	}

	if (girt_is_priority_enabled(DEBUG)) {
		girt_str_vec *vec = girt_str_vec_init();
		girt_str_vec_printf(vec, "Started listener with the following attributes:\n");
		girt_limits_dump_pthread_attr_for_tid(vec, listener->tid);
		for (size_t i = 0; i < kv_size(*vec); i++) {
			girt_loc_print(DEBUG, girt_listener_name(listener), kv_A(*vec, i));
		}
		girt_str_vec_free(vec);
	}

	return listener;
}

void
girt_listener_free(girt_listener *listener)
{
	if (listener == NULL) {
		return;
	}

	if (listener->pthread_result == 0) {
		// started, try to stop it
		if (listener->evbase != NULL) {
			event_base_loopbreak(listener->evbase);
		}

		void *result;
		int err = pthread_join(listener->tid, &result);
		if (err != 0) {
			girt_printf(ERROR, "Failed to join thread '%s': %s", listener->tname, strerror(err));
			if (err == ESRCH) {
				// not found
				listener->pthread_result = 0;
			}
		} else {
			girt_printf(DEBUG, "Joined thread '%s'", listener->tname);
		}
	} else {
		// never started
		listener->pthread_result = 0;
	}

	// thread successfully stopped or was never started
	if (listener->pthread_result == 0) {

		if (listener->plugin != NULL && listener->context != NULL) {
			girt_plugin_listener_funcs(listener->plugin)->free(listener->context);
		}

		if (listener->evbase != NULL) {
			event_base_free(listener->evbase);
		}

		girt_lsnsock_free(listener->socket);

		memset(listener, 0, sizeof(*listener));
		free(listener);
	}
}

static void *
listener_callback(void *arg)
{
	girt_listener *listener = arg;
	girt_lsnsock *s = listener->socket;

	snprintf(listener->tname, sizeof(listener->tname), "%s-%s", girt_lsnsock_protoname(s), girt_lsnsock_portname(s));
	prctl(PR_SET_NAME, listener->tname);

	return girt_plugin_listener_funcs(listener->plugin)->listen(listener->context);
}

girt_lsnsock *
girt_listener_socket(girt_listener *listener)
{
	return listener->socket;
}

struct event_base *
girt_listener_event_base(girt_listener *listener)
{
	return listener->evbase;
}

char *
girt_listener_name(girt_listener *listener)
{
	return girt_proxy_name(listener->proxy);
}

girt_flow *
girt_listener_flow_init(girt_listener *listener, int fd, struct sockaddr *addr, socklen_t len)
{
	return girt_proxy_flow_init(listener->proxy, fd, addr, len, listener->socket);
}

void
girt_listener_flow_free_direct(UNUSED girt_listener *listener, girt_flow *flow, int direct)
{
	girt_proxy_flow_free_direct(listener->proxy, flow, direct);
}

void
girt_listener_flow_start(UNUSED girt_listener *listener, girt_flow *flow)
{
	girt_proxy_flow_start(listener->proxy, flow);
}

void
girt_listener_flow_send(UNUSED girt_listener *listener, girt_flow *flow, struct evbuffer *datagram)
{
	girt_proxy_flow_send(listener->proxy, flow, datagram);
}

#include <girt/girt.h>

#include <private/logtemplate.h>

static template_tokens *tokens_init(char *template_str, int (*tokenize_external)(char *name));
static void tokens_free(template_tokens *tokens);
static char *template_token_default_format(template_token_type type);

girt_logtemplate *
girt_logtemplate_init(char *template_str,
		size_t (*render_external)(char *buf, size_t buflen, int token_type, char *format, const log4c_logging_event_t *event),
		int (*tokenize_external)(char *name)
)
{
	girt_logtemplate *template = girt_malloc(sizeof(*template));

	template->tokens = tokens_init(template_str, tokenize_external);
	template->render_external = render_external;

	return template;
}

void
girt_logtemplate_free(girt_logtemplate *template)
{
	if (template == NULL) {
		return;
	}

	tokens_free(template->tokens);

	memset(template, 0, sizeof(*template));

	free(template);
}

size_t
girt_logtemplate_render_event(girt_logtemplate *template, char *buf, size_t buflen, const log4c_logging_event_t *event)
{
	template_tokens *tokens = template->tokens;

	buf[0] = '\0';
	size_t len = 0;

	if (tokens == NULL) {
		return 0;
	}

	for (size_t i = 0; i < kv_size(*tokens); i++) {
		template_token *token = kv_A(*tokens, i);
		char *format = token->format ? token->format : template_token_default_format(token->type);
		switch (token->type) {
		case TOKEN_UNKNOWN:
			len += snprintf(buf+len, buflen-len, "???");
			break;
		case TOKEN_EXTERNAL:
			if (template->render_external != NULL) {
				len += template->render_external(buf+len, buflen-len, token->external_type, token->format, event);
			}
			break;
		case TOKEN_LITERAL:
			len += snprintf(buf+len, buflen-len, "%s", format);
			break;
		case TOKEN_MESSAGE:
			len += snprintf(buf+len, buflen-len, "%s", event->evt_msg);
			break;
		case TOKEN_LOCALTIME: {
			time_t t = event->evt_timestamp.tv_sec;
			struct tm tm;
			localtime_r(&t, &tm);
			len += strftime(buf+len, buflen-len, format, &tm);
			break;
		}
		case TOKEN_MSEC:
			len += snprintf(buf+len, buflen-len, format, event->evt_timestamp.tv_usec / 1000);
			break;
		case TOKEN_PRIORITY_NAME:
			len += snprintf(buf+len, buflen-len, format, log4c_priority_to_string(event->evt_priority));
			break;
		case TOKEN_FILE:
			len += snprintf(buf+len, buflen-len, format, event->evt_loc->loc_file);
			break;
		case TOKEN_LINE:
			len += snprintf(buf+len, buflen-len, format, event->evt_loc->loc_line);
			break;
		case TOKEN_FUNCTION:
			len += snprintf(buf+len, buflen-len, format, event->evt_loc->loc_function);
			break;
		case TOKEN_THREAD_NAME: {
			char tname[16];
			if (prctl(PR_GET_NAME, tname) != 0) {
				snprintf(tname, sizeof(tname), "%lu", pthread_self());
			}
			len += snprintf(buf+len, buflen-len, format, tname);
			break;
		}
		case TOKEN_THREAD_ID:
			len += snprintf(buf+len, buflen-len, format, pthread_self());
			break;
		case TOKEN_LOCATION_NAME:
			len += snprintf(buf+len, buflen-len, format, event->evt_loc->loc_data == NULL ? "-" : event->evt_loc->loc_data);
			break;
		}
	}

	return len;
}

static template_token_type
template_token_name_to_type(char *name)
{
	if (!strcmp(name, "localtime")) {
		return TOKEN_LOCALTIME;
	}
	if (!strcmp(name, "msec")) {
		return TOKEN_MSEC;
	}
	if (!strcmp(name, "priority-name")) {
		return TOKEN_PRIORITY_NAME;
	}
	if (!strcmp(name, "file")) {
		return TOKEN_FILE;
	}
	if (!strcmp(name, "line")) {
		return TOKEN_LINE;
	}
	if (!strcmp(name, "function")) {
		return TOKEN_FUNCTION;
	}
	if (!strcmp(name, "thread-name")) {
		return TOKEN_THREAD_NAME;
	}
	if (!strcmp(name, "thread-id")) {
		return TOKEN_THREAD_ID;
	}
	if (!strcmp(name, "location-name")) {
		return TOKEN_LOCATION_NAME;
	}
	if (!strcmp(name, "message")) {
		return TOKEN_MESSAGE;
	}

	return TOKEN_UNKNOWN;
}

static char *
template_token_default_format(template_token_type type)
{
	switch (type) {
	case TOKEN_UNKNOWN:
	case TOKEN_EXTERNAL:
	case TOKEN_LITERAL:
	case TOKEN_MESSAGE:
	case TOKEN_PRIORITY_NAME:
	case TOKEN_FILE:
	case TOKEN_FUNCTION:
	case TOKEN_THREAD_NAME:
	case TOKEN_LOCATION_NAME:
		return "%s";
	case TOKEN_LOCALTIME:
		return "%F %T";
	case TOKEN_MSEC:
		return "%03d";
	case TOKEN_LINE:
	case TOKEN_THREAD_ID:
		return "%lu";
		break;
	}

	return "???";
}

static template_tokens *
tokens_init(char *template_str, int (*tokenize_external)(char *name))
{
	template_tokens *tokens = girt_malloc(sizeof(*tokens));

	kv_init(*tokens);

	char *ptr;
	for (char *t = strtok_r(template_str, "}", &ptr); t != NULL; t = strtok_r(NULL, "}", &ptr)) {
		char *lbrace = strchr(t, '{');
		if (lbrace != NULL) {
			*lbrace = '\0';
		}
		if (strlen(t)) {
			template_token *token = girt_malloc(sizeof(*token));
			token->type = TOKEN_LITERAL;
			token->format = girt_strdup(t);
			kv_push(template_token *, *tokens, token);
		}
		if (lbrace != NULL) {
			char *name = lbrace+1;
			template_token *token = girt_malloc(sizeof(*token));
			char *colon = strchr(name, ':');
			if (colon != NULL) {
				token->format = girt_strdup(colon+1);
				*colon = '\0';
			}
			if ((token->type = template_token_name_to_type(name)) == TOKEN_UNKNOWN && tokenize_external != NULL) {
				if ((token->external_type = tokenize_external(name)) != TOKEN_UNKNOWN) {
					token->type = TOKEN_EXTERNAL;
				}
			}
			kv_push(template_token *, *tokens, token);
		}
	}

	return tokens;
}

static void
tokens_free(template_tokens *tokens)
{
	for (size_t i = 0; i < kv_size(*tokens); i++) {
		template_token *token = kv_A(*tokens, i);
		free(token->format);
		free(token);
	}

	kv_destroy(*tokens);

	memset(tokens, 0, sizeof(*tokens));
	free(tokens);
}

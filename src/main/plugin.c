#include <girt/girt.h>

#include <private/plugin.h>

static char *subdirs[] = {
	"%s/lib/plugins/%s/.libs/%s.so",
	"%s/lib/plugins/%s/%s.so",
	"%s/lib/%s/.libs/%s.so",
	"%s/lib/%s/%s.so",
	"%s/%s/.libs/%s.so",
	"%s/%s/%s.so",
	"%s/src/%s/.libs/%s.so",
	"%s/src/%s/%s.so",
	NULL
};

girt_plugin *
girt_plugin_init(girt_root *root, const char *name, const char *type, const char * const funcnames[], const int n)
{
	girt_enter();
	girt_plugin *plugin = girt_malloc(sizeof(*plugin));
	defer { girt_if_fail { girt_plugin_free(plugin); } }

	plugin->functions = girt_malloc(n*sizeof(girt_plugin_fns));

	for (int i = 0; subdirs[i] != NULL; i++) {
		char *sub = subdirs[i];
		plugin->filename = girt_asprintf(sub, girt_config_home(girt_root_config(root)), type, name);
		if (access(plugin->filename, X_OK) == 0) {
			break;
		}
		girt_free_pp((void **)&plugin->filename);
	}
	if (plugin->filename == NULL) {
		girt_printf(ERROR, "Failed to find executable plugin '%s/%s.so'\n", type, name);
		girt_fail(NULL);
	}

	plugin->type = girt_strdup(type);
	plugin->name = girt_strdup(name);

	plugin->dl = dlopen(plugin->filename, RTLD_NOW);
	char *error = dlerror();
	if (plugin->dl == NULL) {
		if (error) {
			girt_printf(ERROR, "Failed to load plugin '%s': %s\n", plugin->filename, error);
		} else {
			girt_printf(ERROR, "Failed to load plugin '%s'\n", plugin->filename);
		}
		girt_fail(NULL);
	}

	for (int i = 0; i < n; i++) {
		char *symname = girt_asprintf("_girt_%s_%s", type, funcnames[i]);
		// This makes more sense:
		// plugin->functions[i] = dlsym(plugin->dl, symname);
		// But ISO/C99 wants this:
		*(void **) (&plugin->functions[i]) = dlsym(plugin->dl, symname);

		char *error = dlerror();
		if (plugin->functions[i] == NULL) {
			girt_printf(ERROR, "Invalid plugin: %s\n", (error != NULL ? error : plugin->filename));
			girt_fail(NULL);
		}
		free(symname);
	}

	girt_return(plugin);
}

void
girt_plugin_free(girt_plugin *plugin)
{
	if (plugin == NULL) {
		return;
	}

	dlclose(plugin->dl);
	dlerror();

	free(plugin->filename);
	free(plugin->functions);
	free(plugin->name);
	free(plugin->type);

	memset(plugin, 0, sizeof(*plugin));
	free(plugin);
}

girt_plugin_fns *
girt_plugin_funcs(girt_plugin *plugin)
{
	return plugin->functions;
}

char *
girt_plugin_type(girt_plugin *plugin)
{
	return plugin->type;
}

char *
girt_plugin_name(girt_plugin *plugin)
{
	return plugin->name;
}


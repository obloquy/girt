#include <girt/girt.h>

#include <private/appenders.h>

girt_appenders *
girt_appenders_init(girt_log *logger, girt_cfg *logger_config)
{
	girt_enter();
	girt_appenders *appenders = girt_malloc(sizeof(*appenders));
	defer { girt_if_fail { girt_appenders_free(appenders); } }

	kv_init(appenders->appenders);

	// @config <logger> appender.<name> <appender>
	// @config-desc Configuration table for the named appender
	girt_cfg_vec *appender_configs = girt_config_get_subtables(logger_config, "appender");
	defer { girt_config_free_subtables(appender_configs); }

	for (size_t i = 0; i < kv_size(*appender_configs); i++) {
		girt_cfg *appender_config = kv_A(*appender_configs, i);
		char *name = girt_config_get_table_name(appender_config);
		defer { free(name); }
		girt_appender *appender = girt_appender_init(logger, appender_config, name);
		if (appender == NULL) {
			girt_fail(NULL);
		}
		kv_push(girt_appender *, appenders->appenders, appender);
	}

	return appenders;
}

void
girt_appenders_free(girt_appenders *appenders)
{
	if (appenders == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(appenders->appenders); i++) {
		girt_appender_free(kv_A(appenders->appenders, i));
	}

	kv_destroy(appenders->appenders);

	memset(appenders, 0, sizeof(*appenders));

	free(appenders);
}

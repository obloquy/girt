#include <girt/girt.h>

#include <private/proxy.h>

PLUGIN_FUNCS(
		proxy,
		{
			girt_plugin_context *(*init)(girt_proxy *proxy, girt_cfg *proxy_config);
			girt_wrk *(*workers)(girt_plugin_context *context);
			void (*flow_start)(girt_plugin_context *context, girt_flow *flow);
			void (*flow_send)(girt_plugin_context *context, girt_flow *flow, struct evbuffer *datagram);
			void (*free)(girt_plugin_context *context);
		},
		"init", "workers", "flow_start", "flow_send", "free"
)

girt_proxy *
girt_proxy_init(girt_root *root, girt_cfg *proxy_config, char *type)
{
	girt_enter();
	girt_proxy *proxy = girt_malloc(sizeof(*proxy));
	defer { girt_if_fail { girt_proxy_free(proxy); } }

	proxy->root = root;

	proxy->name = girt_config_get_table_name(proxy_config);

	// @config <proxy> listener <listener>
 	girt_cfg *listener_config = girt_config_get_table(proxy_config, "listener");
	defer { girt_config_free_table(listener_config); }
	if (girt_config_root_table_count(listener_config) <= 0) {
		girt_loc_print(ERROR, proxy->name, "No listener configured");
		girt_fail(NULL);
	}

	proxy->listener = girt_listener_init(proxy, listener_config);
	if (proxy->listener == NULL) {
		girt_fail(NULL);
	}

	proxy->plugin = girt_plugins_load_proxy(girt_root_plugins(root), type);
	if (proxy->plugin == NULL) {
		girt_printf(ERROR, "Failed to load proxy '%s'\n", proxy->name);
		girt_fail(NULL);
	}

	proxy->context = girt_plugin_proxy_funcs(proxy->plugin)->init(proxy, proxy_config);
	if (proxy->context == NULL) {
		girt_printf(ERROR, "Failed to initialise proxy '%s'\n", proxy->name);
		girt_fail(NULL);
	}

	return proxy;
}

void
girt_proxy_free(girt_proxy *proxy)
{
	if (proxy == NULL) {
		return;
	}

	girt_listener_free(proxy->listener);

	if (proxy->plugin != NULL && proxy->context != NULL) {
		girt_plugin_proxy_funcs(proxy->plugin)->free(proxy->context);
	}

	free(proxy->name);

	memset(proxy, 0, sizeof(*proxy));
	free(proxy);
}

char *
girt_proxy_name(girt_proxy *proxy)
{
	return proxy->name;
}

girt_listener *
girt_proxy_listener(girt_proxy *proxy)
{
	return proxy->listener;
}

girt_flow *
girt_proxy_flow_init(girt_proxy *proxy, int fd, struct sockaddr *addr, socklen_t len, girt_lsnsock *listen)
{
	girt_flow *flow = girt_flow_init(fd, addr, len, listen, girt_workers_next(girt_proxy_workers(proxy)));

	girt_flow_attrs(put_lit, flow, "proxy.name", proxy->name);

	if (girt_lsnsock_type(listen) == SOCK_DGRAM) {
		girt_flow_attrs(set_tag, flow, "datagram");
	}

	girt_flow_log_start(flow);

	return flow;
}

void
girt_proxy_flow_free_direct(UNUSED girt_proxy *proxy, girt_flow *flow, int direct)
{
	if (direct) {
		girt_flow_free(flow);
	} else {
		girt_flow_free_async(flow);
	}
}

void
girt_proxy_flow_start(girt_proxy *proxy, girt_flow *flow)
{
	girt_plugin_proxy_funcs(proxy->plugin)->flow_start(proxy->context, flow);
}

void
girt_proxy_flow_send(girt_proxy *proxy, girt_flow *flow, struct evbuffer *datagram)
{
	girt_plugin_proxy_funcs(proxy->plugin)->flow_send(proxy->context, flow, datagram);
}

girt_wrk *
girt_proxy_workers(girt_proxy *proxy)
{
	girt_wrk *workers = girt_plugin_proxy_funcs(proxy->plugin)->workers(proxy->context);

	return workers != NULL ? workers : girt_root_workers(proxy->root);
}

girt_root *
girt_proxy_root(girt_proxy *proxy)
{
	return proxy->root;
}

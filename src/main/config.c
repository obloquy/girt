#include <girt/girt.h>

#include <private/config.h>

static int config_load_dir(girt_cfg *config, char *dirname);
static int config_load(girt_cfg *config, char *filename);
static const char *config_raw_in(girt_cfg *config, char *key);
static toml_array_t *config_array_in(girt_cfg *config, char *key);

girt_cfg *
girt_config_init(girt_root *root)
{
	girt_enter();
	girt_cfg *config = girt_malloc(sizeof(*config));
	defer { girt_if_fail { girt_config_free(config); } }

	char *home = getenv("GIRT_HOME");
	if (home != NULL) {
		config->home = girt_strdup(home);
	} else {
		config->home = get_current_dir_name();
	}

	if (chdir(config->home) != 0) {
		girt_printf(ERROR, "Failed to change home directory '%s': %s\n", config->home, strerror(errno));
		girt_fail(NULL);
	}
	girt_printf(INFO, "Home directory is '%s'\n", config->home);

	kv_init(config->toml);

	// Apply minimal logger config
    char errbuf[200];
	toml_table_t *logger = toml_parse(
		"[logger.category.flow]\n"
		"[logger.category.girt]\n"
		"[logger.formatter.flow]\n"
		"[logger.formatter.girt]\n"
		"[logger.appender.flow]\n"
		"[logger.appender.girt]\n",
		errbuf, sizeof(errbuf)
	);
	if (logger == NULL) {
		girt_printf(ERROR, "Failed to parse minimal logger config: %s\n", errbuf);
		girt_fail(NULL);
	}
	kv_push(toml_table_t *, config->toml, logger);

	girt_str_vec *args = girt_root_args(root);
	for (size_t i = 1; i < kv_size(*args); i++) {
		if (config_load(config, kv_A(*args, i))) {
			girt_fail(NULL);
		}
	}

	if (config == NULL || kv_size(config->toml) == 0) {
		girt_print(ERROR, "Usage: " GIRT_NAME " <toml_config_file> [<toml_config_file>]...\n"
						"\tAt least one config file must be provided, there is no default.\n"
						"\tLater config files will override earlier ones, except for arrays of\n"
						"\ttables (e.g. [[foo]]) which are cumulative. Directories are allowed\n"
						"\tand files found in each will be processed in alpha order\n");
		girt_fail(NULL);
	}

	girt_return(config);
}

void
girt_config_free(girt_cfg *config)
{
	if (config == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		toml_free(kv_A(config->toml, i));
	}

	free(config->home);
	kv_destroy(config->toml);

	memset(config, 0, sizeof(*config));

	free(config);
}

static int
string_compare(const void *a, const void *b)
{
	char * const *astr = a;
	char * const *bstr = b;

	return strcmp(*astr, *bstr);
}

static int
config_load_dir(girt_cfg *config, char *dirname)
{
	girt_enter();
	DIR *d = opendir(dirname);
	if (d == NULL) {
		girt_printf(ERROR, "Failed to open config directory '%s': %s\n", dirname, strerror(errno));
		girt_fail(1);
	}
	defer { closedir(d); }

	kvec_t(char *) filenames;
	kv_init(filenames);

	struct dirent *dir;
	while ((dir = readdir(d)) != NULL) {
		char filename[strlen(dirname) + 1 + strlen(dir->d_name) + 1];
		snprintf(filename, sizeof(filename), "%s/%s", dirname, dir->d_name);
		char *dot;
		if ((dot = strrchr(dir->d_name, '.')) != NULL && (!strcmp(dot, ".toml") || !strcmp(dot, ".conf"))) {
			struct stat st[1];
			if (stat(filename, st) >= 0 && S_ISREG(st->st_mode)) {
				kv_push(char *, filenames, girt_strdup(filename));
			}
		}
	}

	qsort(&kv_A(filenames, 0), kv_size(filenames), sizeof(char *), string_compare);

	for (size_t i = 0; i < kv_size(filenames); i++) {
		config_load(config, kv_A(filenames, i));
		free(kv_A(filenames, i));
	}

	kv_destroy(filenames);

	girt_return(0);
}

static int
config_load(girt_cfg *config, char *filename)
{
	girt_enter();
	struct stat st[1];
	if (stat(filename, st) < 0) {
		girt_printf(ERROR, "Failed to stat config file '%s': %s\n", filename, strerror(errno));
		girt_fail(1);
	} else
	if(S_ISDIR(st->st_mode)) {
		int ret = config_load_dir(config, filename);
		if (ret) {
			girt_fail(ret);
		} else {
			girt_return(0);
		}
	}

	/* open file and parse */
	FILE *fp = fopen(filename, "r");
	if (fp == NULL) {
		girt_printf(ERROR, "Failed to open config file '%s': %s\n", filename, strerror(errno));
		girt_fail(1);
	}
	defer { fclose(fp); }

	girt_printf(DEBUG, "Loading config from '%s'\n", filename);

    char errbuf[200];
	toml_table_t *toml = toml_parse_file(fp, errbuf, sizeof(errbuf));

	if (toml == NULL) {
		girt_printf(ERROR, "Failed to parse config file '%s': %s\n", filename, errbuf);
		girt_fail(1);
	}

	kv_push(toml_table_t *, config->toml, toml);

	girt_return(0);
}

char *
girt_config_home(girt_cfg *config)
{
	return config->home != NULL ? config->home : ".";
}

int
girt_config_get_boolean(girt_cfg *config, char *key, int def)
{
	int value = def;

	const char* raw = config_raw_in(config, key);
	if (raw != NULL && toml_rtob(raw, &value)) {
		girt_printf(ERROR, "Config key '%s' has a bad boolean value: %s\n", key, raw);
	}

	return value;
}

int64_t
girt_config_get_i64(girt_cfg *config, char *key, int64_t def)
{
	int64_t value = def;

	const char* raw = config_raw_in(config, key);
	if (raw != NULL && toml_rtoi(raw, &value)) {
		girt_printf(ERROR, "Config key '%s' has a bad integer value: %s\n", key, raw);
	}

	return value;
}

char *
girt_config_get_string(girt_cfg *config, char *key, const char *def)
{
	char *str = NULL;

	const char* raw = config_raw_in(config, key);
	if (raw != NULL && toml_rtos(raw, &str)) {
		str = girt_strdup(raw);
	}

	if (str == NULL && def != NULL) {
		str = girt_strdup(def);
	}

	return str;
}

girt_int_vec *
girt_config_get_i64_array(girt_cfg *config, char *key)
{
	girt_int_vec *array = girt_malloc(sizeof(*array));

	kv_init(*array);

	toml_array_t *toml = config_array_in(config, key);

	if (toml != NULL) {
		for (int i = 0; i < toml_array_nelem(toml); i++) {
			const char *raw = toml_raw_at(toml, i);
			int64_t val;
			if (raw != NULL && toml_rtoi(raw, &val)) {
				girt_printf(ERROR, "Config key '%s.%d' has a bad value: %s\n", key, i, raw);
			} else {
				kv_push(int64_t, *array, val);
			}
		}
	}

	return array;
}

void
girt_config_free_i64_array(girt_int_vec *array)
{
	if (array == NULL) {
		return;
	}

	kv_destroy(*array);

	free(array);
}

girt_str_vec *
girt_config_get_string_array(girt_cfg *config, char *key)
{
	girt_str_vec *array = girt_str_vec_init();

	toml_array_t *toml = config_array_in(config, key);

	if (toml != NULL) {
		for (int i = 0; i < toml_array_nelem(toml); i++) {
			const char *raw = toml_raw_at(toml, i);
			char *str = NULL;
			if (raw != NULL && toml_rtos(raw, &str)) {
				str = girt_strdup(raw);
			}
			kv_push(char *, *array, str);
		}
	}

	return array;
}

void
girt_config_free_string_array(girt_str_vec *array)
{
	girt_str_vec_free(array);
}

uint64_t
girt_config_get_millis(girt_cfg *config, char *key, char *def)
{
	uint64_t millis = 0U;

	char *cfgstr = (char *)girt_config_get_string(config, key, def);

	if (cfgstr != NULL) {
		char *pstr = cfgstr;
		while (*pstr) {
			while (*pstr && isspace(*pstr)) {
				pstr += 1;
			}
			if (*pstr) {
				char *endptr = NULL;
				double v = strtod(pstr, &pstr);
				while (*pstr && isspace(*pstr)) {
					pstr += 1;
				}
				if (*pstr) {
					endptr = pstr;
					while (*endptr && !isspace(*endptr) && !isdigit(*endptr)) {
						endptr += 1;
					}
					size_t len = endptr - pstr;
					if (!strncmp(pstr, "s", len) || !strncmp(pstr, "sec", len) || !strncmp(pstr, "secs", len)) {
						v *= 1000;
					} else
					if (!strncmp(pstr, "m", len) || !strncmp(pstr, "min", len) || !strncmp(pstr, "mins", len)) {
						v *= 60 * 1000;
					} else
					if (!strncmp(pstr, "h", len) || !strncmp(pstr, "hour", len) || !strncmp(pstr, "hours", len)) {
						v *= 60 * 60 * 1000;
					} else
					if (!strncmp(pstr, "d", len) || !strncmp(pstr, "day", len) || !strncmp(pstr, "days", len)) {
						v *= 24 * 60 * 60 * 1000;
					} else {
						v = 0;
					}
					pstr = endptr;
				}

				millis += v;
			}
		}
	}

	free(cfgstr);

	return millis;
}

struct timeval *
girt_config_get_timeval(girt_cfg *config, char *key, char *def)
{
	struct timeval *value = girt_malloc(sizeof(*value));

	uint64_t millis = girt_config_get_millis(config, key, def);

	value->tv_sec = (time_t)millis / 1000;
	value->tv_usec = ((suseconds_t)millis % 1000) * 1000;

	return value;
}

void
girt_config_free_timeval(struct timeval *tv)
{
	free(tv);
}

struct addrinfo *
girt_config_get_addrinfo(girt_cfg *config, char *name, int passive, int def_family, char *def_addr, char *def_port)
{
	girt_enter();
	struct addrinfo *addrinfo = NULL;

	girt_cfg *_addr_config = NULL;
	defer { girt_config_free_table(_addr_config); }
	girt_cfg *addr_config = config;
	if (name != NULL && *name != '\0') {
		_addr_config = addr_config = girt_config_get_table(config, name);
	}

	if (girt_config_root_table_count(addr_config) > 0) {
		// @config <addrinfo> host string
		// @config-desc A hostname which may be a numeric IP address
		char *host = girt_config_get_string(addr_config, "host", NULL);
		defer { free(host); }
		// @config <addrinfo> address string
		// @config-desc An IP address which must not be a hostname
		char *address = girt_config_get_string(addr_config, "address", def_addr);
		defer { free(address); }
		// @config <addrinfo> port i64
		// @config-desc A port number which must not be a service name
		char *port = girt_config_get_string(addr_config, "port", def_port);
		defer { free(port); }
		struct addrinfo hints = {
			.ai_family = def_family,
			.ai_flags = (passive ? AI_PASSIVE : 0) | (host == NULL ? AI_NUMERICSERV|AI_NUMERICHOST : 0) | AI_ADDRCONFIG,
		};

		int gai_status = getaddrinfo(host == NULL ? address : host, port, &hints, &addrinfo);
		if (gai_status != 0) {
			girt_printf(ERROR, "Failed to parse address '%s' and port '%s': %s\n", host, port, gai_strerror(gai_status));
			girt_fail(NULL);
		}
	}

	girt_return(addrinfo);
}

void
girt_config_free_addrinfo(struct addrinfo *ai)
{
	if (ai == NULL) {
		return;
	}

	freeaddrinfo(ai);
}

struct sockaddr *
girt_config_get_sockaddr(girt_cfg *config, char *name, int passive, int def_family, char *def_addr, char *def_port, socklen_t *addrlen)
{
	struct sockaddr *addr = NULL;
	// @config <sockaddr> - <addrinfo>
	struct addrinfo *addrinfo = girt_config_get_addrinfo(config, name, passive, def_family, def_addr, def_port);

	if (addrinfo != NULL) {
		addr = girt_memdup(addrinfo->ai_addr, addrinfo->ai_addrlen);
		if (addrlen != NULL) {
			*addrlen = addrinfo->ai_addrlen;
		}

		girt_config_free_addrinfo(addrinfo);
	}

	return addr;
}

void
girt_config_free_sockaddr(struct sockaddr *sa)
{
	free(sa);
}

pthread_attr_t *
girt_config_get_pthread_attr(girt_cfg *config, char *name)
{
	girt_enter();
	pthread_attr_t *attr = girt_malloc(sizeof(*attr));
	defer { girt_if_fail { girt_config_free_pthread_attr(attr); } }

	int err = pthread_attr_init(attr);
	if (err != 0) {
		girt_printf(ERROR, "Failed to initialise pthread attrs: %s\n", strerror(err));
		girt_fail(NULL);
	}

	girt_cfg *_thread_config = NULL;
	defer { girt_config_free_table(_thread_config); }
	girt_cfg *thread_config = config;
	if (name != NULL && *name != '\0') {
		_thread_config = thread_config = girt_config_get_table(config, name);
		if (girt_config_root_table_count(thread_config) == 0) {
			girt_return(attr);
		}
	}

	// @config <pthread_attr> scheduler string "other"
	// @config-desc The scheduling policy for a thread ("other" - Normal round robin, "rr" - Realtime round robin, or "fifo")
	char *sched = girt_config_get_string(thread_config, "scheduler", NULL);
	defer { free(sched); }
	if (sched != NULL) {
		if (!strcmp(sched, "other")) {
			err = pthread_attr_setschedpolicy(attr, SCHED_OTHER);
			if (err != 0) {
				girt_printf(ERROR, "Failed to set scheduler to OTHER in pthread attrs: %s\n", strerror(err));
				girt_fail(NULL);
			}
		} else
		if (!strcmp(sched, "rr") || !strcmp(sched, "roundrobin")) {
			err = pthread_attr_setschedpolicy(attr, SCHED_RR);
			if (err != 0) {
				girt_printf(ERROR, "Failed to set scheduler to RR in pthread attrs: %s\n", strerror(err));
				girt_fail(NULL);
			}
		} else
		if (!strcmp(sched, "fifo")) {
			err = pthread_attr_setschedpolicy(attr, SCHED_FIFO);
			if (err != 0) {
				girt_printf(ERROR, "Failed to set scheduler to FIFO in pthread attrs: %s\n", strerror(err));
				girt_fail(NULL);
			}
		} else {
			girt_printf(ERROR, "Unknown thread scheduler for pthread attrs: %s\n", sched);
			girt_fail(NULL);
		}
		err = pthread_attr_setinheritsched(attr, PTHREAD_EXPLICIT_SCHED);
		if (err != 0) {
			girt_printf(ERROR, "Failed to set explicit scheduling in pthread attrs: %s\n", strerror(err));
			girt_fail(NULL);
		}
	}

	// @config <pthread_attr> priority i64 0 for "other, 1 for "rr" and "fifo"
	// @config-desc Scheduling priority for a thread in the range 1 (low) to 99 (high)
	int prio = girt_config_get_i64(thread_config, "priority", (sched == NULL || !strcmp(sched, "other")) ? 0 : 1);
	if (prio != 0) {
		struct sched_param params = {
			.sched_priority = prio
		};
		err = pthread_attr_setschedparam(attr, &params);
		if (err != 0) {
			girt_printf(ERROR, "Failed to set priority to %d in pthread attrs: %s\n", prio, strerror(err));
			girt_fail(NULL);
		}
	}

	// @config <pthread_attr> stack-size i64 ulimit -s (typically 8MB)
	// @config-desc Minimum stack size for a thread, at least 16KB
	size_t stack_size = girt_config_get_i64(thread_config, "stack-size", 0);
	if (stack_size > 0) {
		err = pthread_attr_setstacksize(attr, stack_size);
		if (err != 0) {
			girt_printf(ERROR, "Failed to set stack size to %lu bytes in pthread attrs: %s\n", stack_size, strerror(err));
			girt_fail(NULL);
		}
	}

	girt_return(attr);
}

void
girt_config_free_pthread_attr(pthread_attr_t *attr)
{
	if (attr == NULL) {
		return;
	}

	int err = pthread_attr_destroy(attr);
	if (err != 0) {
		girt_printf(ERROR, "Failed to destroy pthread attrs: %s\n", strerror(err));
	}

	free(attr);
}

girt_cfg *
girt_config_get_table(girt_cfg *config, char *key)
{
	girt_cfg *table = girt_malloc(sizeof(*table));

	kv_init(table->toml);

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		toml_table_t *toml = toml_table_in(kv_A(config->toml, i), key);
		if (toml != NULL) {
			kv_push(toml_table_t *, table->toml, toml);
		}
	}

	return table;
}

int
girt_config_root_table_count(girt_cfg *config)
{
	return kv_size(config->toml);
}

int
girt_config_root_table_nkval(girt_cfg *config)
{
	int nkval = 0;

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		nkval += toml_table_nkval(kv_A(config->toml, i));
	}

	return nkval;
}

int
girt_config_root_table_narr(girt_cfg *config)
{
	int narr = 0;

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		narr += toml_table_narr(kv_A(config->toml, i));
	}

	return narr;
}

int
girt_config_root_table_ntab(girt_cfg *config)
{
	int ntab = 0;

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		ntab += toml_table_ntab(kv_A(config->toml, i));
	}

	return ntab;
}

void
girt_config_free_table(girt_cfg *table)
{
	if (table == NULL) {
		return;
	}

	kv_destroy(table->toml);

	memset(table, 0, sizeof(*table));
	free(table);
}

girt_cfg_vec *
girt_config_get_subtables(girt_cfg *config, char *key)
{
	girt_cfg_vec *array = girt_malloc(sizeof(*array));

	kv_init(*array);

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		toml_table_t *tab = toml_table_in(kv_A(config->toml, i), key);
		if (tab != NULL) {
			int j = 0;
			const char *name;
			while ((name = toml_key_in(tab, j++)) != NULL) {
				toml_table_t *subtab = toml_table_in(tab, name);
				if (subtab != NULL) {
					girt_cfg *cfg = girt_malloc(sizeof(*cfg));
					kv_init(cfg->toml);
					kv_push(toml_table_t *, cfg->toml, subtab);
					kv_push(girt_cfg *, *array, cfg);
				}
			}
		}
	}

	return array;
}

void
girt_config_free_subtables(girt_cfg_vec *array)
{
	if (array == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(*array); i++) {
		girt_cfg *cfg = kv_A(*array, i);
		if (cfg != NULL) {
			kv_destroy(cfg->toml);
			memset(cfg, 0, sizeof(*cfg));
			free(cfg);
		}
	}

	kv_destroy(*array);

	memset(array, 0, sizeof(*array));

	free(array);
}

girt_cfg_vec *
girt_config_get_table_array(girt_cfg *config, char *key)
{
	girt_cfg_vec *array = girt_malloc(sizeof(*array));

	kv_init(*array);

	for (size_t i = 0; i < kv_size(config->toml); i++) {
		toml_array_t *toml = toml_array_in(kv_A(config->toml, i), key);
		if (toml != NULL && toml_array_kind(toml) == 't') {
			for (int j = 0; j < toml_array_nelem(toml); j++) {
				girt_cfg *cfg = girt_malloc(sizeof(*cfg));
				kv_init(cfg->toml);
				kv_push(toml_table_t *, cfg->toml, toml_table_at(toml, j));
				kv_push(girt_cfg *, *array, cfg);
			}
		}
	}

	return array;
}

char *
girt_config_get_table_name(girt_cfg *config)
{
	const char *name = toml_table_key(kv_A(config->toml, 0));

	if (name != NULL) {
		name = girt_strdup(name);
	}

	return (char *)name;
}

static const char *
config_raw_in(girt_cfg *config, char *key)
{
	const char* raw = NULL;

	for (ssize_t i = (ssize_t)kv_size(config->toml)-1; raw == NULL && i >= 0; i--) {
		raw = toml_raw_in(kv_A(config->toml, i), key);
	}

	return raw;	
}

static toml_array_t *
config_array_in(girt_cfg *config, char *key)
{
	toml_array_t *array = NULL;

	for (ssize_t i = (ssize_t)kv_size(config->toml)-1; array == NULL && i >= 0; i--) {
		array = toml_array_in(kv_A(config->toml, i), key);
	}

	return array;	
}


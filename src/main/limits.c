#include <girt/girt.h>

#include <grp.h>
#include <pwd.h>
#include <net/if.h>

static uint64_t get_file_max();
static int set_nofile(rlim_t max_fds);
static uint64_t get_integer_kernel_param(char *paramname);
static char *get_kernel_param(char *paramname, char *buf, size_t buflen);

static char *integer_kernel_params[] = {
	"kernel.sched_migration_cost_ns",
	"kernel.sched_autogroup_enabled",
	"fs.file-max",
	"net.core.rmem_max",
	"net.core.wmem_max",
	"net.core.somaxconn",
	"net.ipv4.tcp_max_syn_backlog",
	"net.ipv4.tcp_syncookies",
	"net.ipv4.tcp_tw_recycle",
	"net.ipv4.tcp_tw_reuse"
};

typedef struct { int rsrc; char *name; } resource;
static resource resources[] = {
	{ RLIMIT_AS, "virtual memory (kbytes)" },
	{ RLIMIT_CORE, "core file size (blocks)" },
	{ RLIMIT_CPU, "cpu time (seconds)" },
	{ RLIMIT_DATA, "data seg size (kbytes)" },
	{ RLIMIT_FSIZE, "file size (blocks)" },
	{ RLIMIT_LOCKS,	"file locks" },
	{ RLIMIT_MEMLOCK, "max locked memory (bytes)"},
	{ RLIMIT_MSGQUEUE,	"POSIX message queues (bytes)" },
	{ RLIMIT_NICE,		"RLIMIT_NICE" },
	{ RLIMIT_NOFILE, "open files" },
	{ RLIMIT_NPROC, "max user processes" },
	{ RLIMIT_RSS, "max memory (kbytes)" },
	{ RLIMIT_RTPRIO,	"real-time priority" },
#ifdef RLIMIT_RTTIME
	{ RLIMIT_RTTIME,	"RLIMIT_RTTIME" },
#endif
	{ RLIMIT_SIGPENDING,"pending signals" },
	{ RLIMIT_STACK, 	"stack size (kbytes)" }
};

static void limits_dump_features_for_evbase(girt_str_vec *vec, struct event_base *evbase);

int
girt_limits_set(girt_root *root)
{
	// @config <root> max-fds i64 10% of OS limit from /proc/sys/fs/file-max
	// @config-desc The maximum number of file descriptors available to girt. Typically each event base
	// @config-desc (one main, each listener and worker) needs 4 file descriptors, each worker needs one more
	// @config-desc for DNS, and each listener needs one more for the socket being monitored. Futhermore each
	// @config-desc active TCP connection will require two file descriptors. Active UDP flows require one.
	if (set_nofile(girt_config_get_i64(girt_root_config(root), "max-fds", (get_file_max() / 100) * 10)) != 0) {
		return -1;
	}

	if (girt_is_priority_enabled(DEBUG)) {
		girt_print(DEBUG, "Current kernel parameters:");
		for (size_t i = 0; i < sizeof(integer_kernel_params)/sizeof(char *); i++) {
			girt_printf(DEBUG, "\t%-32s %lu", integer_kernel_params[i], get_integer_kernel_param(integer_kernel_params[i]));
		}

		char buf[64];
		get_kernel_param("net.ipv4.ip_local_port_range", buf, sizeof(buf));
		char *tab = strchr(buf, '\t');
		if (tab != NULL) {
			*tab = '-';
		}

		girt_printf(DEBUG, "\t%-32s %s", "net.ipv4.ip_local_port_range", buf);

		girt_print(DEBUG, "Current and maximum resource limits:");
		for (size_t i = 0; i < sizeof(resources)/sizeof(resource); i++) {
			struct rlimit rl[1];
			if (getrlimit(resources[i].rsrc, rl) >= 0) {
				char cur[64];
				snprintf(cur, sizeof(cur), rl->rlim_cur == RLIM_INFINITY ? "inf" : "%lu", rl->rlim_cur);
				char max[64];
				snprintf(max, sizeof(max), rl->rlim_max == RLIM_INFINITY ? "inf" : "%lu", rl->rlim_max);
				girt_printf(DEBUG, "\t%-32s %-9s %-9s", resources[i].name, cur, max);
			}
		}

		girt_str_vec *vec = girt_str_vec_init();
		defer { girt_str_vec_free(vec); }
		limits_dump_features_for_evbase(vec, NULL);
		limits_dump_features_for_evbase(vec, girt_root_event_base(root));
		for (size_t i = 0; i < kv_size(*vec); i++) {
			girt_print(DEBUG, kv_A(*vec, i));
		}
	} else {
		girt_printf(INFO, "Using Libevent %s, with backend method %s", event_get_version(), event_base_get_method(girt_root_event_base(root)));
	}

	return 0;
}

/*
 * Get maximum listen backlog
 */
uint64_t
girt_limits_get_max_backlog()
{
	return get_integer_kernel_param("net.core.somaxconn");
} 

/*
 * Get maximum listen backlog
 */
uint64_t
girt_limits_get_cpu_count()
{
	return sysconf(_SC_NPROCESSORS_ONLN);
} 

/*
 * Get system-wide max file descriptors
 */
static uint64_t
get_file_max()
{
	return get_integer_kernel_param("fs.file-max");
} 

/*
 * Set process max file descriptors
 */
static int
set_nofile(rlim_t max_fds)
{
	struct rlimit rl[1];

	if (getrlimit(RLIMIT_NOFILE, rl) >= 0 && rl->rlim_cur >= max_fds && rl->rlim_max >= max_fds) {
		girt_printf(INFO, "Leaving file descriptor limit at %lu", rl->rlim_cur);
		return 0;
	}

	rl->rlim_cur = rl->rlim_max = max_fds;

	while (rl->rlim_max > 0) {
		if (setrlimit(RLIMIT_NOFILE, rl) >= 0) {
			break;
		}
		rl->rlim_cur = rl->rlim_max = --max_fds;
	}

	if (rl->rlim_max == 0) {
		girt_printf(ERROR, "Failed to set file descriptor limit: %s\n", strerror(errno));
		return -1;
	}

	girt_printf(INFO, "File descriptor limit set to %lu", rl->rlim_cur);

	return 0;
}

static uint64_t
get_integer_kernel_param(char *paramname)
{
	uint64_t value = 0;

	char buf[64];
	get_kernel_param(paramname, buf, sizeof(buf));

	if (buf[0] != '\0') {
		char *end;
		value = strtoul(buf, &end, 10);
		if (*end != '\0') {
			value = 0;
		}
	}

	return value;
} 

static char *
get_kernel_param(char *paramname, char *buf, size_t buflen)
{
	if (buf == NULL || buflen == 0) {
		return "";
	}

	buf[0] = '\0';

	char filename[256];

	snprintf(filename, sizeof(filename), "/proc/sys/%s", paramname);
	char *dot;
	while ((dot = strchr(filename, '.')) != NULL) {
		*dot = '/';
	}

	FILE *fp = fopen(filename, "r");
	if (fp != NULL) {
		if (fgets(buf, buflen, fp) != NULL) {
			while (buf[0] != '\0' && isspace(buf[strlen(buf)-1])) {
				buf[strlen(buf)-1] = '\0';
			}
		}
		fclose(fp);
	}

	return buf;
} 

void
girt_limits_dump_pthread_attr_for_tid(girt_str_vec *vec, pthread_t tid)
{
	int s, i;
	size_t v;
	void *stkaddr;
	struct sched_param sp;

	pthread_attr_t attr;
	int err = pthread_getattr_np(tid, &attr);
	if (err != 0) {
		girt_str_vec_printf(vec, "Failed to get pthread attr for thread: %s", strerror(err));
		return;
	}

	s = pthread_attr_getdetachstate(&attr, &i);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %s", "Detach state",
			(i == PTHREAD_CREATE_DETACHED) ? "PTHREAD_CREATE_DETACHED" :
			(i == PTHREAD_CREATE_JOINABLE) ? "PTHREAD_CREATE_JOINABLE" :
			"???");
	}

	s = pthread_attr_getscope(&attr, &i);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %s", "Scope",
			(i == PTHREAD_SCOPE_SYSTEM)? "PTHREAD_SCOPE_SYSTEM" :
			(i == PTHREAD_SCOPE_PROCESS) ? "PTHREAD_SCOPE_PROCESS" :
			"???");
	}

	s = pthread_attr_getinheritsched(&attr, &i);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %s", "Inherit scheduler",
			(i == PTHREAD_INHERIT_SCHED)? "PTHREAD_INHERIT_SCHED" :
			(i == PTHREAD_EXPLICIT_SCHED) ? "PTHREAD_EXPLICIT_SCHED" :
			"???");
	}

	s = pthread_attr_getschedpolicy(&attr, &i);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %s", "Scheduling policy",
			(i == SCHED_OTHER) ? "SCHED_OTHER" :
			(i == SCHED_FIFO)? "SCHED_FIFO" :
			(i == SCHED_RR)	? "SCHED_RR" :
			"???");
	}

	s = pthread_attr_getschedparam(&attr, &sp);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %d", "Scheduling priority", sp.sched_priority);
	}

	s = pthread_attr_getguardsize(&attr, &v);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s %ld bytes", "Guard size", v);
	}

	s = pthread_attr_getstack(&attr, &stkaddr, &v);
	if (s == 0) {
		girt_str_vec_printf(vec, "\t%-32s 0x%lx bytes", "Stack size", v);
	}

	err = pthread_attr_destroy(&attr);
	if (err != 0) {
		girt_str_vec_printf(vec, "Failed to destroy pthread attrs for thread: %s", strerror(err));
	}
}

int
girt_limits_set_user_group(girt_root *root)
{
	int result = 0;

	// @config <root> group string The group of the user that girt will run as
	// @config-desc The name of the group that girt should run as after setting up privileged resources.
	char *group = girt_config_get_string(girt_root_config(root), "group", NULL);
	struct group *grp = NULL, _grp[1] = {{0}};
	defer { free(group); }
	long grplen = sysconf(_SC_GETGR_R_SIZE_MAX);
	char grpbuf[grplen];

	// @config <root> user string The value of the SUDO_USER environment variable
	// @config-desc The name of the user that girt should run as after setting up privileged resources.
	char *user = girt_config_get_string(girt_root_config(root), "user", getenv("SUDO_USER"));
	defer { free(user); }
	struct passwd *usr = NULL, _usr[1] = {{0}};
	long usrlen = sysconf(_SC_GETPW_R_SIZE_MAX);
	char usrbuf[grplen];

	if (group != NULL) {
		int err = getgrnam_r(group, _grp, grpbuf, grplen, &grp);
		if (err != 0) {
			girt_printf(ERROR, "Failed to get group '%s': %s", group, strerror(err));
			result = -1;
		} else
		if (grp == NULL) {
			girt_printf(ERROR, "Group '%s' not found", group);
			result = -1;
		}
	}

	if (result == 0) {
		if (user != NULL) {
			int err = getpwnam_r(user, _usr, usrbuf, usrlen, &usr);
			if (err != 0) {
				girt_printf(ERROR, "Failed to get user '%s': %s", user, strerror(err));
				result = -1;
			} else
			if (usr == NULL) {
				girt_printf(ERROR, "User '%s' not found", user);
				result = -1;
			} else {
				if (grp != NULL) {
					usr->pw_gid = grp->gr_gid;
				} else {
					getgrgid_r(usr->pw_gid, _grp, grpbuf, grplen, &grp);
				}

				if (initgroups(user, usr->pw_gid) != 0) {
					girt_printf(ERROR, "Failed to initgroups user '%s': %s", user, strerror(errno));
					result = -1;
				} else
				if (setgid(usr->pw_gid) == -1) {
					girt_printf(ERROR, "Failed to setgid to %d: %s", usr->pw_gid, strerror(errno));
					result = -1;
				} else
				if (setuid(usr->pw_uid) == -1) {
					girt_printf(ERROR, "Failed to setuid to %d: %s", usr->pw_uid, strerror(errno));
					result = -1;
				} else
				if (grp != NULL ) {
					girt_printf(INFO, "Running as user %s:%s", usr->pw_name, grp->gr_name);
				} else {
					girt_printf(INFO, "Running as user %s:%d", usr->pw_name, usr->pw_gid);
				}
			}
		} else
		if (getuid() != geteuid() && setuid(getuid()) != 0) {
			girt_printf(ERROR, "Failed to setuid(getuid()): %s", strerror(errno));
			result = -1;
		} else {
			girt_printf(INFO, "Running as user %d:%d", getuid(), getgid());
		}
	}

	return result;
}

static void
limits_dump_features_for_evbase(girt_str_vec *vec, struct event_base *evbase)
{
	if (evbase == NULL) {
		// This leaks one block of memeory inside libevent
		const char **methods = event_get_supported_methods();

		girt_str_vec_printf(vec, "Using Libevent %s, available methods are:", event_get_version());

		while (methods != NULL && *methods != NULL) {
			girt_str_vec_printf(vec, "\t%s", *(methods++));
		}
	} else {
		enum event_method_feature f = event_base_get_features(evbase);
		girt_str_vec_printf(vec, "Using Libevent with backend method %s.%s%s%s",
			event_base_get_method(evbase),
			(f & EV_FEATURE_ET) != 0 ? " Edge-triggered events are supported." : "",
			(f & EV_FEATURE_O1) != 0 ? " O(1) event notification is supported." : "",
			(f & EV_FEATURE_FDS) != 0 ? " All FD types are supported." : ""
		);
	}
}

char *
girt_limits_ifindex_name(int ifindex)
{
	char buf[IF_NAMESIZE];
	char *ifname = if_indextoname(ifindex, buf);
	if (ifname == NULL) {
		girt_printf(ERROR, "Failed to get iface name for index %d: %s\n", ifindex, strerror(errno));
		return NULL;
	}

	return girt_strdup(ifname);
}

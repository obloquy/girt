#include <girt/girt.h>

#include <private/message.h>

girt_message *
girt_message_init()
{
	girt_message *message = girt_malloc(sizeof(*message));

	message->method = GIRT_MESSAGE_MSGHDR;

	message->u.m.msg.msg_name = &message->addr;
	message->u.m.msg.msg_namelen = sizeof(message->addr);
	message->u.m.msg.msg_iov = &message->iov;
	message->u.m.msg.msg_iovlen = 1;
	message->u.m.msg.msg_control = message->u.m.control;
	message->u.m.msg.msg_controllen = sizeof(message->u.m.control);

	return message;
}

void
girt_message_free_pp(void **vpp)
{
	if (vpp == NULL) {
		return;
	}

	girt_message_free(*((girt_message **)vpp));
}

void
girt_message_free(girt_message *message)
{
	if (message == NULL) {
		return;
	}

	memset(message, 0, sizeof(*message));

	free(message);
}

girt_message *
girt_message_set_sockaddr(girt_message *message, struct sockaddr *addr, socklen_t addrlen)
{
	if (addr == NULL) {
		memset(&message->addr, 0, sizeof(message->addr));
		message->u.m.msg.msg_namelen = sizeof(message->addr);
	} else
	if (addrlen <= sizeof(message->addr)) {
		memcpy(&message->addr, addr, addrlen);
		message->u.m.msg.msg_namelen = addrlen;
	}

	return message;
}

struct sockaddr *
girt_message_sockaddr(girt_message *message, socklen_t *addrlen)
{
	if (addrlen != NULL) {
		*addrlen = message->u.m.msg.msg_namelen;
	}

	return (struct sockaddr *)message->u.m.msg.msg_name;
}

struct msghdr *
girt_message_msghdr(girt_message *message)
{
	return &message->u.m.msg;
}

girt_message *
girt_message_set_data(girt_message *message, void *data, size_t datalen)
{

	switch (message->method) {
		case GIRT_MESSAGE_MSGHDR: {
			message->iov.iov_base = data;
			message->iov.iov_len = datalen;
			break;
		}

		case GIRT_MESSAGE_LIBNET: {
			message->u.l.udp_tag = libnet_build_udp(
				ntohs(girt_sockaddr_port(&message->u.l.spoof.sa)),
				ntohs(girt_sockaddr_port(&message->addr.sa)),
				LIBNET_UDP_H + datalen,
				0, /* checksum */
				data,
				datalen,
				message->u.l.net,
				message->u.l.udp_tag);

			if (message->u.l.udp_tag == -1) {
				girt_printf(ERROR, "Failed to build UDP header: %s\n", libnet_geterror(message->u.l.net));
				return NULL;
			}

			if (message->addr.sa.sa_family == AF_INET) {
				message->u.l.ip_tag = libnet_build_ipv4(
					LIBNET_IPV4_H + LIBNET_UDP_H + datalen,
					0, /* tos */
					(u_int16_t)libnet_get_prand(LIBNET_PR16), /* id */
					0, /* frag */
					64, /* ttl */
					IPPROTO_UDP,
					0, /* checksum */
					message->u.l.spoof.in.sin_addr.s_addr,
					message->addr.in.sin_addr.s_addr,
					NULL,
					0,
					message->u.l.net,
					message->u.l.ip_tag);
			} else {
				// IPv6
			}

			if (message->u.l.ip_tag == -1) {
				girt_printf(ERROR, "Failed to build IP header: %s\n", libnet_geterror(message->u.l.net));
				return NULL;
			}
			break;
		}
	}

	return message;
}

void *
girt_message_data(girt_message *message, size_t *datalen)
{
	if (datalen != NULL) {
		*datalen = message->iov.iov_len;
	}

	return message->iov.iov_base;
}

ssize_t
girt_message_receive(girt_message *message, int fd, int flags)
{
	girt_message_set_sockaddr(message, NULL, 0);
	message->u.m.msg.msg_controllen = sizeof(message->u.m.control);

	return recvmsg(fd, &message->u.m.msg, flags);
}

char *
girt_message_error(girt_message *message)
{
	if (message == NULL) {
		return "Bad message";
	}

	return message->errmsg;
}

ssize_t
girt_message_send(girt_message *message, int fd, int flags)
{
	if (message == NULL) {
		return -1;
	}

	int n_written = -1;

	switch (message->method) {
	case GIRT_MESSAGE_MSGHDR:
		message->u.m.msg.msg_controllen = message->u.m.cmsg_space;
		n_written = sendmsg(fd, &message->u.m.msg, flags);
		if (n_written < 0) {
			message->errmsg = strerror(errno);
		}
		break;

	case GIRT_MESSAGE_LIBNET:
		n_written = libnet_write(message->u.l.net);
		if (n_written < 0) {
			message->errmsg = libnet_geterror(message->u.l.net);
		}
		break;
	}

	return n_written;
}

girt_message *
girt_message_push_cmsg(girt_message *message, int level, int type, void *data, size_t datalen)
{
	struct cmsghdr *cmsg = message->u.m.cmsg_last = (message->u.m.cmsg_space == 0 ? CMSG_FIRSTHDR(&message->u.m.msg) : CMSG_NXTHDR(&message->u.m.msg, message->u.m.cmsg_last));

	cmsg->cmsg_level = level;
	cmsg->cmsg_type = type;
	cmsg->cmsg_len = CMSG_LEN(datalen);
	memcpy(CMSG_DATA(cmsg), data, datalen);

	message->u.m.cmsg_space += CMSG_SPACE(datalen);

	return message;
}

void
girt_message_dump(girt_message *message, girt_str_vec *vec)
{
	girt_hexdump(vec, message->u.m.msg.msg_name, message->u.m.msg.msg_namelen,				   "msg_name:	", "   ", '.');
	girt_hexdump(vec, message->u.m.msg.msg_iov[0].iov_base, message->u.m.msg.msg_iov[0].iov_len, "msg_iov:	 ", "   ", '.');
	girt_hexdump(vec, message->u.m.msg.msg_control, message->u.m.msg.msg_controllen,			 "msg_control: ", "   ", '.');

	for (size_t i = 0; i < kv_size(*vec); i++) {
		fprintf(stderr, "%s", kv_A(*vec, i));
	}
}

void
girt_message_dump_fp(girt_message *message, FILE *fp)
{
	girt_str_vec *vec = girt_str_vec_init();
	defer { girt_str_vec_free(vec); }

	girt_message_dump(message, vec);

	for (size_t i = 0; i < kv_size(*vec); i++) {
		fprintf(fp, "%s", kv_A(*vec, i));
	}
}

girt_message *
girt_message_spoof_init(girt_message_method method, struct sockaddr *addr, socklen_t addrlen,
			struct sockaddr *spoof, socklen_t spooflen, int ifidx)
{
	girt_message *message = girt_message_init();

	message->method = method;

	if (addr != NULL && addrlen > 0 && addrlen <= sizeof(message->addr)) {
		memcpy(&message->addr, addr, addrlen);
		message->u.m.msg.msg_namelen = addrlen;
	}

	switch (method) {
		case GIRT_MESSAGE_MSGHDR: {
			int proto;
			int pktinfotype;
			if (spoof->sa_family == AF_INET) {
				proto = IPPROTO_IP;
				pktinfotype = IP_PKTINFO;
				struct in_pktinfo ipi[1];
				memset(ipi, 0, sizeof(*ipi));
				ipi->ipi_spec_dst = ((struct sockaddr_in *)spoof)->sin_addr;
				girt_message_push_cmsg(message, IPPROTO_IP, IP_PKTINFO, ipi, sizeof(*ipi));
			} else {
				proto = IPPROTO_IPV6;
				pktinfotype = IPV6_PKTINFO;
				struct sockaddr_in6 *sa = (struct sockaddr_in6 *)spoof;
				struct in6_pktinfo ipi[1];
				memset(ipi, 0, sizeof(*ipi));
				ipi->ipi6_addr = sa->sin6_addr;
				if (IN6_IS_ADDR_LINKLOCAL(&sa->sin6_addr) || IN6_IS_ADDR_MULTICAST(&sa->sin6_addr)) {
					ipi->ipi6_ifindex = sa->sin6_scope_id;
				}
				girt_message_push_cmsg(message, proto, pktinfotype, ipi, sizeof(*ipi));
			}
			break;
		}
		case GIRT_MESSAGE_LIBNET: {
			if (spoof != NULL && spooflen > 0 && spooflen <= sizeof(message->u.l.spoof)) {
				memcpy(&message->u.l.spoof, spoof, spooflen);
			}
			char *ifname = girt_limits_ifindex_name(ifidx);
			if (ifname == NULL) {
				return NULL;
			}
			char errbuf[LIBNET_ERRBUF_SIZE];
			message->u.l.net = libnet_init(spoof->sa_family == AF_INET ? LIBNET_RAW4 : LIBNET_RAW6, ifname, errbuf);
			if (message->u.l.net == NULL) {
				girt_printf(ERROR, "Failed to initialise libnet: %s\n", errbuf);
				return NULL;
			}
			libnet_seed_prand(message->u.l.net);
			message->u.l.udp_tag = LIBNET_PTAG_INITIALIZER;
			message->u.l.ip_tag = LIBNET_PTAG_INITIALIZER;
			break;
		}
	}

	return message;
}

girt_message *
girt_message_libnet_init(struct sockaddr *addr, socklen_t addrlen, int pktinfotype, void *pktinfo, size_t pktinfolen)
{
	girt_message *message = girt_message_init();

	if (addr != NULL && addrlen > 0 && addrlen <= sizeof(message->addr)) {
		memcpy(&message->addr, addr, addrlen);
		message->u.m.msg.msg_namelen = addrlen;
	}

	girt_message_push_cmsg(message, pktinfotype == IP_PKTINFO ? IPPROTO_IP : IPPROTO_IPV6, pktinfotype, pktinfo, pktinfolen);

	return message;
}


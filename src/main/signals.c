#include <girt/girt.h>

#include <private/signals.h>

static void signals_event_func(evutil_socket_t fd, short flags, void *arg);
static struct event *signals_event_new(girt_sig *signals, char *name, struct event *ev);

girt_sig *
girt_signals_init(girt_root *root)
{
	girt_sig *signals = girt_malloc(sizeof(*signals));

	signals->events = girt_check_oom_ptr(kh_init(events));

	int64_t sigs[] = { SIGQUIT, SIGHUP, SIGINT, SIGTERM, SIGPIPE, SIGUSR1, SIGUSR2, 0 };
	for (size_t i = 0; sigs != NULL && sigs[i] > 0; i++) {
		char name[16];
		snprintf(name, sizeof(name), "sig-%ld", sigs[i]);
		struct event *ev = girt_check_oom_ptr(evsignal_new(girt_root_event_base(root), sigs[i], signals_event_func, event_self_cbarg()));
		evsignal_add(signals_event_new(signals, name, ev), NULL);
	}

	return signals;
}

void
girt_signals_free(girt_sig *signals)
{
	if (signals == NULL) {
		return;
	}

	for (khiter_t k = kh_begin(signals->events); k != kh_end(signals->events); ++k) {
		if (kh_exist(signals->events, k)) {
			event_free(kh_value(signals->events, k));
			free((void *)kh_key(signals->events, k));
			kh_del(events, signals->events, k); 
		}
	}
	kh_destroy(events, signals->events);

	memset(signals, 0, sizeof(*signals));

	free(signals);
}

static void 
signals_event_func(evutil_socket_t fd, UNUSED short flags, void *arg)
{
	struct event *ev = arg;

	switch (fd) {
	case SIGINT:
		girt_print(INFO, "Caught SIGINT");
		event_base_loopbreak(event_get_base(ev));
		break;
	case SIGTERM:
		girt_print(INFO, "Caught SIGTERM");
		event_base_loopbreak(event_get_base(ev));
		break;
	}
}

static struct event *
signals_event_new(girt_sig *signals, char *name, struct event *ev)
{
	name = girt_strdup(name);

	int ret;
	khiter_t k = kh_put(events, signals->events, name, &ret);
	if (!ret) {
		// already exists
		free(name);
		event_free(kh_value(signals->events, k));
	}

	kh_value(signals->events, k) = ev;
	
	return ev;
}

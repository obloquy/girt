#include <girt/girt.h>

#include <openssl/ssl.h>

int
girt_ssl_check(unsigned char *buf, size_t len, char **sni)
{
	if (buf[0] == 0x16) { // SSL3 or TLS
		// NULL message
		if (len == 5 && buf[1] == 0x03 && buf[2] == 0x01 && buf[3] == 0x0 && buf[4] == 0x0) {
			return (buf[1] << 8) | buf[2];
		}
		if (len < 6) return -1;
		size_t dlen = (buf[3] << 8) | buf[4];
		if (dlen >= 44 && buf[5] == 0x01) { // ClientHello
			if (len < (dlen+5)) return -1;
			if (len == (dlen+5)) {
				size_t dlen2 = (buf[6] << 16) | (buf[7] << 8) | buf[8];
				int version = (buf[9] << 8) | buf[10];
				if ((4+dlen2) == dlen && buf[9] == 0x03) {
					size_t session_id_length = buf[43];
					size_t off = 43+1+session_id_length;
					if (session_id_length <= 32 && (off+4) <= len) {
						size_t cipher_spec_length = (buf[off] << 8) | buf[off+1];
						off += 2+cipher_spec_length;
                        if (cipher_spec_length > 1 && (cipher_spec_length % 2) == 0 && (off+1) <= len) {
                            size_t compression_spec_length = buf[off];
                            off += 1+compression_spec_length;
							if (off == len) {
								return version;
							} else
							if ((off+2) <= len) {
								size_t extension_length = (buf[off] << 8) | buf[off+1];
								off += 2;
								if ((off+extension_length) == len) {
									// extract SNI if required
									while (sni != NULL && *sni == NULL && (off+4) <= len) {
										unsigned short exttype = (buf[off] << 8) | buf[off+1];
										size_t extlen = (buf[off+2] << 8) | buf[off+3];
										off += 4;
										if ((off+extlen) <= len) {
											switch (exttype) {
											case 0:
												if (extlen >= 2) {
													size_t namelistlen = (buf[off] << 8) | buf[off+1];
													size_t nloff = 2;
													if ((namelistlen + nloff) == extlen) {
														while (*sni == NULL && (nloff+3) <= extlen) {
															unsigned char sntype = buf[off+nloff];
															size_t snlen = (buf[off+nloff+1] << 8) | buf[off+nloff+2];
															nloff += 3;
															if ((nloff+snlen) <= extlen && snlen <= TLSEXT_MAXLEN_host_name) {
																if (sntype == 0) {
																	*sni = girt_strndup((char *)&buf[off+nloff], snlen);
																}
															}
															nloff += snlen;
														}
													}
												}
												break;
											default:
												break;
											}
										}
										off += extlen;
									}

									return version;
								}
							}
						}
					}
				}
			}
		}
	} else
	if ((buf[0] & 0x80) != 0) { // SSL2
		if (len < 3) return -1;
		size_t dlen = ((buf[0] & 0x7f) << 8) | buf[1];
		if (dlen >= 12 && buf[2] == 0x01) {
			if (len < (dlen+2)) return -1;
			if (buf[3] <= 0x03) {
				int version = (buf[3] << 8) | buf[4];
				size_t cipher_spec_length = (buf[5] << 8) | buf[6];
				size_t session_id_length = (buf[7] << 8) | buf[8];
				size_t challenge_length = (buf[9] << 8) | buf[10];
				if (cipher_spec_length > 0 && (cipher_spec_length % 3) == 0 &&
						session_id_length == 0 &&
						challenge_length >= 16 &&
						(9+cipher_spec_length+session_id_length+challenge_length) == dlen &&
						(2+dlen) == len
					) {
					return version;
				}
			}
		}
	}

	return 0;
}

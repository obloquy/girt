#include <girt/girt.h>

#include <private/lsnsock.h>

girt_lsnsock *
girt_lsnsock_init(girt_listener *listener, girt_cfg *listener_config)
{
	girt_enter();
	girt_lsnsock *s = girt_malloc(sizeof(*s));
	s->fd = -1;

	defer { girt_if_fail { girt_lsnsock_free(s); } }

	// @config <listener> family string "inet"
	// @config-desc The address family the socket (inet or inet6)
	s->familyname = girt_config_get_string(listener_config, "family", "inet");
	if (!strcmp(s->familyname, "inet")) {
		s->family = AF_INET;
	} else
	if (!strcmp(s->familyname, "inet6")) {
		s->family = AF_INET6;
	} else {
		girt_loc_printf(ERROR, girt_listener_name(listener), "Unknown address family: %s\n", s->familyname);
		girt_fail(NULL);
	}

	// @config <listener> type string "stream"
	// @config-desc The type of the socket (stream or datagram)
	s->typename = girt_config_get_string(listener_config, "type", "stream");
	if (!strcmp(s->typename, "stream")) {
		s->type = SOCK_STREAM;
	} else
	if (!strcmp(s->typename, "datagram")) {
		s->type = SOCK_DGRAM;
	} else {
		girt_loc_printf(ERROR, girt_listener_name(listener), "Unknown socket type: %s\n", s->typename);
		girt_fail(NULL);
	}

	// @config <listener> proto string "tcp" if type is "stream", "udp" if type is "datagram"
	// @config-desc The protocol for the socket (tcp or udp)
	s->protoname = girt_config_get_string(listener_config, "proto", s->type == SOCK_STREAM ? "tcp" : (s->type == SOCK_DGRAM ? "udp" : NULL));
	if (s->protoname == NULL) {
		// shouldn't happen
		girt_loc_print(ERROR, girt_listener_name(listener), "No protocol configured\n");
		girt_fail(NULL);
	}
	if (!strcmp(s->protoname, "tcp")) {
		s->proto = IPPROTO_TCP;
	} else
	if (!strcmp(s->protoname, "udp")) {
		s->proto = IPPROTO_UDP;
	} else {
		girt_loc_printf(ERROR, girt_listener_name(listener), "Unknown protocol: %s\n", s->protoname);
		girt_fail(NULL);
	}

	// @config <listener> - <addrinfo>
	// @config-default address All zeros
	// @config-default port 8080
	s->addrinfo = girt_config_get_addrinfo(listener_config, NULL, 1, s->family, NULL, "8080");
	s->addrname = girt_addrinfo_to_addrname(s->addrinfo, &s->addr, &s->portname);
	if (s->addrname == NULL) {
		girt_loc_printf(ERROR, girt_listener_name(listener), "Failed to convert address '%s' back to string: %s\n", s->addr, strerror(errno));
		girt_fail(NULL);
	}

	s->fd = girt_socket(s->addrinfo->ai_family, s->addrinfo->ai_socktype = s->type, s->addrinfo->ai_protocol = s->proto);
	if (s->fd < 0) {
		girt_fail(NULL);
	}

	// @config <listener> transparent boolean false
	// @config-desc Should the listener socket have IP_TRANSPARENT mode set to allow recovery of the original deestination address for connections and packets
	s->transparent = girt_config_get_boolean(listener_config, "transparent", 0);
	if (s->transparent) {
		int one = 1;
		if (setsockopt(girt_lsnsock_fd(s), SOL_IP, IP_TRANSPARENT, &one, sizeof(one)) < 0) {
			girt_loc_printf(ERROR, girt_listener_name(listener),
				"Failed to set transparent mode for socket '%s': %s\n", s->addrname, strerror(errno));
			girt_fail(NULL);
		}
	}

	girt_return(s);
}

void
girt_lsnsock_free(girt_lsnsock *s)
{
	if (s == NULL) {
		return;
	}

	free(s->familyname);
	free(s->typename);
	free(s->protoname);
	free(s->addr);
	free(s->portname);
	free(s->addrname);

	freeaddrinfo(s->addrinfo);

	if (s->fd >= 0) {
		close(s->fd);
	}

	memset(s, 0, sizeof(*s));
	s->fd = -1;
	free(s);
}

char *
girt_lsnsock_protoname(girt_lsnsock *s)
{
	return s->protoname;
}

int
girt_lsnsock_proto(girt_lsnsock *s)
{
	return s->proto;
}

char *
girt_lsnsock_portname(girt_lsnsock *s)
{
	return s->portname;
}

char *
girt_lsnsock_familyname(girt_lsnsock *s)
{
	return s->familyname;
}

int
girt_lsnsock_family(girt_lsnsock *s)
{
	return s->family;
}

char *
girt_lsnsock_typename(girt_lsnsock *s)
{
	return s->typename;
}

int
girt_lsnsock_type(girt_lsnsock *s)
{
	return s->type;
}

char *
girt_lsnsock_addrname(girt_lsnsock *s)
{
	return s->addrname;
}

evutil_socket_t
girt_lsnsock_fd(girt_lsnsock *s)
{
	return s->fd;
}

struct sockaddr *
girt_lsnsock_addr(girt_lsnsock *s)
{
	return s->addrinfo->ai_addr;
}

socklen_t
girt_lsnsock_addrlen(girt_lsnsock *s)
{
	return s->addrinfo->ai_addrlen;
}

int
girt_lsnsock_transparent(girt_lsnsock *s)
{
	return s->transparent;
}


#include <girt/girt.h>

#include <private/root.h>

girt_root *
girt_root_init(girt_str_vec *args)
{
	girt_root *root = girt_malloc(sizeof(*root));

	root->args = args;

	if (
		(root->evbase = event_base_new()) == NULL ||
		(root->config = girt_config_init(root)) == NULL ||
		(root->plugins = girt_plugins_init(root)) == NULL ||
		(root->logger = girt_logger_init(root)) == NULL ||
		girt_limits_set(root) != 0 ||
		(root->signals = girt_signals_init(root)) == NULL ||
		(root->workers = girt_workers_init(root)) == NULL ||
		(root->proxies = girt_proxies_init(root)) == NULL ||
		girt_limits_set_user_group(root) != 0
	) {
		root->exit_code = EXIT_FAILURE;
	}

	return root;
}

intptr_t
girt_root_free(girt_root *root)
{
	if (root == NULL) {
		return EXIT_FAILURE;
	}

	girt_proxies_free(root->proxies);

	girt_workers_free(root->workers);

	girt_signals_free(root->signals);

	event_base_free(root->evbase);

	girt_logger_free(root->logger);

	girt_plugins_free(root->plugins);

	girt_config_free(root->config);

	girt_str_vec_free(root->args);

	intptr_t exit_code = root->exit_code;

	memset(root, 0, sizeof(*root));

	free(root);

	return exit_code;
}

girt_cfg *
girt_root_config(girt_root *root)
{
	return root->config;
}

girt_plg *
girt_root_plugins(girt_root *root)
{
	return root->plugins;
}

girt_wrk *
girt_root_workers(girt_root *root)
{
	return root->workers;
}

struct event_base *
girt_root_event_base(girt_root *root)
{
	return root->evbase;
}

girt_str_vec *
girt_root_args(girt_root *root)
{
	return root->args;
}

void
girt_root_loop(girt_root *root)
{
	event_base_loop(root->evbase, EVLOOP_NO_EXIT_ON_EMPTY);
}

intptr_t
girt_root_exit_code(girt_root *root)
{
	return root->exit_code;
}


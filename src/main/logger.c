#include <girt/girt.h>

#include <private/logger.h>

int girt_logger_level_ROOT = GIRT_LOGGER_INFO;
int girt_logger_level_GIRT = GIRT_LOGGER_INFO;

#ifndef WITHOUT_LOG4C

static void categories_init(girt_cfg *config);

int girt_logger_init_done = 0;

girt_log *
girt_logger_init(girt_root *root)
{
	girt_enter();
	girt_log *log = girt_malloc(sizeof(*log));
	defer { girt_if_fail { girt_logger_free(log); } }

	// Do this first, otherwise resources won't be freed if girt_logger_init() fails
	if (log4c_init() != 0) {
		girt_print(ERROR, "Log4c initialisation failed\n");
		girt_fail(NULL);
	}

	log->root = root;
	girt_cfg *global = girt_root_config(root);

	// @config <root> logger <logger>
	// @config-desc The logger subsystem configuration
	girt_cfg *logger_config = girt_config_get_table(global, "logger");
	defer { girt_config_free_table(logger_config); }
	if (girt_config_root_table_count(logger_config) <= 0) {
		girt_print(ERROR, "No logger configuration\n");
		girt_fail(NULL);
	}

	log->formatters = girt_formatters_init(log, logger_config);
	if (log->formatters == NULL) {
		girt_fail(NULL);
	}
	log->appenders = girt_appenders_init(log, logger_config);
	if (log->appenders == NULL) {
		girt_fail(NULL);
	}
	categories_init(logger_config);

	char etc[strlen(girt_config_home(girt_root_config(root)))+5];
	sprintf(etc, "%s/etc", girt_config_home(girt_root_config(root)));
	// @config <logger> log4crc-directory string "./etc"
	// @config-desc Directory containing the log4crc file to bootstrap the log4c logger.
	// @config-desc The log4crc file does not need to exist, but if it does is must match
	// @config-desc the schema expected by log4c. A minimal version is installed in "./etc"
	char *cfg_dir = girt_config_get_string(logger_config, "log4crc-directory", etc);

	if (cfg_dir != NULL) {
		setenv("LOG4C_RCPATH", cfg_dir, 1);
	}
	free(cfg_dir);

	// @config <logger> sd-debug boolean false
	// @config-desc Enable debug mode for the XML DOM used by log4c
	if (girt_config_get_boolean(logger_config, "sd-debug", 0)) {
		setenv("SD_DEBUG", "1", 1);
	} else {
		unsetenv("SD_DEBUG");
	}

	girt_logger_init_done = 1;

	return log;
}

void
girt_logger_free(girt_log *log)
{
	girt_logger_init_done = 0;

	log4c_fini();

	if (log == NULL) {
		return;
	}

	girt_formatters_free(log->formatters);
	girt_appenders_free(log->appenders);

	memset(log, 0, sizeof(*log));

	free(log);
}

girt_root *
girt_logger_root(girt_log *log)
{
	return log->root;
}

static void
category_init(girt_cfg *category_config, char *name)
{
	log4c_category_t *category = log4c_category_get(name);

	// @config <category> appender string The category's name
	// @config-desc The name of the appender to use with the category
 	char *appender_name = girt_config_get_string(category_config, "appender", name);
	if (appender_name != NULL) {
		log4c_category_set_appender(category, log4c_appender_get(appender_name));
		free(appender_name);
	}

	// @config <category> priority string "info"
	// @config-desc The priority (aka log level) for this category. Girt only uses "debug", "info", and "error".
	char *priority_name = girt_config_get_string(category_config, "priority", "info");
	if (priority_name != NULL) {
	 	int priority = log4c_priority_to_int(priority_name);
		if (priority != LOG4C_PRIORITY_UNKNOWN) {
			log4c_category_set_priority(category, priority);
		} else {
			girt_printf(ERROR, "Warning: ignoring unknown priority name '%s' for logger category '%s'\n", priority_name, name);
		}
		free(priority_name);
	}
}

static void
categories_init(girt_cfg *logger_config)
{
	// @config <logger> category.<name> <category>
	// @config-desc Configuration table for the named category
	girt_cfg_vec *category_cfgs = girt_config_get_subtables(logger_config, "category");

	for (size_t i = 0; i < kv_size(*category_cfgs); i++) {
		girt_cfg *category_config = kv_A(*category_cfgs, i);
		char *name = girt_config_get_table_name(category_config);
		category_init(category_config, name);
		free(name);
	}

	girt_config_free_subtables(category_cfgs);
}

#endif

#include <girt/girt.h>

#include <private/prereader.h>

PLUGIN_FUNCS(
		prereader,
		{
			girt_plugin_context *(*init)(girt_prereader *prereader, girt_cfg *prereader_config);
			ssize_t (*peek)(girt_plugin_context *context, void *buf, size_t len, girt_flow *flow);
			void (*free)(girt_plugin_context *context);
		},
		"init", "peek", "free"
)

girt_prereader *
girt_prereader_init(girt_proxy *proxy, girt_cfg *prereader_config, char *type)
{
	girt_enter();
	girt_prereader *prereader = girt_malloc(sizeof(*prereader));
	defer { girt_if_fail { girt_prereader_free(prereader); } }

	prereader->proxy = proxy;

	prereader->plugin = girt_plugins_load_prereader(girt_root_plugins(girt_proxy_root(proxy)), type);
	if (prereader->plugin == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to load prereader\n");
		girt_fail(NULL);
	}
	prereader->context = girt_plugin_prereader_funcs(prereader->plugin)->init(prereader, prereader_config);
	if (prereader->context == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to initialise prereader\n");
		girt_fail(NULL);
	}

	girt_return(prereader);
}

void
girt_prereader_free(girt_prereader *prereader)
{
	if (prereader == NULL) {
		return;
	}

	if (prereader->plugin != NULL && prereader->context != NULL) {
		girt_plugin_prereader_funcs(prereader->plugin)->free(prereader->context);
	}

	memset(prereader, 0, sizeof(*prereader));

	free(prereader);
}

ssize_t
girt_prereader_peek(UNUSED girt_prereader *prereader, void *buf, size_t len, girt_flow *flow)
{
	return girt_plugin_prereader_funcs(prereader->plugin)->peek(prereader->context, buf, len, flow);
}

#include <girt/girt.h>

#include <private/link.h>

girt_link *
girt_link_init_bev_evbuf(girt_flow *flow, char *name, struct bufferevent *bev, struct evbuffer *evbuf, void *arg, int from_orig)
{
	girt_link *link = girt_flow_attrs(push_ptr_len, flow, name, NULL, sizeof(girt_link), girt_free_pp);

	link->name = girt_flow_attrs(get_name_ref, flow, name);
	link->flow = flow;
	link->bev = bev;
	link->evbuf = evbuf;
	link->arg = arg;
	link->from_orig = from_orig;

	return link;
}

char *
girt_link_name(girt_link *link)
{
	return link->name;
}

girt_flow *
girt_link_flow(girt_link *link)
{
	return link->flow;
}

struct bufferevent *
girt_link_bev(girt_link *link)
{
	return link->bev;
}

struct evbuffer *
girt_link_evbuf(girt_link *link)
{
	return link->evbuf;
}

void *
girt_link_arg(girt_link *link)
{
	return link->arg;
}

int
girt_link_from_orig(girt_link *link)
{
	return link->from_orig;
}




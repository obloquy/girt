#include <girt/girt.h>

#include <private/resolver.h>

PLUGIN_FUNCS(
		resolver,
		{
			girt_plugin_context *(*init)(girt_resolver *resolver, girt_cfg *resolver_config);
			void (*resolve)(char *host, char *port, girt_resolver_result_fn, girt_resolverarg *arg);
			void (*free)(girt_plugin_context *context);
		},
		"init", "resolve", "free"
)

girt_resolver *
girt_resolver_init(girt_proxy *proxy, girt_cfg *resolver_config, char *type, char *attr_in, char *attr_out, char *attr_status)
{
	girt_enter();
	girt_resolver *resolver = girt_malloc(sizeof(*resolver));
	defer { girt_if_fail { girt_resolver_free(resolver); } }

	resolver->proxy = proxy;

	resolver->attr_in = girt_strdup(attr_in);
	resolver->attr_out = girt_strdup(attr_out);
	resolver->attr_status = girt_strdup(attr_status);

	resolver->plugin = girt_plugins_load_resolver(girt_root_plugins(girt_proxy_root(proxy)), type);
	if (resolver->plugin  == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to load resolver\n");
		girt_fail(NULL);
	}
	resolver->context = girt_plugin_resolver_funcs(resolver->plugin)->init(resolver, resolver_config);
	if (resolver->context == NULL) {
		girt_loc_print(ERROR, girt_proxy_name(proxy), "Failed to initialise resolver\n");
		girt_fail(NULL);
	}

	girt_return(resolver);
}

void
girt_resolver_free(girt_resolver *resolver)
{
	if (resolver == NULL) {
		return;
	}

	if (resolver->plugin != NULL && resolver->context != NULL) {
		girt_plugin_resolver_funcs(resolver->plugin)->free(resolver->context);
	}

	free(resolver->attr_in);
	free(resolver->attr_out);
	free(resolver->attr_status);

	memset(resolver, 0, sizeof(*resolver));

	free(resolver);
}

static void
resolver_result_callback(int errcode, struct addrinfo *ai, void *arg)
{
	girt_resolver *resolver = ((girt_resolverarg *)arg)->resolver;
	girt_flow *flow = ((girt_resolverarg *)arg)->flow;

	free(arg);

	if (errcode == 0) {
		girt_flow_set_addrinfo(flow, resolver->attr_out, ai);
		freeaddrinfo(ai);
		girt_flow_attrs(put_lit_int, flow, resolver->attr_status, evutil_gai_strerror(errcode), 1);
	} else {
		char *host = girt_flow_attrs(get_str, flow, girt_flow_attrs(mkkey, flow, resolver->attr_in, "host"));
		char *port = girt_flow_attrs(get_str, flow, girt_flow_attrs(mkkey, flow, resolver->attr_in, "port"));

		kstring_t msg[1] = {{0}};
		ksprintf(msg, "Cannot resolve %s '%s:%s': %s", resolver->attr_in, host, port, evutil_gai_strerror(errcode));
		girt_flow_attrs(put_str_int, flow, resolver->attr_status, ks_release(msg), errcode);
	}
}

void
girt_resolver_resolve(girt_resolver *resolver, girt_flow *flow)
{
	girt_resolverarg *arg = girt_malloc(sizeof(*arg));

	arg->resolver = resolver;
	arg->context = resolver->context;
	arg->flow = flow;

	char *host = girt_flow_attrs(get_str, flow, girt_flow_attrs(mkkey, flow, resolver->attr_in, "host"));
	char *port = girt_flow_attrs(get_str, flow, girt_flow_attrs(mkkey, flow, resolver->attr_in, "port"));

	if (host == NULL || port == NULL) {
		kstring_t msg[1] = {{0}};
		ksprintf(msg, "No host name and/or port number available for resolution in attr '%s.{host,port}'", resolver->attr_in);
		girt_flow_attrs(put_str_int, flow, resolver->attr_status, ks_release(msg), EAI_NONAME);
	} else {
		girt_plugin_resolver_funcs(resolver->plugin)->resolve(host, port, resolver_result_callback, arg);
	}
}

girt_flow *
girt_resolverarg_flow(girt_resolverarg *arg)
{
	return arg->flow;
}

void *
girt_resolverarg_local(girt_resolverarg *arg)
{
	return arg->context;
}

void *
girt_resolver_local(girt_resolver *resolver)
{
	return resolver->context;
}

girt_proxy *
girt_resolver_proxy(girt_resolver *resolver)
{
	return resolver->proxy;
}


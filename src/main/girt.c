#include <girt/girt.h>

static void *girt(void *arg);

/*
 * Main entry point.
 */
int
main(int argc, char *argv[])
{
	evthread_use_pthreads();

	pthread_t t;
	pthread_create(&t, NULL, girt, girt_args(argc, argv));

	intptr_t exit_code = 0;
	pthread_join(t, (void **)&exit_code);

	exit(exit_code);
}

static void *
girt(void *arg)
{
	girt_str_vec *args = arg;

	prctl(PR_SET_NAME, "girt-main");

	girt_root *root = girt_root_init(args);
	if (root != NULL && girt_root_exit_code(root) == 0) {

		girt_root_loop(root);

		girt_print(INFO, "Exiting");
	}

	return (void *)girt_root_free(root);
}

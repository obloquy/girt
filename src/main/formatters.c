#include <girt/girt.h>

#include <private/formatters.h>

girt_formatters *
girt_formatters_init(girt_log *logger, girt_cfg *logger_config)
{
	girt_enter();
	girt_formatters *formatters = girt_malloc(sizeof(*formatters));
	defer { girt_if_fail { girt_formatters_free(formatters); } }

	kv_init(formatters->formatters);

	// @config <formatters> formatter.<name> <formatter>
	// @config-desc Configuration table for the named formatter
	girt_cfg_vec *formatter_configs = girt_config_get_subtables(logger_config, "formatter");
	defer { girt_config_free_subtables(formatter_configs); }

	for (size_t i = 0; i < kv_size(*formatter_configs); i++) {
		girt_cfg *formatter_config = kv_A(*formatter_configs, i);
		char *name = girt_config_get_table_name(formatter_config);
		defer { free(name); }
		girt_formatter *formatter = girt_formatter_init(logger, formatter_config, name);
		if (formatter == NULL) {
			girt_fail(NULL);
		}
		kv_push(girt_formatter *, formatters->formatters, formatter);
	}

	return formatters;
}

void
girt_formatters_free(girt_formatters *formatters)
{
	for (size_t i = 0; i < kv_size(formatters->formatters); i++) {
		girt_formatter_free(kv_A(formatters->formatters, i));
	}

	kv_destroy(formatters->formatters);

	memset(formatters, 0, sizeof(*formatters));

	free(formatters);
}

#include <girt/girt.h>

void
girt_oom()
{
	static const char oom[] = GIRT_NAME ": Out of memory\n";
	ssize_t dummy = write(2, oom, sizeof(oom));
	exit(EXIT_FAILURE+dummy-dummy);
}

char *
girt_inet_ntop(struct sockaddr *sa, char *buf, socklen_t bufsize)
{
	int offset = (sa->sa_family == AF_INET ? offsetof(struct sockaddr_in, sin_addr) : offsetof(struct sockaddr_in6, sin6_addr));

	return (char *)inet_ntop(sa->sa_family, ((char *)sa) + offset, buf, bufsize);
}

uint16_t
girt_sockaddr_port(struct sockaddr *sa)
{
	switch (sa->sa_family) {
		case AF_INET:
			return ((struct sockaddr_in *)sa)->sin_port;
		case AF_INET6:
			return ((struct sockaddr_in6 *)sa)->sin6_port;
	}

	return 0;
}

char *
girt_addrinfo_to_addrname(struct addrinfo *ai, char **host, char **port)
{
	char *addrname = NULL;

	switch (ai->ai_family) {
		case AF_INET:
		{
			char buf[INET_ADDRSTRLEN] = {0};
			if (inet_ntop(ai->ai_family, &((struct sockaddr_in *)ai->ai_addr)->sin_addr, buf, sizeof(buf)) != NULL) {
				addrname = girt_asprintf("%s:%d", buf, ntohs(((struct sockaddr_in *)ai->ai_addr)->sin_port));
				if (host != NULL) {
					*host = girt_strdup(buf);
				}
				if (port != NULL) {
					*port = girt_asprintf("%d", ntohs(((struct sockaddr_in *)ai->ai_addr)->sin_port));
				}
			}
			break;
		}
		case AF_INET6:
		{
			char buf[INET6_ADDRSTRLEN] = {0};
			if (inet_ntop(ai->ai_family, &((struct sockaddr_in6 *)ai->ai_addr)->sin6_addr, buf, sizeof(buf)) != NULL) {
				addrname = girt_asprintf("[%s]:%d", buf, ntohs(((struct sockaddr_in6 *)ai->ai_addr)->sin6_port));
				if (host != NULL) {
					*host = girt_strdup(buf);
				}
				if (port != NULL) {
					*port = girt_asprintf("%d", ntohs(((struct sockaddr_in6 *)ai->ai_addr)->sin6_port));
				}
			}
			break;
		}
		default:
			addrname = girt_asprintf("AF=%d", ai->ai_family);
			break;
	}

	return addrname;
}

char *
girt_sockaddr_to_addrname(struct sockaddr *sa, char **host, char **port)
{
	char *addrname = NULL;

	switch (sa->sa_family) {
		case AF_INET:
		{
			char buf[INET_ADDRSTRLEN] = {0};
			if (inet_ntop(sa->sa_family, &((struct sockaddr_in *)sa)->sin_addr, buf, sizeof(buf)) != NULL) {
				addrname = girt_asprintf("%s:%d", buf, ntohs(((struct sockaddr_in *)sa)->sin_port));
				if (host != NULL) {
					*host = girt_strdup(buf);
				}
				if (port != NULL) {
					*port = girt_asprintf("%d", ntohs(((struct sockaddr_in *)sa)->sin_port));
				}
			}
			break;
		}
		case AF_INET6:
		{
			char buf[INET6_ADDRSTRLEN] = {0};
			if (inet_ntop(sa->sa_family, &((struct sockaddr_in6 *)sa)->sin6_addr, buf, sizeof(buf)) != NULL) {
				addrname = girt_asprintf("[%s]:%d", buf, ntohs(((struct sockaddr_in6 *)sa)->sin6_port));
				if (host != NULL) {
					*host = girt_strdup(buf);
				}
				if (port != NULL) {
					*port = girt_asprintf("%d", ntohs(((struct sockaddr_in6 *)sa)->sin6_port));
				}
			}
			break;
		}
		default:
			addrname = girt_asprintf("AF=%d", sa->sa_family);
			break;
	}

	return addrname;
}

int
girt_socket(int family, int type, int protocol)
{
#ifdef SOCK_NONBLOCK
	type |= SOCK_NONBLOCK;
#endif

	int fd = socket(family, type, protocol);
	if (fd < 0) {
		girt_printf(ERROR, "Failed to create socket: %s\n", strerror(errno));
	}

#ifndef SOCK_NONBLOCK
	else
	if (fcntl(fd, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK) < 0) {
		girt_printf(ERROR, "Failed to make socket nonblocking: %s\n", strerror(errno));
		close(fd);
		fd = -1;
	}
#endif

	return fd;
}

void *
girt_malloc(size_t len)
{
	void *ptr = malloc(len);

	if (ptr == NULL) {
		girt_oom();
	}

	return memset(ptr, 0, len);
}

void *
girt_memdup(void *ptr, size_t len)
{
	if (ptr == NULL) {
		return NULL;
	}

	return memcpy(girt_malloc(len), ptr, len);
}

char *
girt_strdup(const char *str)
{
	if (str == NULL) {
		return NULL;
	}

	size_t slen = strlen(str);

	return memcpy(girt_malloc(slen+1), str, slen);
}

char *
girt_strndup(const char *str, size_t len)
{
	if (str == NULL) {
		return NULL;
	}

	size_t slen = strlen(str);
	if (len > slen) {
		len = slen;
	}

	return memcpy(girt_malloc(len+1), str, len);
}

char *
girt_strlower(char *str)
{
	for (char *ptr = str; ptr && *ptr; ptr++) {
		*ptr = tolower(*ptr);
	}

	return str;
}

char *
girt_unquote(char *str)
{
	for (char *ptr = str; ptr && *ptr; ptr++) {
		if (*ptr == '"') {
			*ptr = '\'';
		}
	}

	return str;
}

char *girt_strnstr(const char *s, const char *find, size_t slen)
{
	char c, sc;
	size_t len;

	if ((c = *find++) != '\0') {
		len = strlen(find);
		do {
			do {
				if (slen-- < 1 || (sc = *s++) == '\0')
					return (NULL);
			} while (sc != c);
			if (len > slen)
				return (NULL);
		} while (strncmp(s, find, len) != 0);
		s--;
	}
	return ((char *)s);
}

uint64_t
girt_current_millis()
{
	struct timeval tv[1];

	gettimeofday(tv, NULL);

	return (uint64_t)tv->tv_sec * 1000 + (uint64_t)tv->tv_usec / 1000;
}

size_t
girt_strncpy(char *buf, size_t buflen, char *str)
{
	size_t buflenz = buflen-1;
	size_t lenz = strlen(str);

	if (lenz > buflenz) {
		strncpy(buf, str, buflenz);
		buf[buflenz] = '\0';
		return buflenz;
	}

	strcpy(buf, str);
	return lenz;
}

char *
girt_strappend_v(char *buf, size_t buflen, char *str, ...)
{
	size_t len = 0;
	va_list args;
	va_start(args, str);

	do {
		len += girt_strncpy(buf+len, buflen-len, str);
	} while ((str = va_arg(args, char *)) != NULL);

	return buf;
}

void
girt_close_p(int *fd)
{
	if (fd != NULL && *fd >= 0) {
		if (close(*fd) != 0) {
			girt_printf(ERROR, "Failed to close file descriptor %d: %s", *fd, strerror(errno));
		}
		*fd = -1;
	}
}

void
girt_free_pp(void **ptr)
{
	if (ptr != NULL) {
		girt_free(*ptr);
		*ptr = NULL;
	}
}

void
girt_bufferevent_free_pp(void **vpp)
{
	if (vpp != NULL) {
		struct bufferevent **bev = (struct bufferevent **)vpp;
		if (*bev != NULL) {
			bufferevent_free(*bev);
		}
		*vpp = NULL;
	}
}

void
girt_bufferevent_pair_free_pp(void **vpp)
{
	if (vpp == NULL) {
		return;
	}

	struct bufferevent ***bevppp = (struct bufferevent ***)vpp;

	if (*bevppp != NULL) {
		for (int i = 0; i < 2; i++) {
			if ((*bevppp)[i] != NULL) {
				girt_bufferevent_free_pp((void **)&(*bevppp)[i]);
			}
		}
		free(*bevppp);
	}

	*vpp = NULL;
}

void
girt_evbuffer_free_pp(void **vpp)
{
	if (vpp != NULL && *vpp != NULL) {
		evbuffer_free((struct evbuffer *)*vpp);
		*vpp = NULL;
	}
}

void
girt_event_free_pp(void **vpp)
{
	if (vpp != NULL && *vpp != NULL) {
		event_free((struct event *)*vpp);
		*vpp = NULL;
	}
}

void
girt_evbuffer_ref_free(const void *data, UNUSED size_t datalen, UNUSED void *extra)
{
	free((void *)data);
}

void
girt_hexdump(girt_str_vec *vec, void *data, size_t datalen, char *prefix, char *xx, int dot)
{
	unsigned char *p, *q;
	int i;
 
	p = data;
	size_t rem = datalen;
	while (rem > 0) {
		int buflen = strlen(prefix)+6+(16*3)+2+16+3+1 + 20;
		char buf[buflen];
		int len = 0;
		q = p;
		len += snprintf(buf+len, buflen-len, "%s%04lx: ", prefix, datalen-rem);
		for (i = 0; i < 16 && rem > 0; ++i) {
			len += snprintf(buf+len, buflen-len, "%02x ", *p);
			++p;
			--rem;
		}
		rem += i;
		while (i < 16) {
			len += snprintf(buf+len, buflen-len, "%s", xx);
			++i;
		}
		len += snprintf(buf+len, buflen-len, "| ");
		p = q;
		for (i = 0; i < 16 && rem > 0; ++i) {
			len += snprintf(buf+len, buflen-len, "%c", isprint(*p) && !isspace(*p) ? *p : dot);
			++p;
			--rem;
		}
		while (i < 16) {
			len += snprintf(buf+len, buflen-len, " ");
			++i;
		}
		len += snprintf(buf+len, buflen-len, " |\n");
		kv_push(char *, *vec, girt_strdup(buf));
	}
	return;
}

girt_str_vec *
girt_str_vec_init()
{
	girt_str_vec *vec = girt_malloc(sizeof(*vec));

	kv_init(*vec);

	return vec;
}

void
girt_str_vec_free(girt_str_vec *vec)
{
	if (vec == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(*vec); i++) {
		free(kv_A(*vec, i));
	}

	kv_destroy(*vec);

	free(vec);
}

int
girt_str_vec_printf(girt_str_vec *vec, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	int n = vasprintf(kv_pushp(char *, *vec), fmt, args);
	va_end(args);

	return n;
}

void
girt_str_vec_push(girt_str_vec *vec, const char *str)
{
	kv_push(char *, *vec, girt_strdup(str));
}

girt_str_vec *
girt_args(int argc, char *argv[])
{
	girt_str_vec *args = girt_str_vec_init();
	kv_init(*args);

	for (int i = 0; i < argc; i++) {
		kv_push(char *, *args, girt_strdup(argv[i]));
	}

	return args;
}

void
girt_millisleep(uint64_t millis)
{
	struct timespec rqtp = { .tv_sec = millis / 1000, .tv_nsec = (millis % 1000) * 1000000 };
	struct timespec rmtp = { 0 };

	while (nanosleep(&rqtp, &rmtp) == -1 && errno == EINTR && (rmtp.tv_sec > 0 || rmtp.tv_nsec > 0)) {
		rqtp = rmtp;
	}
}


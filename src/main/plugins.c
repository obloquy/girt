#include <girt/girt.h>

#include <private/plugins.h>

girt_plg *
girt_plugins_init(girt_root *root)
{
	girt_plg *plg = girt_malloc(sizeof(*plg));

	plg->root = root;

	plg->plugins = girt_check_oom_ptr(kh_init(plugins));

	return plg;
}

void
girt_plugins_free(girt_plg *plg)
{
	if (plg == NULL) {
		return;
	}

	for (khiter_t k = kh_begin(plg->plugins); k != kh_end(plg->plugins); ++k) {
		if (kh_exist(plg->plugins, k)) {
			girt_plugin_free(kh_value(plg->plugins, k));
			free((void *)kh_key(plg->plugins, k));
			kh_del(plugins, plg->plugins, k); 
		}
	}
	kh_destroy(plugins, plg->plugins);

	memset(plg, 0, sizeof(*plg));
	free(plg);
}

girt_plugin *
girt_plugins_load(girt_plg *plg, const char *name, const char *type, const char * const funcnames[], const int n)
{
	int ret;
	char *pname = girt_asprintf("%s/%s", type, name);
	khiter_t k = kh_put(plugins, plg->plugins, pname, &ret);
	if (ret) {
		// new
		kh_value(plg->plugins, k) = girt_plugin_init(plg->root, name, type, funcnames, n);
	} else {
		// exists
		free(pname);
	}

	return kh_value(plg->plugins, k);
}


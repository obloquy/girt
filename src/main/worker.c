#include <girt/girt.h>

#include <private/worker.h>

static void *worker_callback(void *arg);

girt_worker *
girt_worker_init(UNUSED girt_root *root, UNUSED girt_cfg *workers_config, pthread_attr_t *attr, int idx)
{
	girt_enter();
	girt_worker *worker = girt_malloc(sizeof(*worker));
	worker->pthread_result = -1;
	defer { girt_if_fail { girt_worker_free(worker); } }

	worker->idx = idx;

	worker->evbase = girt_check_oom_ptr(event_base_new());

	worker->evdns = girt_check_oom_ptr(evdns_base_new(worker->evbase, EVDNS_BASE_INITIALIZE_NAMESERVERS));

	int err = worker->pthread_result = pthread_create(&worker->tid, attr, worker_callback, worker);
	if (err != 0) {
		girt_printf(ERROR, "Failed to start worker #%d: %s\n", idx, strerror(err));
		girt_fail(NULL);
	}

	girt_return(worker);
}

static void
worker_self_destruct_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_worker *worker = arg;

	event_base_loopbreak(worker->evbase);
}

static void
worker_self_destruct(girt_worker *worker)
{
	if (worker->evbase != NULL) {
		worker->self_destruct_ev = girt_check_oom_ptr(evtimer_new(worker->evbase, worker_self_destruct_callback, worker));
		event_add(worker->self_destruct_ev, &(struct timeval){0,0});
	}
}

void
girt_worker_free(girt_worker *worker)
{
	if (worker == NULL) {
		return;
	}

	if (worker->pthread_result == 0) {
		// started, try to stop it
		worker_self_destruct(worker);

		void *result;
		int err = worker->pthread_result = pthread_join(worker->tid, &result);
		if (err != 0) {
			girt_printf(ERROR, "Failed to join thread '%s': %s", worker->tname, strerror(err));
			if (err == ESRCH) {
				// not found
				worker->pthread_result = 0;
			}
		} else {
			girt_printf(DEBUG, "Joined thread '%s'\n", worker->tname);
		}
	} else {
		// never started
		worker->pthread_result = 0;
	}

	// thread successfully stopped or was never started
	if (worker->pthread_result == 0) {

		if (worker->self_destruct_ev != NULL) {
			event_free(worker->self_destruct_ev);
		}

		evdns_base_free(worker->evdns, 0);

		event_base_free(worker->evbase);

		memset(worker, 0, sizeof(*worker));
		free(worker);
	}
}

static void *
worker_callback(void *arg)
{
	girt_worker *worker = arg;	

	snprintf(worker->tname, sizeof(worker->tname), "worker-%03d", worker->idx);
	prctl(PR_SET_NAME, worker->tname);

	event_base_loop(worker->evbase, EVLOOP_NO_EXIT_ON_EMPTY);

	return NULL;
}

pthread_t
girt_worker_tid(girt_worker *worker)
{
	return worker->tid;
}

int
girt_worker_idx(girt_worker *worker)
{
	return worker->idx;
}

struct event_base *
girt_worker_event_base(girt_worker *worker)
{
	return worker->evbase;
}

struct evdns_base *
girt_worker_evdns_base(girt_worker *worker)
{
	return worker->evdns;
}


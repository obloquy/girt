#include <girt/girt.h>

#include <private/formatter.h>

PLUGIN_FUNCS(
		formatter,
		{
			girt_plugin_context *(*init)(girt_formatter *formatter, girt_cfg *formatter_config, log4c_layout_t *layout);
			const char *(*format)(const log4c_layout_t *layout, const log4c_logging_event_t *event);
			void (*free)(girt_plugin_context *context);
		},
		"init", "format", "free"
)

girt_formatter *
girt_formatter_init(girt_log *logger, girt_cfg *formatter_config, char *name)
{
	girt_enter();
	girt_formatter *formatter = girt_malloc(sizeof(*formatter));
	defer { girt_if_fail { girt_formatter_free(formatter); } }

	formatter->logger = logger;

	formatter->layout = log4c_layout_get(name);
	formatter->layout_type = girt_malloc(sizeof(*formatter->layout_type));

	// @config <formatter> type string The formatter's name
	// @config-desc The type of the formatter which is used as the plug-in name
	formatter->layout_type->name = girt_config_get_string(formatter_config, "type", name);
	if (formatter->layout_type == NULL) {
		girt_printf(ERROR, "No type for formatter '%s'\n", name);
		girt_fail(NULL);
	}

	formatter->plugin = girt_plugins_load_formatter(girt_root_plugins(girt_logger_root(logger)), formatter->layout_type->name);
	if (formatter->plugin == NULL) {
		girt_printf(ERROR, "Failed to load formatter '%s'\n", name);
		girt_fail(NULL);
	}
	formatter->context = girt_plugin_formatter_funcs(formatter->plugin)->init(formatter, formatter_config, formatter->layout);
	if (formatter->context == NULL) {
		girt_printf(ERROR, "Failed to initialise formatter '%s'\n", name);
		girt_fail(NULL);
	}

	formatter->layout_type->format = girt_plugin_formatter_funcs(formatter->plugin)->format;

	log4c_layout_type_set(formatter->layout_type);

	log4c_layout_set_type(formatter->layout, formatter->layout_type);

	return formatter;
}

void
girt_formatter_free(girt_formatter *formatter)
{
	if (formatter->plugin != NULL && formatter->context != NULL) {
		girt_plugin_formatter_funcs(formatter->plugin)->free(formatter->context);
	}

	free((char *)formatter->layout_type->name);
	free(formatter->layout_type);

	memset(formatter, 0, sizeof(*formatter));

	free(formatter);
}

#include <girt/girt.h>

#include <private/proxies.h>

girt_pxy *
girt_proxies_init(girt_root *root)
{
	girt_enter();
	girt_pxy *pxy = girt_malloc(sizeof(*pxy));
	defer { girt_if_fail { girt_proxies_free(pxy); } }

	pxy->proxies = girt_check_oom_ptr(kh_init(pxy));

	pxy->root = root;

	// @config <root> proxies string[]
	// @config-desc List of names of proxies to start. This allows proxies to be defined in the configuration but
	// @config-desc not neccessarily be started.
	girt_str_vec *names = girt_config_get_string_array(girt_root_config(root), "proxies");
	defer { girt_config_free_string_array(names); }
	if (kv_size(*names) == 0) {
		girt_print(ERROR, "No proxies enabled\n");
		girt_fail(NULL);
	}

	// @config <root> proxy <proxies> -
 	girt_cfg *all_config = girt_config_get_table(girt_root_config(root), "proxy");
	defer { girt_config_free_table(all_config); }

	for (size_t i = 0; i < kv_size(*names); i++) {
		char *name = girt_strdup(kv_A(*names, i));
		// @config <proxies> <name> <proxy>
		girt_cfg *proxy_config = girt_config_get_table(all_config, name);
		defer { girt_config_free_table(proxy_config); }
		if (girt_config_root_table_count(proxy_config) == 0) {
			girt_printf(ERROR, "Missing proxy '%s'\n", name);
			girt_fail(NULL);
		}
		// @config <proxy> type string "default"
		// @config-desc The type of the proxy which is used as the plug-in name
		char *type = girt_config_get_string(proxy_config, "type", "default");
		defer { free(type); }

		int ret;
		khiter_t k = kh_put(pxy, pxy->proxies, name, &ret);
		if (ret) {
			// new
			kh_value(pxy->proxies, k) = girt_proxy_init(root, proxy_config, type);
			if (kh_value(pxy->proxies, k) == NULL) {
				girt_fail(NULL);
			}
		} else {
			// exists
			girt_printf(ERROR, "Duplicate proxy name '%s'", name);
			girt_fail(NULL);
		}
	}

	girt_return(pxy);
}

void
girt_proxies_free(girt_pxy *pxy)
{
	if (pxy == NULL) {
		return;
	}

	for (khiter_t k = kh_begin(pxy->proxies); k != kh_end(pxy->proxies); ++k) {
		if (kh_exist(pxy->proxies, k)) {
			girt_proxy_free(kh_value(pxy->proxies, k));
			free((void *)kh_key(pxy->proxies, k));
			kh_del(pxy, pxy->proxies, k); 
		}
	}
	kh_destroy(pxy, pxy->proxies);

	memset(pxy, 0, sizeof(*pxy));
	free(pxy);
}

girt_root *
girt_proxies_root(girt_pxy *proxies)
{
	return proxies->root;
}

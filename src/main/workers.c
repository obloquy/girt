#include <girt/girt.h>

#include <private/workers.h>

girt_wrk *
girt_workers_init(girt_root *root)
{
	girt_enter();
	girt_wrk *wrk = girt_malloc(sizeof(*wrk));
	defer { girt_if_fail { girt_workers_free(wrk); } }

	wrk->root = root;

	kv_init(wrk->workers);

	// @config <root> workers <workers> -
	girt_cfg *workers_config = girt_config_get_table(girt_root_config(root), "workers");
	defer { girt_config_free_table(workers_config); }

	// @config <workers> thread <pthread_attr>
	// @config-desc Thread attributes for all workers
	pthread_attr_t *attr = girt_config_get_pthread_attr(workers_config, "thread");
	defer { girt_config_free_pthread_attr(attr); }

	// @config <workers> threads i64 Twice the CPU count from /proc/cpuinfo. The CPU count can also be checked form the command line with 'nproc'
	// @config-desc The number of worker threads to start
	int n_workers = girt_config_get_i64(workers_config, "threads", 2 * girt_limits_get_cpu_count());

	for (int i = 0; i < n_workers; i++) {
		girt_worker *worker = girt_worker_init(root, workers_config, attr, i);
		if (worker == NULL) {
			girt_fail(NULL);
		}
		kv_push(girt_worker *, wrk->workers, worker);
	}

	if (kv_size(wrk->workers) == 0) {
		girt_print(ERROR, "No workers started\n");
		girt_fail(NULL);
	}

	if (girt_is_priority_enabled(DEBUG)) {
		girt_str_vec *vec = girt_str_vec_init();
		girt_str_vec_printf(vec, "Started %d workers with the following attributes:\n", n_workers);
		girt_limits_dump_pthread_attr_for_tid(vec, girt_worker_tid(kv_A(wrk->workers, 0)));
		for (size_t i = 0; i < kv_size(*vec); i++) {
			girt_print(DEBUG, kv_A(*vec, i));
		}
		girt_str_vec_free(vec);
	} else {
		girt_printf(INFO, "Started %d workers", n_workers);
	}

	girt_return(wrk);
}

void
girt_workers_free(girt_wrk *wrk)
{
	if (wrk == NULL) {
		return;
	}

	for (size_t i = 0; i < kv_size(wrk->workers); i++) {
		girt_worker_free(kv_A(wrk->workers, i));
	}
	kv_destroy(wrk->workers);

	memset(wrk, 0, sizeof(*wrk));
	free(wrk);
}

girt_worker *
girt_workers_next(girt_wrk *wrk)
{
	if (wrk->next >= kv_size(wrk->workers)) {
		wrk->next = 0;
	}

	return kv_A(wrk->workers, wrk->next++);
}

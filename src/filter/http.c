
#include <girt/girt.h>

typedef struct girt_local girt_local;

#include <girt/http_parser.h>

typedef struct {
	girt_link *link;
	kstring_t curr_field[1];
	kstring_t curr_value[1];
	kstring_t curr_start[1];
	kstring_t scheme[1];
	kstring_t hostport[1];
	kstring_t path[1];
	unsigned int init_done:1;
	unsigned int start_line_done:1;
} http_parser_context;

static ssize_t http_parse(struct bufferevent *bev, girt_link *link);
static void http_parse_url(char *buf, size_t buflen, int is_connect, kstring_t *scheme, kstring_t *hostport, kstring_t *path);

girt_local *
_girt_filter_init(UNUSED girt_filter *filter, UNUSED girt_cfg *filter_config)
{
	return girt_malloc(1);
}

void
_girt_filter_free(UNUSED girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_free(local);
}

void
_girt_filter_event(UNUSED struct bufferevent *bev, short events, void *arg)
{
	girt_link *link = arg;
//	girt_flow *flow = girt_link_flow(link);
	int from_orig = girt_link_from_orig(link);
//	girt_local *local = girt_link_filter_arg(link);

	girt_printf(DEBUG, "http: Filter event %s %04x", from_orig ? "orig" : "dest", events);
}

void
_girt_filter_read(struct bufferevent *bev, void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);

	if (girt_flow_attrs(has_all_tags, flow, "http", "!ssl")) {
		http_parse(bev, link);
	}

	// Copy any/all remaining data out
	bufferevent_write_buffer(bev, bufferevent_get_input(bev));
}

static void
http_handle_completed_header(http_parser *parser)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	struct evbuffer *output = girt_link_evbuf(context->link);

	if (ks_len(context->curr_field) > 0) {
		if (ks_len(context->hostport) > 0 && !strcasecmp(ks_str(context->curr_field), "host") && strcasecmp(ks_str(context->curr_value), ks_str(context->hostport))) {
			girt_flow_printf(INFO, "HTTP host header (%s) and start line host (%s) mismatch, using start line", ks_str(context->curr_value), ks_str(context->hostport));
			context->curr_value->l = 0;
			kputs(ks_str(context->hostport), context->curr_value);
		}

		if (strcasecmp(ks_str(context->curr_field), "connection") &&
				strcasecmp(ks_str(context->curr_field), "proxy-connection")) {
			evbuffer_add_printf(output, "%s: %s\r\n", ks_str(context->curr_field), ks_str(context->curr_value));
		}

		if (!strcasecmp(ks_str(context->curr_field), "host") || !strcasecmp(ks_str(context->curr_field), "user-agent")) {
			girt_flow_printf(INFO, girt_link_flow(context->link), "HEADER %s=%s\n", girt_strlower(ks_str(context->curr_field)), ks_str(context->curr_value));
		}

		context->curr_field->l = context->curr_value->l = 0;
	}
}

static void
http_on_start_line_complete(http_parser* parser)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	if (!context->start_line_done) {

		context->start_line_done = 1;

		if (parser->type == HTTP_REQUEST) {
			context->scheme->l = context->hostport->l = context->path->l = 0;
			http_parse_url(ks_str(context->curr_start), ks_len(context->curr_start), parser->method == HTTP_CONNECT, context->scheme, context->hostport, context->path);
			kstring_t msg[1] = {{0}};
			ksprintf(msg, "HTTP %s", http_method_str(parser->method));
			if (ks_len(context->scheme) > 0) {
				ksprintf(msg, " scheme=%s", ks_str(context->scheme));
			}
			if (ks_len(context->hostport) > 0) {
				ksprintf(msg, " hostport=%s", ks_str(context->hostport));
			}
			ksprintf(msg, " path=%s v=%d.%d\n",
				ks_len(context->path) == 0 ? "/" : ks_str(context->path),
				parser->http_major,
				parser->http_minor
			);
			girt_flow_print(INFO, girt_link_flow(context->link), ks_str(msg));
			free(ks_release(msg));
			evbuffer_add_printf(girt_link_evbuf(context->link),
				"%s %s HTTP/%d.%d\r\n",
				http_method_str(parser->method),
				ks_len(context->path) == 0 ? "/" : ks_str(context->path),
				parser->http_major,
				parser->http_minor
			);
		} else {
			girt_flow_printf(INFO, girt_link_flow(context->link), "HTTP status=%d msg=\"%s\" v=%d.%d\n",
				parser->status_code,
				girt_unquote(ks_str(context->curr_start)),
				parser->http_major,
				parser->http_major
			);
			evbuffer_add_printf(girt_link_evbuf(context->link),
				"HTTP/%d.%d %d %s\r\n",
				parser->http_major,
				parser->http_major,
				parser->status_code,
				ks_str(context->curr_start)
			);
		}
	}
}

/*
 * Call-back for HTTP parser when request URL is received.
 *
 * It is possible to receive the URL in chunks because the parser
 * doesn't buffer.
 */
static int
http_on_url(http_parser* parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	// append to the current url
	kputsn((char *)at, len, context->curr_start);

	return 0;
}

static int
http_on_status(http_parser* parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	// append to the current status
	kputsn((char *)at, len, context->curr_start);

	return 0;
}

/*
 * Call-back for HTTP parser when header parsing is complete
 */
static int
http_on_headers_complete(http_parser* parser)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	if (!context->start_line_done) {
		http_on_start_line_complete(parser);
	}

	if (ks_len(context->curr_field) > 0) {
		http_handle_completed_header(parser);
	}

	struct evbuffer *output = girt_link_evbuf(context->link);

	evbuffer_add_printf(output, "Connection: close\r\n");
	evbuffer_add(output, "\r\n", 2);

#if 0 // TODO // Handle weird case of telling parser not to expect response body after head request
    if (parser->type == HTTP_REQUEST) {
        side->conn->prev_http_method = parser->method;
    }

    return parser->type == HTTP_RESPONSE && side->conn->prev_http_method == HTTP_HEAD;
#endif
	return 0;
}


/**
 * Call-back for HTTP parser when a header field (name) is encountered. The name ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int
http_on_header_field(http_parser *parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	if (!context->start_line_done) {
		http_on_start_line_complete(parser);
	}

	if (ks_len(context->curr_value) > 0) {
		if (ks_len(context->curr_field) > 0) {
			// Previous header complete
			http_handle_completed_header(parser);
		} else {
			// No name
			context->curr_field->l = context->curr_value->l = 0;
			return 1;
		}
	}

	// append to the current header field
	kputsn((char *)at, len, context->curr_field);

	return 0;
}

/**
 * Call-back for HTTP parser when a header value is encountered. The value ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int
http_on_header_value(http_parser *parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	if (ks_len(context->curr_field) == 0) {
		// no header field, fail
		return 1;
	}

	kputsn((char *)at, len, context->curr_value);

	return 0;
}

static int
http_on_body(UNUSED http_parser *parser, UNUSED const char *at, UNUSED size_t length)
{
	http_parser_context *context = (http_parser_context *)parser->data;

	evbuffer_add(girt_link_evbuf(context->link), at, length);

	return 0;
}

static int
http_on_message_complete(UNUSED http_parser* parser)
{
	// reinit parser

	return 0;
}

static http_parser_settings parser_settings[1] = {{
	.on_url = http_on_url,
	.on_status = http_on_status,
	.on_header_field = http_on_header_field,
	.on_header_value = http_on_header_value,
	.on_headers_complete = http_on_headers_complete,
	.on_body = http_on_body,
	.on_message_complete = http_on_message_complete
}};

static void
http_parser_context_free_pp(void **vpp)
{
	if (vpp != NULL) {
		http_parser_context *context = (http_parser_context *)(*vpp);
		if (context != NULL) {
			free(ks_release(context->curr_field));
			free(ks_release(context->curr_value));
			free(ks_release(context->curr_start));
			free(ks_release(context->scheme));
			free(ks_release(context->hostport));
			free(ks_release(context->path));
		}
		free(*vpp);
		*vpp = NULL;
	}
}

static ssize_t
http_parse(struct bufferevent *bev, girt_link *link)
{
	girt_flow *flow = girt_link_flow(link);
	int from_orig = girt_link_from_orig(link);

	http_parser_context *context = girt_flow_attrs(get_or_put_ptr, flow, 
		girt_flow_attrs(mkkey, flow, "filter", from_orig ? "orig" : "dest", "http.context"),
		sizeof(http_parser_context),
		http_parser_context_free_pp
	);
	http_parser *parser = girt_flow_attrs(get_or_put_ptr, flow,
		girt_flow_attrs(mkkey, flow, "filter", from_orig ? "orig" : "dest", "http.parser"),
		sizeof(http_parser),
		girt_free_pp
	);

	if (!context->init_done) {
		context->link = link;
		context->init_done = 1;
		http_parser_init(parser, from_orig ? HTTP_REQUEST : HTTP_RESPONSE);
		parser->data = context;
	}

	size_t total_used = 0;
	struct evbuffer *input = bufferevent_get_input(bev);
	int n = evbuffer_peek(input, -1, NULL, NULL, 0);
	struct evbuffer_iovec iov[n];
	n = evbuffer_peek(input, -1, NULL, iov, n);
	for (int i = 0; i < n; i++) {
		size_t used = http_parser_execute(parser, parser_settings, iov[i].iov_base, iov[i].iov_len);

		if (used != iov[i].iov_len) {
			girt_flow_printf(DEBUG, flow, "http: Only used %lu of %lu\n", used, iov[i].iov_len);
			return -1;
		} else
		if (HTTP_PARSER_ERRNO(parser) != HPE_OK) {
			girt_flow_printf(DEBUG, flow, "http: (%d) %s\n", HTTP_PARSER_ERRNO(parser), http_errno_name(HTTP_PARSER_ERRNO(parser)));
			return -1;
		}

		total_used += used;
	}

	evbuffer_drain(bufferevent_get_input(bev), total_used);

	return 0;
}

static void
http_parse_url(char *buf, size_t buflen, int is_connect, kstring_t *scheme, kstring_t *hostport, kstring_t *path)
{
	struct http_parser_url url_fields[1] = {{0}};

	if (http_parser_parse_url(buf, buflen, is_connect, url_fields) == 0) {

		if ((url_fields->field_set & (1<<UF_SCHEMA)) != 0) {
			kputsn(&buf[url_fields->field_data[UF_SCHEMA].off], url_fields->field_data[UF_SCHEMA].len, scheme);
		}

		if ((url_fields->field_set & (1<<UF_HOST)) != 0 && url_fields->field_data[UF_HOST].len > 0) {
			kputsn(&buf[url_fields->field_data[UF_HOST].off], url_fields->field_data[UF_HOST].len, hostport);
			if ((url_fields->field_set & (1<<UF_PORT)) != 0 && url_fields->field_data[UF_PORT].len > 0) {
				kputc(':', hostport);
				kputsn(&buf[url_fields->field_data[UF_PORT].off], url_fields->field_data[UF_PORT].len, hostport);
			}
		}

		if ((url_fields->field_set & (1<<UF_PATH)) != 0 && url_fields->field_data[UF_PATH].len > 0) {
			size_t len = url_fields->field_data[UF_PATH].len +
				((url_fields->field_set & (1<<UF_QUERY)) != 0 ? url_fields->field_data[UF_QUERY].len : 0) + 
				((url_fields->field_set & (1<<UF_FRAGMENT)) != 0 ? url_fields->field_data[UF_FRAGMENT].len : 0);
			kputsn(&buf[url_fields->field_data[UF_PATH].off], len, path);
		}
	}
}

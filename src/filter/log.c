
#include <girt/girt.h>

typedef struct girt_local girt_local;

#define GIRT_LOGGER_CONTENT "content"
int girt_logger_level_CONTENT = GIRT_LOGGER_INFO;

girt_local *
_girt_filter_init(UNUSED girt_filter *filter, UNUSED girt_cfg *filter_config)
{
	return girt_malloc(1);
}

void
_girt_filter_free(UNUSED girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_free(local);
}

void
_girt_filter_read(struct bufferevent *bev, UNUSED void *arg)
{
	girt_link *link = arg;
	girt_flow *flow = girt_link_flow(link);
	int from_orig = girt_link_from_orig(link);
//	girt_local *local = girt_link_filter_arg(link);

	struct evbuffer *input = bufferevent_get_input(bev);

	struct evbuffer *tmp = evbuffer_new();
	defer { evbuffer_free(tmp); }
	
	int is_all_printable = 1;
	int n = evbuffer_peek(input, -1, NULL, NULL, 0);
	struct evbuffer_iovec iov[n];
	n = evbuffer_peek(input, -1, NULL, iov, n);
	for (int i = 0; is_all_printable && i < n; i++) {
		evbuffer_add_reference(tmp, iov[i].iov_base, iov[i].iov_len, NULL, NULL);
		for (size_t j = 0; j < iov[i].iov_len; j++) {
			int c = ((char*)iov[i].iov_base)[j];
			if (!(is_all_printable = ((c >= 0x20 && c < 0x7f) || c == '\n' || c == '\r' || c == '\t'))) {
				break;
			}
		}
	}

	if (is_all_printable) {
		char *line;
		while ((line = evbuffer_readln(tmp, NULL, EVBUFFER_EOL_CRLF))) {
			girt_logger_printf(CONTENT, INFO, flow, "%c %s\n", from_orig ? '>' : '<', line);
			free(line);
		}

		size_t len = evbuffer_get_length(tmp);
		if (len > 0 && len < 1024) {
			char buf[len+1];
			memset(buf, 0, sizeof(buf));
			evbuffer_remove(tmp, buf, len);
			girt_logger_printf(CONTENT, INFO, flow, "%c %s\n", from_orig ? '>' : '<', buf);
		}
	}

	bufferevent_write_buffer(bev, input);
}

void
_girt_filter_event(UNUSED struct bufferevent *bev, short events, void *arg)
{
	girt_link *link = arg;
//	girt_flow *flow = girt_link_flow(link);
	int from_orig = girt_link_from_orig(link);
//	girt_local *local = girt_link_filter_arg(link);

	girt_printf(DEBUG, "log: Filter event %s %04x", from_orig ? "orig" : "dest", events);
}


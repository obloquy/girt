
#include <girt/girt.h>

typedef struct {
	int fh;
} girt_local;

girt_local *
_girt_appender_init(UNUSED girt_appender *appender, girt_cfg *appender_config, log4c_appender_t *log4c_appender)
{
	girt_local *local = girt_malloc(sizeof(*local));

	// @config <appender:stdio> fh i64 2 (stderr)
	// @config-desc The file handle number to log to.
	local->fh = girt_config_get_i64(appender_config, "fh", 2);

	log4c_appender_set_udata(log4c_appender, local);

	return local;
}

void
_girt_appender_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	memset(local, 0, sizeof(*local));
	local->fh = -1;

	free(local);
}

int
_girt_appender_open(UNUSED log4c_appender_t *appender)
{
	return 0;
}

int
_girt_appender_append(log4c_appender_t *appender, const log4c_logging_event_t *event)
{
	girt_local *local = log4c_appender_get_udata(appender);

	return write(local->fh, event->evt_rendered_msg, strlen(event->evt_rendered_msg));
}

int
_girt_appender_close(UNUSED log4c_appender_t *appender)
{
	return 0;
}

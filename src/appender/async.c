
#include <girt/girt.h>

#undef UNUSED

#include <logger.h>

typedef struct {
	char *filename;
	logger_t *logger;
	int fh;
} girt_local;

static int local_open_callback(void *arg);
static void local_sync_callback(void *arg);
static ssize_t local_writev_callback(void *arg, const struct iovec *iov, int n);
static void local_close_callback(void *arg);
void _girt_appender_free(girt_local *local);

girt_local *
_girt_appender_init(UNUSED girt_appender *appender, girt_cfg *appender_config, log4c_appender_t *log4c_appender)
{
	girt_enter();
	girt_local *local = girt_malloc(sizeof(*local));
	defer { girt_if_fail { _girt_appender_free(local); } }

	local->fh = -1;

	// @config <appender:async> directory string "/var/log/girt"
	// @config-desc The log directory
	char *dir = girt_config_get_string(appender_config, "directory", "/var/log/" GIRT_NAME);
	defer { free(dir); }
	char *dirdot = girt_asprintf("%s/.", dir);
	defer { free(dirdot); }
	if (access(dirdot, R_OK|W_OK|X_OK) != 0) {
		girt_printf(ERROR, "Cannot access directory '%s' for read/write/search: %s\n", dir, strerror(errno));
		girt_fail(NULL);
	}

	const char *name = log4c_appender_get_name(log4c_appender);
	// @config <appender:async> basename string The appender's name
	// @config-desc The basename of the log file. The full name will be "<directory>/<basename>.log"
	char *basename = girt_config_get_string(appender_config, "basename", name);
	defer { free(basename); }
	local->filename = girt_asprintf("%s/%s.log", dir, basename);

	local->logger = girt_check_oom_ptr(logger_new_v(local_open_callback, local_close_callback, local_writev_callback, NULL));

	logger_set_sync(local->logger, local_sync_callback);

	char tname[16];
	snprintf(tname, sizeof(tname), "logger-%s", name);
	logger_set_name(local->logger, tname);

	// @config <appender:async> queue-size i64 0x40000
	// @config-desc The maximum number of log lines that can be queued before liblogger starts discarding.
	if (logger_start_with_queue_size(local->logger, girt_config_get_i64(appender_config, "queue-size",  0x40000))) { // 256k
		girt_printf(ERROR, "Failed to start logger thread '%s'", tname);
		_girt_appender_free(local);
		girt_fail(NULL);
	}

	log4c_appender_set_udata(log4c_appender, local);

	girt_return(local);
}

void
_girt_appender_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	if (local->logger != NULL) {
		int ret = logger_stop(local->logger);
		if (ret != 0) {
			girt_printf(ERROR, "Asynchronous logger returned %d\n", ret);
		}

		logger_free(local->logger);
	}

	logbuf_free_free_list();

	free(local->filename);

	memset(local, 0, sizeof(*local));

	local->fh = -1;

	free(local);
}

int
_girt_appender_open(log4c_appender_t *appender)
{
	girt_local *local = log4c_appender_get_udata(appender);

	return logger_open(local->logger, local);
}

int
_girt_appender_append(log4c_appender_t *appender, const log4c_logging_event_t *event)
{
	girt_local *local = log4c_appender_get_udata(appender);

	return logger_print(local->logger, local, 0, event->evt_rendered_msg);
}

int
_girt_appender_close(log4c_appender_t *appender)
{
	girt_local *local = log4c_appender_get_udata(appender);

	return logger_close(local->logger, local);
}

static int
local_open_callback(void *arg)
{
	girt_local *local = arg;

	if (local->fh >= 0) {
		return 0;
	}

	local->fh = open(local->filename, O_WRONLY|O_APPEND|O_CREAT, 0600);

	if (local->fh < 0) {
		girt_printf(ERROR, "Failed to open %s for writing: %s\n", local->filename, strerror(errno));
		return -1;
	}

	return 0;
}

static void
local_sync_callback(void *arg)
{
	girt_local *local = arg;

	if (local->fh >= 0) {
		fdatasync(local->fh);
	}
}

static ssize_t
local_writev_callback(void *arg, const struct iovec *iov, int n)
{
	girt_local *local = arg;

	return writev(local->fh >= 0 ? local->fh : 2, iov, n);
}

static void
local_close_callback(void *arg)
{
	girt_local *local = arg;

	close(local->fh);
	local->fh = -1;
}


#include <girt/girt.h>

typedef struct {
	int explicit;
} girt_local;

#include <girt/http_parser.h>

static ssize_t http_parse_request(char *buf, size_t buflen, girt_flow *flow, int explicit);
static void http_dest_connect_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg);

girt_local *
_girt_prereader_init(UNUSED girt_prereader *prereader, UNUSED girt_cfg *prereader_config)
{
	girt_local *local = girt_malloc(sizeof(*local));

	// @config <prereader:http> explicit boolean false
	// @config-desc Should the HTTP prereader handle explicit proxying. ie. Skipping CONNECT request headers and extracting hostnames from URLs.
	local->explicit = girt_config_get_boolean(prereader_config, "explicit", 0);

	return local;
}

void
_girt_prereader_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	memset(local, 0, sizeof(*local));

	free(local);
}

ssize_t
_girt_prereader_peek(UNUSED girt_local *local, void *buf, size_t len, girt_flow *flow)
{
	ssize_t used = http_parse_request(buf, len, flow, local->explicit);

	if (used <= 0) {
		return used;
	}

	int major_version = girt_flow_attrs(get_u64_at, flow, "orig.http.version", 0);
	int minor_version = girt_flow_attrs(get_u64_at, flow, "orig.http.version", 1);

	girt_flow_attrs(set_tag_int, flow, "http", (major_version << 8) | minor_version);

	if (!local->explicit) {
		// Not a proxy we are just checking if this is http
		return GIRT_PREREADER_DONE;
	}

	kstring_t msg[1] = {{0}};
	int method;
	ksprintf(msg, "PROXY method=%s", girt_flow_attrs(get_str_int, flow, "orig.http.method", &method));

	char *scheme = girt_flow_attrs(get_str, flow, "orig.http.url.scheme");
	if (scheme != NULL) {
		ksprintf(msg, " scheme=%s", scheme);
	}
	ksprintf(msg, " host=%s", girt_flow_attrs(get_str, flow, "orig.http.url.host"));
	int port = girt_flow_attrs(get_int, flow, "orig.http.url.port");
	if (port > 0) {
		ksprintf(msg, " port=%d", port);
	}

	ksprintf(msg, " v=%d.%d", major_version, minor_version);

	girt_flow_print(INFO, flow, ks_str(msg));
	free(ks_release(msg));

	if (port <= 0) {
		girt_flow_print(ERROR, flow, "Not a proxy request");
		return GIRT_PREREADER_ERROR;
	}

	// Set up for proxy reply
	girt_flow_attrs_notify_enable(flow, "dest.connect.status", GIRT_ATTR_PTR, http_dest_connect_callback);

	// Skip request if it is a connect
	return (method == HTTP_CONNECT ? used : GIRT_PREREADER_DONE);
}

static void
http_connect_reply(girt_flow *flow, int status, const char *message)
{
	kstring_t resp[1] = {{0}};

	int resplen = ksprintf(resp, "HTTP/1.0 %d %s\r\n"
						"Proxy-agent: " GIRT_NAME "/" GIRT_VERSION "\r\n"
						"Connection: close\r\n"
						"Proxy-Connection: close\r\n"
						"\r\n", status, message);
	if (resplen > 0) {
		send(girt_flow_attrs(get_int, flow, "orig.addr.name"), ks_str(resp), resplen, 0);
	}

	free(ks_release(resp));
}

static void
http_dest_connect_callback(UNUSED evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_flow *flow = arg;

	int status = 0;
	char *msg = girt_flow_attrs(get_str_int, flow, "dest.connect.status", &status);

	if (status > 0) {
		if (girt_flow_attrs(get_int, flow, "orig.http.method") == HTTP_CONNECT) {
			http_connect_reply((girt_flow *)arg, 200, msg == NULL ? "Connection established" : msg);
		}
	} else
	if (status < 0) {
		http_connect_reply((girt_flow *)arg, 500, msg == NULL ? "Connection failed" : msg);
	} else {
		// re-arm
		girt_flow_attrs_notify_enable(flow, "dest.connect.status", GIRT_ATTR_PTR, http_dest_connect_callback);
	}
}

static void
http_handle_completed_header(char *name, char *value, girt_flow *flow)
{
	kstring_t full_name[1] = {{0}};
	ksprintf(full_name, "orig.http.headers.%s", girt_strlower(name));
	girt_flow_attrs(push_str, flow, ks_str(full_name), value);
	free(ks_release(full_name));

	if (!strcmp(name, "host")) {
		char *colon = strchr(value, ':');
		if (colon != NULL) {
			girt_flow_attrs(put_str, flow, "orig.http.headers.host.host", girt_strndup(value, colon-value));
			char *end = NULL;
			int port = strtol(colon+1, &end, 10);
			girt_flow_attrs(put_str_int, flow, "orig.http.headers.host.port", girt_strdup(colon+1), port);
		} else {
			girt_flow_attrs(put_lit, flow, "orig.http.headers.host.host", value);
		}
	}
}

static void
http_parse_url(char *buf, size_t buflen, int is_connect, girt_flow *flow, char *prefix)
{
	struct http_parser_url url_fields[1] = {{0}};

	if (http_parser_parse_url(buf, buflen, is_connect, url_fields) == 0) {

		kstring_t key[1] = {{0}};
		uint32_t port = 0;

		if ((url_fields->field_set & (1<<UF_SCHEMA)) != 0) {
			char *scheme = &buf[url_fields->field_data[UF_SCHEMA].off];
			int scheme_len = url_fields->field_data[UF_SCHEMA].len;
			key->l = 0; ksprintf(key, "%s.host", prefix);
			girt_flow_attrs(put_str, flow, ks_str(key), girt_strndup(scheme, scheme_len));

			if ((scheme_len == 4 || scheme_len == 5) && !strncmp(scheme, "http", 4)) {
				port = (scheme_len == 5 && scheme[4] == 's') ? 443 : 80;
			}
		}

		if ((url_fields->field_set & (1<<UF_HOST)) != 0 && url_fields->field_data[UF_HOST].len > 0) {
			key->l = 0; ksprintf(key, "%s.host", prefix);
			girt_flow_attrs(put_str, flow, ks_str(key),
				girt_strndup(&buf[url_fields->field_data[UF_HOST].off], url_fields->field_data[UF_HOST].len)
			);
	
			if ((url_fields->field_set & (1<<UF_PORT)) != 0 && url_fields->field_data[UF_PORT].len > 0) {
				port = url_fields->port;
			}
		}

		if (port > 0) {
			key->l = 0;	ksprintf(key, "%s.port", prefix);
			girt_flow_attrs(put_str_int, flow, ks_str(key), girt_asprintf("%u", port), port);
		}

		if ((url_fields->field_set & (1<<UF_PATH)) != 0 && url_fields->field_data[UF_PATH].len > 0) {
			size_t len = url_fields->field_data[UF_PATH].len +
				((url_fields->field_set & (1<<UF_QUERY)) != 0 ? url_fields->field_data[UF_QUERY].len : 0) + 
				((url_fields->field_set & (1<<UF_FRAGMENT)) != 0 ? url_fields->field_data[UF_FRAGMENT].len : 0);
			key->l = 0;	ksprintf(key, "%s.path", prefix);
			girt_flow_attrs(put_str, flow, ks_str(key),
				girt_strndup(&buf[url_fields->field_data[UF_PATH].off], len)
			);
		}

		free(ks_release(key));
	}
}

typedef struct {
	girt_flow *flow;
	char *scheme;
	int method;
	int start_line_checked;
} http_parser_context;

static void
on_start_line_complete(http_parser* parser)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	girt_flow *flow = context->flow;

	context->start_line_checked = 1;

	girt_flow_attrs(put_lit_int, flow, "orig.http.method", http_method_str(parser->method), parser->method);

	kstring_t *curr_url = girt_flow_attrs(get_kstr, flow, "_curr_url");
	if (curr_url && ks_len(curr_url) > 0) {

		kstring_t host[1] = {{0}};
		uint32_t port = 0;
		http_parse_url(ks_str(curr_url), ks_len(curr_url), parser->method == HTTP_CONNECT, flow, "orig.http.url");
		if (ks_len(host) > 0) {
			girt_flow_attrs(put_str, flow, "orig.http.url.host", ks_release(host));
		} else {
			free(ks_release(host));
		}
		if (port > 0) {
			girt_flow_attrs(put_u64, flow, "orig.http.url.port", port);
		}

		girt_flow_attrs(put_str, flow, "orig.http.url", ks_release(curr_url));
		free(ks_release(curr_url));
	}

	girt_flow_attrs(push_u64, flow, "orig.http.version", parser->http_major);
	girt_flow_attrs(push_u64, flow, "orig.http.version", parser->http_minor);
}

/*
 * Call-back for HTTP parser when request URL is received.
 *
 * It is possible to receive the URL in chunks because the parser
 * doesn't buffer.
 */
static int
on_proxy_url(http_parser* parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	girt_flow *flow = context->flow;

	kstring_t *curr_url = girt_flow_attrs(get_kstr, flow, "_curr_url");
	if (curr_url == NULL) {
		curr_url = girt_flow_attrs(put_kstr, flow, "_curr_url", NULL);
	}

	// append to the current url
	kputsn((char *)at, len, curr_url);

	return 0;
}

/*
 * Call-back for HTTP parser when header parsing is complete
 */
static int
on_proxy_headers_complete(http_parser* parser)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	girt_flow *flow = context->flow;

	if (!context->start_line_checked) {
		on_start_line_complete(parser);
	}

	kstring_t *curr_name = girt_flow_attrs(get_kstr, flow, "_curr_name");
	kstring_t *curr_value = girt_flow_attrs(get_kstr, flow, "_curr_value");

	if (curr_name != NULL && ks_len(curr_name) && curr_value != NULL && ks_str(curr_value)) {
		// previous header completed;
		http_handle_completed_header(ks_str(curr_name), ks_release(curr_value), flow);
		curr_name->l = 0;
	}

	free(ks_release(curr_name));
	free(ks_release(curr_value));

	return 1; // Always skip the body (if any)
}


/**
 * Call-back for HTTP parser when a header field (name) is encountered. The name ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int
on_proxy_header_field(http_parser *parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	girt_flow *flow = context->flow;

	if (!context->start_line_checked) {
		on_start_line_complete(parser);
	}

	kstring_t *curr_name = girt_flow_attrs(get_kstr, flow, "_curr_name");
	if (curr_name == NULL) {
		curr_name = girt_flow_attrs(put_kstr, flow, "_curr_name", NULL);
	}

	kstring_t *curr_value = girt_flow_attrs(get_kstr, flow, "_curr_value");
	if (curr_value != NULL && ks_str(curr_value) != NULL) {
		// previous header completed
		if (curr_name != NULL && ks_len(curr_name)) {
			http_handle_completed_header(ks_str(curr_name), ks_release(curr_value), flow);
			curr_name->l = 0;
		} else {
			// No name
			if (curr_name != NULL) {
				free(ks_release(curr_name));
			}
			free(ks_release(curr_value));
			return 1;
		}
	}

	// append to the current header field
	kputsn((char *)at, len, curr_name);

	return 0;
}

/**
 * Call-back for HTTP parser when a header value is encountered. The value ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int
on_proxy_header_value (http_parser *parser, const char *at, size_t len)
{
	http_parser_context *context = (http_parser_context *)parser->data;
	girt_flow *flow = context->flow;

	kstring_t *curr_name = girt_flow_attrs(get_kstr, flow, "_curr_name");
	if (curr_name == NULL || ks_len(curr_name) == 0) {
		// no header field, fail
		return 1;
	}

	// create or append to the current header value
	kstring_t *curr_value = girt_flow_attrs(get_kstr, flow, "_curr_value");
	if (curr_value == NULL) {
		curr_value = girt_flow_attrs(put_kstr, flow, "_curr_value", NULL);
	}
	kputsn((char *)at, len, curr_value);

	return 0;
}

static http_parser_settings proxy_parser_settings[1] = {{
	.on_url = on_proxy_url,
	.on_header_field = on_proxy_header_field,
	.on_header_value = on_proxy_header_value,
	.on_headers_complete = on_proxy_headers_complete,
}};

static ssize_t
http_parse_request(char *buf, size_t buflen, girt_flow *flow, int explicit)
{
	// See if there is an empty line for end-of-headers
	int *prep = NULL;
	char *eoh = kstrnstr(buf, "\r\n\r\n", buflen, &prep);
	free(prep); prep = NULL;
	if (eoh != NULL) {
		eoh += 4;
	} else {
		eoh = kstrnstr(buf, "\n\n", buflen, &prep);
		free(prep); prep = NULL;
		if (eoh != NULL) {
			eoh += 2;
		}
	}
	// Found it, only try parsing up to there
	if (eoh != NULL) {
		buflen = (size_t)(eoh-buf);
	}

	size_t used = 0;

	http_parser_context context = { .flow = flow };
	http_parser parser = { .data = &context };
	http_parser_init(&parser, HTTP_REQUEST);

	used = http_parser_execute(&parser, proxy_parser_settings, buf, buflen);

	if (HTTP_PARSER_ERRNO(&parser) != HPE_OK) {
		// Definitely not http
		if (explicit) {
			girt_flow_printf(DEBUG, flow, "http: (%d) %s\n", HTTP_PARSER_ERRNO(&parser), http_errno_name(HTTP_PARSER_ERRNO(&parser)));
		}
		return GIRT_PREREADER_DONE;
	}

	// Possibly http

	if (used == buflen) {
		// Parsed everything
		if (eoh == NULL) {
			// But no eoh, ask for more
			return GIRT_PREREADER_AGAIN;
		} else {
			// All done, report the used size
			return (ssize_t)used;
		}
	}

	// Couldn't parse all of it, almost certainly not http, just stop

	return GIRT_PREREADER_DONE;
}


#include <girt/girt.h>

typedef struct girt_local girt_local;

girt_local *
_girt_prereader_init(UNUSED girt_prereader *prereader, UNUSED girt_cfg *prereader_config)
{
	return girt_malloc(1);
}

void
_girt_prereader_free(UNUSED girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_free(local);
}

ssize_t
_girt_prereader_peek(UNUSED girt_local *local, void *buf, size_t len, girt_flow *flow)
{
	char *sni = NULL;

	int version = girt_ssl_check((unsigned char*)buf, len, &sni);

	if (version < 0) {
		return GIRT_PREREADER_AGAIN;
	}

	if (version > 0) {
		if (sni != NULL) {
			girt_flow_attrs(put_str, flow, "orig.sni", sni);
		}

		girt_flow_attrs(push_u64, flow, "orig.ssl.version", version >> 8);
		girt_flow_attrs(push_u64, flow, "orig.ssl.version", version & 0xff);

		girt_flow_attrs(set_tag_int, flow, "ssl", version);

		girt_flow_printf(INFO, flow, "SSL sni=%s v=%d.%d", sni, version >> 8, version & 0xff);
	}

	return GIRT_PREREADER_DONE;
}


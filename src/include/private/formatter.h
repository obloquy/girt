struct girt_formatter {
	log4c_layout_t *layout;
	log4c_layout_type_t *layout_type;

	girt_plugin *plugin;
	girt_plugin_context *context;

	girt_log *logger;
};


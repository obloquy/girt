struct girt_prereaders {
	kvec_t(girt_prereader *) prereaders;
	pthread_key_t bufkey;
	ssize_t buflen;
};


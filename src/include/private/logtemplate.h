typedef enum {
	TOKEN_UNKNOWN,
	TOKEN_EXTERNAL,
	TOKEN_LITERAL,
	TOKEN_MESSAGE,
	TOKEN_LOCALTIME,
	TOKEN_MSEC,
	TOKEN_PRIORITY_NAME,
	TOKEN_FILE,
	TOKEN_LINE,
	TOKEN_FUNCTION,
	TOKEN_THREAD_NAME,
	TOKEN_THREAD_ID,
	TOKEN_LOCATION_NAME,
} template_token_type;

typedef struct {
	template_token_type type;
	int external_type;
	char *format;
} template_token;

typedef kvec_t(template_token *) template_tokens;

struct girt_logtemplate {
	template_tokens *tokens;
	int (*tokenize_external)(char *name);
	size_t (*render_external)(char *buf, size_t buflen, int token_type, char *format, const log4c_logging_event_t *event);
};


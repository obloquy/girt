struct girt_proxy {
	char *name;
	int verbose;
	girt_listener *listener;

	girt_plugin *plugin;
	girt_plugin_context *context;

	girt_root *root;
};


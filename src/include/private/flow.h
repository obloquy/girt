#define girt_pointer_hash_func(key) (khint32_t)(((intptr_t)key)>>33^((intptr_t)key)^((intptr_t)key)<<11)
#define girt_pointer_hash_equal(a, b) ((a) == (b))

KHASH_INIT(events, struct girt_flow_event *, char, 0, girt_pointer_hash_func, girt_pointer_hash_equal)

struct girt_flow {
	ulid_t				id;
	char				idstr[ULID_STRINGZ_LENGTH];

	girt_worker			*worker;
	girt_attrs			*attrs;

	khash_t(events)		*events;

	unsigned int		start_logged:1;
	unsigned int		dest_logged:1;
};

struct girt_flow_event {
	girt_flow			*flow;
	event_callback_fn	callback;
	struct event		*ev;
	unsigned int		persist:1;
	unsigned int		reuse:1;
};


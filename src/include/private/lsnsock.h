struct girt_lsnsock {
	char *familyname;
	int family;
	char *typename;
	int type;
	char *protoname;
	int proto;
	char *addr;
	char *portname;
	char *addrname;
	struct addrinfo *addrinfo;
	evutil_socket_t fd;
	int transparent;
};


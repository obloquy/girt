
#include <libnet.h>

typedef union {
	struct sockaddr sa;
	struct sockaddr_in in;
	struct sockaddr_in6 in6;
} girt_sockaddr;

struct girt_message {

	girt_message_method method;

	union {

		struct {
			libnet_t *net;
			girt_sockaddr spoof;
			libnet_ptag_t ip_tag;
			libnet_ptag_t udp_tag;
		} l;

		struct {
			struct msghdr msg;
			struct cmsghdr *cmsg_last;
			size_t cmsg_space;
			char control[64];
		} m;
	} u;

	char *errmsg;
	girt_sockaddr addr;
	struct iovec iov;
};


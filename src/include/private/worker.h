struct girt_worker {
	int idx;
	int pthread_result;
	pthread_t tid;
	char tname[16];
	struct event_base *evbase;
	struct evdns_base *evdns;
	struct event *self_destruct_ev;
};


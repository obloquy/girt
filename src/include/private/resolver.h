struct girt_resolver {
	char *attr_in;
	char *attr_out;
	char *attr_status;

	girt_plugin *plugin;
	girt_plugin_context *context;

	girt_proxy *proxy;
};

struct girt_resolverarg {
	girt_resolver *resolver;
	girt_plugin_context *context;
	girt_flow *flow;
};


typedef struct {
	void				*ptr;
	size_t				len; // or zero
	int					type;
	girt_attr_free_fn	free;
	girt_attr_close_fn	close; // If this is set it will be applied to girt_attr.i
} girt_attr_ptr;

typedef struct {
	uint32_t			flags;
	event_callback_fn	callback;
	void				*arg;
	girt_location		loc;
} girt_attr_notify;

typedef kvec_t(girt_attr_notify) girt_attr_notify_vec;

typedef struct girt_attr_value {
	union {
		girt_attr_ptr		ptr;
		uint64_t			u64;
		int64_t				i64;
		kstring_t			kstr[1];
	} u;
	int						i;
} girt_attr_value;

typedef kvec_t(girt_attr_value) girt_attr_value_vec;

typedef struct {
	girt_attr_type			type;
	girt_attr_value_vec		vec;
	girt_attr_notify_vec	notify;
	char					name[1];
} girt_attr;

KHASH_MAP_INIT_STR(attrs, girt_attr *)
KHASH_MAP_INIT_STR(notify, girt_attr_notify_vec)

struct girt_attrs {
	kvec_t(girt_attr *) vec;
	khash_t(attrs) 		*hash;
	khash_t(notify) 	*notify;
	girt_attrs_notifier_fn notifier;
	char				keybuf[64];
};


#ifndef _PRIVATE_APPENDER_H
#define _PRIVATE_APPENDER_H

struct girt_appender {
	log4c_appender_t *appender;
	log4c_appender_type_t *appender_type;

	girt_plugin *plugin;
	girt_plugin_context *context;

	girt_log *logger;
};

#endif // _PRIVATE_APPENDER_H


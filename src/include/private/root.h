struct girt_root {
	int argc;
	char **argv;
	girt_str_vec *args;
	struct event_base *evbase;
	girt_cfg *config;
	girt_plg *plugins;
	girt_log *logger;
	girt_sig *signals;
	girt_wrk *workers;
	girt_pxy *proxies;
	intptr_t exit_code; // intptr_t so it can be cast to (void*) for pthread result
};


struct girt_listener {
	int pthread_result;
	pthread_t tid;
	char tname[16];
	int verbose;
	struct event_base *evbase;
	girt_lsnsock *socket;

	girt_plugin *plugin;
	girt_plugin_context *context;

	girt_proxy *proxy;
};


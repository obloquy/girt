#ifndef WITHOUT_LOG4C

struct girt_log {
	girt_formatters *formatters;
	girt_appenders *appenders;
	girt_root *root;
};

#endif

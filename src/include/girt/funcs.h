#ifndef _GIRT_FUNCS_H
#define _GIRT_FUNCS_H

typedef kvec_t(girt_cfg *) girt_cfg_vec;

#if __STDC_ISO_10646__ >= 201706L
#define __FUNCTION__ ""
#define __STATIC_ASSERT_SEMICOLON__ ;
#else
#define __STATIC_ASSERT_SEMICOLON__
#endif

uint32_t crc32(uint32_t crc, const void *buf, size_t size);

girt_appender *girt_appender_init(girt_log *log, girt_cfg *appender_config, char *name);
void girt_appender_free(girt_appender *appender);

girt_appenders *girt_appenders_init(girt_log *log, girt_cfg *logger_config);
void girt_appenders_free(girt_appenders *appenders);

girt_attrs *girt_attrs_init();
void girt_attrs_free(girt_attrs *attrs);
int girt_attrs_exists(girt_attrs *attrs, char *name);
void *girt_attrs_get_or_put_ptr(girt_attrs *attrs, char *name, size_t len, girt_attr_free_fn free);
void *girt_attrs_push_ptr_len_type_int(girt_attrs *attrs, char *name, void *ptr, size_t len, int type, int i, girt_attr_free_fn free, girt_attr_close_fn close);
void *girt_attrs_put_ptr_len_type_int(girt_attrs *attrs, char *name, void *ptr, size_t len, int type, int i, girt_attr_free_fn free, girt_attr_close_fn close);
#define girt_attrs_push_ptr_len(a, n, p, l, f) girt_attrs_push_ptr_len_type_int(a, n, p, l, 0, 0, f, NULL)
#define girt_attrs_push_ptr(a, n, p, f) girt_attrs_push_ptr_len_type_int(a, n, p, 0, 0, 0, f, NULL)
#define girt_attrs_put_ptr(a, n, p, f) girt_attrs_put_ptr_len_type_int(a, n, p, 0, 0, 0, f, NULL)
#define girt_attrs_put_ptr_len(a, n, p, l, f) girt_attrs_put_ptr_len_type_int(a, n, p, l, 0, 0, f, NULL)
#define girt_attrs_put_ptr_len_type(a, n, p, l, t, f) girt_attrs_put_ptr_len_type_int(a, n, p, l, t, 0, f, NULL)
#define girt_attrs_put_ptr_int(a, n, p, i, f) girt_attrs_put_ptr_len_type_int(a, n, p, 0, 0, i, f, NULL)
#define girt_attrs_put_ptr_type_int(a, n, p, t, i, f) girt_attrs_put_ptr_len_type_int(a, n, p, 0, t, i, f, NULL)
#define girt_attrs_push_str(a, n, s) (char *)girt_attrs_push_ptr_len_type_int(a, n, s, 0, GIRT_ATTR_PTR_STR, 0, girt_free_pp, NULL)
#define girt_attrs_put_str(a, n, s) (char *)girt_attrs_put_ptr_len_type_int(a, n, s, 0, GIRT_ATTR_PTR_STR, 0, girt_free_pp, NULL)
#define girt_attrs_put_str_int(a, n, s, i) (char *)girt_attrs_put_ptr_len_type_int(a, n, s, 0, GIRT_ATTR_PTR_STR, i, girt_free_pp, NULL)
#define girt_attrs_put_str_alias(a, n, s) (char *)girt_attrs_put_ptr_len_type_int(a, n, s, 0, GIRT_ATTR_PTR_ALIAS, 0, girt_free_pp, NULL)
#define girt_attrs_push_lit(a, n, s) (char *)girt_attrs_push_ptr_len_type_int(a, n, (char *)s, 0, GIRT_ATTR_PTR_STR, 0, NULL, NULL)
#define girt_attrs_put_lit(a, n, s) (char *)girt_attrs_put_ptr_len_type_int(a, n, (char *)s, 0, GIRT_ATTR_PTR_STR, 0, NULL, NULL)
#define girt_attrs_put_lit_int(a, n, s, i) (char *)girt_attrs_put_ptr_len_type_int(a, n, (char *)s, 0, GIRT_ATTR_PTR_STR, i, NULL, NULL)
#define girt_attrs_put_lit_alias(a, n, s) (char *)girt_attrs_put_ptr_len_type_int(a, n, (char *)s, 0, GIRT_ATTR_PTR_ALIAS, 0, NULL, NULL)
void *girt_attrs_get_ptr_len_int_at(girt_attrs *attrs, char *name, size_t *lptr, int *iptr, size_t idx);
#define girt_attrs_get_ptr_int(a, n, i) girt_attrs_get_ptr_len_int_at(a, n, NULL, i, 0)
#define girt_attrs_get_ptr_len(a, n, l) girt_attrs_get_ptr_len_int_at(a, n, l, NULL, 0)
#define girt_attrs_get_ptr(a, n) girt_attrs_get_ptr_len_int_at(a, n, NULL, NULL, 0)
char *girt_attrs_get_str_int_at(girt_attrs *attrs, char *name, int *iptr, size_t idx);
#define girt_attrs_get_str_int(a, n, i) girt_attrs_get_str_int_at(a, n, i, 0)
#define girt_attrs_get_str(a, n) girt_attrs_get_str_int_at(a, n, NULL, 0)
kstring_t *girt_attrs_put_kstr(girt_attrs *attrs, char *name, char *str);
kstring_t *girt_attrs_get_kstr(girt_attrs *attrs, char *name);
int girt_attrs_set_tag_int(girt_attrs *attrs, char *name, int m);
#define girt_attrs_set_tag(a, n) girt_attrs_set_tag_int(a, n, 1)
#define girt_attrs_unset_tag(a, n) girt_attrs_set_tag_int(a, n, 0)
int girt_attrs_get_tag(girt_attrs *attrs, char *name);
__attribute__ ((sentinel))
int girt_attrs_has_any_tag_v(girt_attrs *attrs, ...);
__attribute__ ((sentinel))
int girt_attrs_has_all_tags_v(girt_attrs *attrs, ...);
#define girt_attrs_has_tag girt_attrs_get_tag
#define girt_attrs_has_any_tag(a, n, ...) girt_attrs_has_any_tag_v(a, n, __VA_ARGS__, NULL)
#define girt_attrs_has_all_tags(a, n, ...) girt_attrs_has_all_tags_v(a, n, __VA_ARGS__, NULL)
uint64_t girt_attrs_push_u64(girt_attrs *attrs, char *name, uint64_t u64);
uint64_t girt_attrs_put_u64(girt_attrs *attrs, char *name, uint64_t val);
uint64_t girt_attrs_add_u64(girt_attrs *attrs, char *name, uint64_t val);
uint64_t girt_attrs_get_u64_at(girt_attrs *attrs, char *name, size_t idx);
#define girt_attrs_get_u64(a, n) girt_attrs_get_u64_at(a, n, 0)
uint64_t girt_attrs_push_u64(girt_attrs *attrs, char *name, uint64_t u64);
int64_t girt_attrs_put_i64(girt_attrs *attrs, char *name, int64_t val);
int64_t girt_attrs_add_i64(girt_attrs *attrs, char *name, int64_t val);
int64_t girt_attrs_get_i64_at(girt_attrs *attrs, char *name, size_t idx);
#define girt_attrs_get_i64(a, n) girt_attrs_get_i64_at(a, n, 0)
int girt_attrs_set_int_at(girt_attrs *attrs, char *name, int i, size_t idx);
#define girt_attrs_set_int(a, n, i) girt_attrs_set_int_at(a, n, i, 0)
int girt_attrs_get_int(girt_attrs *attrs, char *name);
char *girt_attrs_get_name_ref(girt_attrs *attrs, char *name);
int girt_attrs_delete(girt_attrs *attrs, char *name);
void girt_attrs_set_or_init_notify_loc(girt_attrs *attrs, char *name, girt_attr_type type, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc);
#define girt_attrs_set_or_init_notify(attrs, n, t, c, a, f) girt_attrs_set_or_init_notify_loc(attrs, n, t, c, a, f, GIRT_LOCATION(c))
void girt_attrs_set_notify_loc(girt_attrs *attrs, char *name, event_callback_fn callback, void *arg, uint32_t flags, girt_location *loc);
#define girt_attrs_set_notify(attrs, n, c, a, f) girt_attrs_set_notify_loc(attrs, n, c, a, f, GIRT_LOCATION(c))
#define girt_attrs_disable_notify(a, n, c) girt_attrs_set_notify(a, n, c, NULL, 0)
void girt_attrs_notify(girt_attrs *attrs, char *name);
void girt_attrs_dump(girt_attrs *attrs, girt_str_vec *vec);
void girt_attrs_dump_fp(girt_attrs *attrs, FILE *fp);
__attribute__ ((sentinel))
char *girt_attrs_mkkey_v(girt_attrs *attrs, ...);
#define girt_attrs_mkkey(a, ...) girt_attrs_mkkey_v(a, __VA_ARGS__, NULL) 

girt_cfg *girt_config_init(girt_root *root);
void girt_config_free(girt_cfg *config);
char *girt_config_procname(girt_cfg *config);
char *girt_config_home(girt_cfg *config);
char *girt_config_get_string(girt_cfg *config, char *key, const char *def);
int girt_config_get_boolean(girt_cfg *config, char *key, int def);
int64_t girt_config_get_i64(girt_cfg *config, char *key, int64_t def);
girt_int_vec *girt_config_get_i64_array(girt_cfg *config, char *key);
void girt_config_free_i64_array(girt_int_vec *);
girt_str_vec *girt_config_get_string_array(girt_cfg *config, char *key);
void girt_config_free_string_array(girt_str_vec *array);
uint64_t girt_config_get_millis(girt_cfg *config, char *key, char *def);
struct addrinfo *girt_config_get_addrinfo(
	girt_cfg *config, char *name, int passive, int def_family, char *def_addr, char *def_port);
void girt_config_free_addrinfo(struct addrinfo *ai);
struct sockaddr *girt_config_get_sockaddr(
	girt_cfg *config, char *name, int passive, int def_family, char *def_addr, char *def_port, socklen_t *addrlen);
void girt_config_free_sockaddr(struct sockaddr *sa);
struct timeval *girt_config_get_timeval(girt_cfg *config, char *key, char *def);
void girt_config_free_timeval(struct timeval *tv);
pthread_attr_t *girt_config_get_pthread_attr(girt_cfg *config, char *name);
void girt_config_free_pthread_attr(pthread_attr_t *attr);
girt_cfg *girt_config_get_table(girt_cfg *config, char *key);
void girt_config_free_table(girt_cfg *config);
girt_cfg_vec *girt_config_get_subtables(girt_cfg *config, char *key);
void girt_config_free_subtables(girt_cfg_vec *array);
girt_cfg_vec *girt_config_get_table_array(girt_cfg *config, char *key);
#define girt_config_free_table_array girt_config_free_subtables
char *girt_config_get_table_name(girt_cfg *config);
int girt_config_root_table_count(girt_cfg *config);
int girt_config_root_table_nkval(girt_cfg *config);
int girt_config_root_table_narr(girt_cfg *config);
int girt_config_root_table_ntab(girt_cfg *config);

girt_datagram *girt_datagram_init(void *data, size_t len, girt_flow *flow);
void girt_datagram_free(girt_datagram *datagram);

girt_filter *girt_filter_init(girt_proxy *proxy, girt_cfg *filter_config, char *type);
void girt_filter_free(girt_filter *filter);
girt_link *girt_filter_flow_start(girt_filter *filter, girt_link *next_hop);
girt_root *girt_filter_root(girt_filter *filter);

girt_filters *girt_filters_init(girt_proxy *proxy, girt_cfg *filters_config);
void girt_filters_free(girt_filters *filters);
girt_link *girt_filters_flow_start(girt_filters *filters, girt_link *last_hop);

girt_flow *girt_flow_init(evutil_socket_t fd, struct sockaddr *addr, int len, girt_lsnsock *socket, girt_worker *worker);
void girt_flow_free(girt_flow *flow);
void girt_flow_free_async(girt_flow *flow);
//void girt_flow_attr_notifier(void *arg, struct timeval *tmo, event_callback_fn callback, char *attr_name, girt_location *loc);
void girt_flow_log_start(girt_flow *flow);
void girt_flow_set_sockaddr_fd(girt_flow *flow, char *prefix, struct sockaddr *addr, socklen_t len, int fd);
#define girt_flow_set_sockaddr(f, p, s, l) girt_flow_set_sockaddr_fd(f, p, s, l, -1)
#define girt_flow_set_addrinfo(f, p, a) girt_flow_set_sockaddr(f, p, (a)->ai_addr, (a)->ai_addrlen)
girt_flow_event *girt_flow_schedule_flags(girt_flow *flow, char *name, struct timeval *tmo, event_callback_fn callback, int flags);
#define girt_flow_schedule_persist(f, n, t, c) girt_flow_schedule_flags(f, n, t, c, GIRT_FLOW_EVENT_PERSIST)
#define girt_flow_reschedule(f, n, t, c) girt_flow_schedule_flags(f, n, t, c, GIRT_FLOW_EVENT_REUSE)
#define girt_flow_schedule(f, n, t, c) girt_flow_schedule_flags(f, n, t, c, 0)
void girt_flow_cancel_event(girt_flow *flow, girt_flow_event *event);
#define girt_flow_cancel_event_by_name(f, n) girt_flow_cancel_event(f, girt_flow_attrs(get_ptr, f, n))
void girt_flow_cancel_event_by_callback(girt_flow *flow, event_callback_fn callback);
#define girt_flow_immediate(f, c) girt_flow_schedule_flags(f, NULL, &(struct timeval){ 0, 0 }, c, 0)
char *girt_flow_idstr(girt_flow *flow);
girt_attrs *girt_flow_get_attrs(girt_flow *flow);
void girt_flow_dump(girt_flow *flow);
girt_worker *girt_flow_worker(girt_flow *flow);
struct event_base *girt_flow_event_base(girt_flow *flow);
struct evdns_base *girt_flow_evdns_base(girt_flow *flow);

#define girt_flow_attrs(what, flow, ...) girt_attrs_##what(girt_flow_get_attrs(flow), __VA_ARGS__)
#define girt_flow_attrs_v(what, flow, ...) girt_attrs_##what##_v(girt_flow_get_attrs(flow), __VA_ARGS__, NULL)
#define girt_flow_attrs_notify_enable(f, n, t, c) girt_flow_attrs(set_or_init_notify, f, n, t, c, f, GIRT_ATTR_NOTIFY_ENABLED)
#define girt_flow_attrs_notify_direct(f, n, t, c) girt_flow_attrs(set_or_init_notify, f, n, t, c, f, GIRT_ATTR_NOTIFY_ENABLED|GIRT_ATTR_NOTIFY_DIRECT)
#define girt_flow_attrs_notify_persist(f, n, t, c) girt_flow_attrs(set_or_init_notify, f, n, t, c, f, GIRT_ATTR_NOTIFY_ENABLED|GIRT_ATTR_NOTIFY_PERSIST)
#define girt_flow_attrs_notify_disable(f, n, c) girt_flow_attrs(disable_notify, f, n, c)

#define GIRT_LOGGER_FLOW "flow"
extern int girt_logger_level_FLOW;
#ifndef WITHOUT_LOG4C
const char *flow_format(const log4c_layout_t *layout, const log4c_logging_event_t *event);
#endif
#define girt_flow_printf(p, f, fmt, ...) girt_logger_printf(FLOW, p, f, fmt, __VA_ARGS__)
#define girt_flow_print(p, f, m) girt_logger_print(FLOW, p, f, m)
#define girt_flow_is_priority_enabled(p) girt_logger_is_priority_enabled(FLOW, p)
girt_root *girt_logger_root(girt_log *log);
girt_str_vec *girt_root_args(girt_root *root);

girt_formatter *girt_formatter_init(girt_log *log, girt_cfg *formatter_config, char *name);
void girt_formatter_free(girt_formatter *formatter);

girt_formatters *girt_formatters_init(girt_log *log, girt_cfg *logger_config);
void girt_formatters_free(girt_formatters *formatters);

int girt_limits_set(girt_root *root);
void girt_limits_dump_pthread_attr_for_tid(girt_str_vec *vec, pthread_t tid);
int girt_limits_set_user_group(girt_root *root);
uint64_t girt_limits_get_max_backlog();
uint64_t girt_limits_get_cpu_count();
char *girt_limits_ifindex_name(int ifindex);


girt_link *girt_link_init_bev_evbuf(girt_flow *flow, char *name, struct bufferevent *bev, struct evbuffer *evbuf, void *arg, int from_orig);
#define girt_link_init(f, n, e, a, o) girt_link_init_bev_evbuf(f, n, NULL, e, a, o)
char *girt_link_name(girt_link *link);
girt_flow *girt_link_flow(girt_link *link);
struct bufferevent *girt_link_bev(girt_link *link);
struct evbuffer *girt_link_evbuf(girt_link *link);
void *girt_link_arg(girt_link *link);
int girt_link_from_orig(girt_link *link);

girt_listener *girt_listener_init(girt_proxy *proxy, girt_cfg *listener_config);
void girt_listener_free(girt_listener *listener);
girt_lsnsock *girt_listener_socket(girt_listener *listener);
struct event_base *girt_listener_event_base(girt_listener *listener);
char *girt_listener_name(girt_listener *listener);
girt_flow *girt_listener_flow_init(girt_listener *listener, int fd, struct sockaddr *addr, socklen_t len);
void girt_listener_flow_free_direct(girt_listener *listener, girt_flow *flow, int direct);
#define girt_listener_flow_free_async(l, f) girt_listener_flow_free_direct(l, f, 0)
#define girt_listener_flow_free(l, f) girt_listener_flow_free_direct(l, f, 0)
void girt_listener_flow_start(girt_listener *listener, girt_flow *flow);
void girt_listener_flow_send(girt_listener *listener, girt_flow *flow, girt_datagram *datagram);

#ifndef WITHOUT_LOG4C
girt_log *girt_logger_init(girt_root *root);
void girt_logger_free(girt_log *log);
#else
#define girt_logger_init(root) NULL
#define girt_logger_free(log)
#endif

girt_logtemplate *girt_logtemplate_init(char *template_str,
		size_t (*render)(char *buf, size_t buflen, int token_type, char *format, const log4c_logging_event_t *event),
		int (*tokenize)(char *name)
);
void girt_logtemplate_free(girt_logtemplate *template);
size_t girt_logtemplate_render_event(girt_logtemplate *template, char *buf, size_t buflen, const log4c_logging_event_t *event);

girt_lsnsock *girt_lsnsock_init(girt_listener *listener, girt_cfg *listener_config);
void girt_lsnsock_free(girt_lsnsock *socket);
char *girt_lsnsock_protoname(girt_lsnsock *s);
int girt_lsnsock_proto(girt_lsnsock *s);
char *girt_lsnsock_portname(girt_lsnsock *s);
char *girt_lsnsock_familyname(girt_lsnsock *s);
int girt_lsnsock_family(girt_lsnsock *s);
char *girt_lsnsock_typename(girt_lsnsock *s);
int girt_lsnsock_type(girt_lsnsock *s);
char *girt_lsnsock_addrname(girt_lsnsock *s);
evutil_socket_t girt_lsnsock_fd(girt_lsnsock *s);
struct sockaddr *girt_lsnsock_addr(girt_lsnsock *s);
socklen_t girt_lsnsock_addrlen(girt_lsnsock *s);
int girt_lsnsock_transparent(girt_lsnsock *s);

girt_message *girt_message_init();
girt_message *girt_message_spoof_init(girt_message_method method, 
	struct sockaddr *addr, socklen_t addrlen, struct sockaddr *spoof, socklen_t spooflen, int ifidx);
void girt_message_free_pp(void **vpp);
void girt_message_free(girt_message *message);
girt_message *girt_message_set_sockaddr(girt_message *message, struct sockaddr *addr, socklen_t addrlen);
struct sockaddr *girt_message_sockaddr(girt_message *message, socklen_t *addrlen);
struct msghdr *girt_message_msghdr(girt_message *message);
girt_message *girt_message_set_data(girt_message *message, void *data, size_t datalen);
void *girt_message_data(girt_message *message, size_t *datalen);
ssize_t girt_message_receive(girt_message *message, int fd, int flags);
ssize_t girt_message_send(girt_message *message, int fd, int flags);
char *girt_message_error(girt_message *message);
girt_message *girt_message_push_cmsg(girt_message *message, int level, int type, void *data, size_t datalen);
void girt_message_dump(girt_message *message, girt_str_vec *vec);
void girt_message_dump_fp(girt_message *message, FILE *fp);

girt_plugin *girt_plugin_init(girt_root *root, const char *name, const char *type, const char * const funcnames[], const int n);
void girt_plugin_free(girt_plugin *plugin);
girt_plugin_fns *girt_plugin_funcs(girt_plugin *plugin);
char *girt_plugin_type(girt_plugin *plugin);
char *girt_plugin_name(girt_plugin *plugin);

girt_plg *girt_plugins_init(girt_root *root);
void girt_plugins_free(girt_plg *plugins);

#if !defined(_Static_assert)
#define _Static_assert(a,b)
#endif

#define PLUGIN_FUNCS(t,a,...) \
typedef struct \
	a \
girt_plugin_##t##_fns; \
static const char * const girt_plugin_##t##_funcnames[] = { __VA_ARGS__ }; \
static const int girt_plugin_##t##_funcnames_count = sizeof(girt_plugin_##t##_funcnames)/sizeof(girt_plugin_##t##_funcnames[0]); \
_Static_assert(sizeof(girt_plugin_##t##_funcnames) == sizeof(girt_plugin_##t##_fns), "inconsistent "#t" plugin function declarations")__STATIC_ASSERT_SEMICOLON__ \
\
static girt_plugin *girt_plugins_load_##t(girt_plg *plugins, const char *name) { return girt_plugins_load(plugins, name, #t, girt_plugin_##t##_funcnames, girt_plugin_##t##_funcnames_count); } \
static girt_plugin_##t##_fns *girt_plugin_##t##_funcs(girt_plugin *plugin) { return (girt_plugin_##t##_fns *)girt_plugin_funcs(plugin); }

girt_plugin *girt_plugins_load(girt_plg *plg, const char *name, const char *type, const char * const funcnames[], const int n);

girt_prereader *girt_prereader_init(girt_proxy *proxy, girt_cfg *prereader_config, char *name);
void girt_prereader_free(girt_prereader *prereader);
ssize_t girt_prereader_peek(girt_prereader *prereader, void *buf, size_t len, girt_flow *flow);

girt_prereaders *girt_prereaders_init(girt_proxy *proxy, girt_cfg *prereaders_config);
void girt_prereaders_free(girt_prereaders *prereaders);
int girt_prereaders_peek(girt_prereaders *prereaders, char *fd_name, girt_flow *flow);

girt_proxy *girt_proxy_init(girt_root *root, girt_cfg *proxy_config, char *type);
void girt_proxy_free(girt_proxy *proxy);
char *girt_proxy_name(girt_proxy *proxy);
girt_listener *girt_proxy_listener(girt_proxy *proxy);
girt_flow *girt_proxy_flow_init(girt_proxy *proxy, int fd, struct sockaddr *addr, socklen_t len, girt_lsnsock *listen);
void girt_proxy_flow_free_direct(girt_proxy *proxy, girt_flow *flow, int direct);
#define girt_proxy_flow_free(p, f) girt_proxy_flow_free_direct(p, f, 0)
void girt_proxy_flow_start(girt_proxy *proxy, girt_flow *flow);
void girt_proxy_flow_send(girt_proxy *proxy, girt_flow *flow, girt_datagram *datagram);
girt_wrk *girt_proxy_workers(girt_proxy *proxy);
girt_root *girt_proxy_root(girt_proxy *proxy);

girt_pxy *girt_proxies_init(girt_root *root);
void girt_proxies_free(girt_pxy *proxies);
girt_root *girt_proxies_root(girt_pxy *proxies);

girt_resolver *girt_resolver_init(girt_proxy *proxy, girt_cfg *resolver_config, char *type, char *attr_in, char *attr_out, char *attr_status);
void girt_resolver_free(girt_resolver *resolver);
void girt_resolver_resolve(girt_resolver *resolver, girt_flow *flow);
void *girt_resolver_local(girt_resolver *resolver);
girt_proxy *girt_resolver_proxy(girt_resolver *resolver);
girt_resolver *girt_resolverarg_resolver(girt_resolverarg *arg);
girt_flow *girt_resolverarg_flow(girt_resolverarg *arg);
void *girt_resolverarg_local(girt_resolverarg *arg);

girt_root *girt_root_init(girt_str_vec *args);
intptr_t girt_root_free(girt_root *root);
girt_cfg *girt_root_config(girt_root *root);
girt_plg *girt_root_plugins(girt_root *root);
girt_wrk *girt_root_workers(girt_root *root);
struct event_base *girt_root_event_base(girt_root *root);
void girt_root_loop(girt_root *root);
intptr_t girt_root_exit_code(girt_root *root);

girt_sig *girt_signals_init(girt_root *root);
void girt_signals_free(girt_sig *signals);

int girt_ssl_check(unsigned char *buf, size_t len, char **sni);

#define UNUSED __attribute__ ((unused))

int girt_threadbuf_make(pthread_key_t *key);
void *girt_threadbuf_get(pthread_key_t key, size_t len);
size_t girt_threadbuf_len(pthread_key_t key);

#define girt_check_oom_ptr(ptr) __extension__({__typeof__ (ptr) _ptr = ptr; if (_ptr == NULL) girt_oom(); _ptr;})
#define girt_check_oom_result(result) __extension__({int _result = result; if (_result == -1) girt_oom(); _result;})
#define girt_asprintf(...) __extension__({char *_str; if (asprintf(&_str, __VA_ARGS__) == -1) girt_oom(); _str;})
void girt_oom();
char *girt_inet_ntop(struct sockaddr *sa, char *buf, socklen_t bufsize);
struct sockaddr *girt_sockaddr_new(int family, char *host, char *port, socklen_t *len);
char *girt_addrinfo_to_addrname(struct addrinfo *ai, char **addr, char **port);
char *girt_sockaddr_to_addrname(struct sockaddr *sa, char **addr, char **port);
uint16_t girt_sockaddr_port(struct sockaddr *sa);
int girt_socket(int family, int type, int protocol);
void *girt_malloc(size_t len);
#define girt_free free
#define girt_realloc(ptr, size) girt_check_oom_ptr(realloc(ptr, size))
void *girt_memdup(void *ptr, size_t len);
char *girt_strdup(const char *str);
char *girt_strndup(const char *str, size_t len);
char *girt_strlower(char *str);
char *girt_unquote(char *str);
uint64_t girt_current_millis();
size_t girt_strncpy(char *buf, size_t buflen, char *str);
__attribute__ ((sentinel))
char *girt_strappend_v(char *buf, size_t buflen, char *str, ...);
#define girt_strappend(b, l, ...) girt_strappend_v(b, l, __VA_ARGS__, NULL)
void girt_close_p(int *fd);
void girt_free_pp(void **ptr);
void girt_bufferevent_free_pp(void **vpp);
void girt_bufferevent_pair_free_pp(void **vpp);
void girt_evbuffer_free_pp(void **vpp);
void girt_event_free_pp(void **vpp);
void girt_evbuffer_ref_free(const void *data, size_t datalen, void *extra);
void girt_hexdump(girt_str_vec *vec, void *ptr, size_t len, char *prefix, char *xx, int dot);
girt_str_vec *girt_str_vec_init();
void girt_str_vec_free(girt_str_vec *vec);
int girt_str_vec_printf(girt_str_vec *vec, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
girt_str_vec *girt_args(int argc, char *argv[]);
void girt_millisleep(uint64_t millis);
#define girt_enter() UNUSED int _fail = 0
#define girt_fail(ret) do { _fail = 1; return ret; } while (0)
#define girt_if_fail if (_fail)
#define girt_return(ret) return ret

girt_worker *girt_worker_init(girt_root *root, girt_cfg *workers_config, pthread_attr_t *attr, int idx);
void girt_worker_free(girt_worker *worker);
int girt_worker_verbose(girt_worker *worker);
pthread_t girt_worker_tid(girt_worker *worker);
int girt_worker_idx(girt_worker *worker);
struct event_base *girt_worker_event_base(girt_worker *worker);
struct evdns_base *girt_worker_evdns_base(girt_worker *worker);

girt_wrk *girt_workers_init(girt_root *root);
void girt_workers_free(girt_wrk *wrk);
girt_worker *girt_workers_next(girt_wrk *wrk);

#endif // _GIRT_FUNCS_H

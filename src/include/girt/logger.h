
#ifndef _GIRT_LOGGER_H
#define _GIRT_LOGGER_H

//#define WITHOUT_LOG4C

#include <log4c.h>

#define GIRT_LOGGER_ROOT "root"
#define GIRT_LOGGER_GIRT "girt"

extern int girt_logger_init_done;

#ifndef WITHOUT_LOG4C

#define GIRT_LOGGER_FATAL LOG4C_PRIORITY_FATAL
#define GIRT_LOGGER_ERROR LOG4C_PRIORITY_ERROR
#define GIRT_LOGGER_WARN  LOG4C_PRIORITY_WARN
#define GIRT_LOGGER_INFO  LOG4C_PRIORITY_INFO
#define GIRT_LOGGER_DEBUG LOG4C_PRIORITY_DEBUG

#else

#include <syslog.h>
#define GIRT_LOGGER_FATAL LOG_EMERG
#define GIRT_LOGGER_ERROR LOG_ERR
#define GIRT_LOGGER_WARN  LOG_WARNING
#define GIRT_LOGGER_INFO LOG_INFO
#define GIRT_LOGGER_DEBUG LOG_DEBUG

#endif 

extern int girt_logger_level_ROOT;
extern int girt_logger_level_GIRT;

#define _girt_logger_printf(c, p, l, ...) \
	do { \
		if (girt_logger_level_##c >= GIRT_LOGGER_##p) { \
			char _buf[500]; \
			char *_msg = _buf; \
			size_t _len = snprintf(_buf, sizeof(_buf), __VA_ARGS__); \
			if (_len >= sizeof(_buf)) { \
				sprintf(_msg = alloca(1 + _len), __VA_ARGS__); \
			} \
			fprintf(stderr, "%s: " #p " (%s:%d) - %s", \
				girt_strlower((char[]) { #c }), \
				__FILE__, __LINE__, \
				_msg \
			); \
		} \
	} while(0)

#ifndef WITHOUT_LOG4C

#define girt_logger_is_priority_enabled(c, p) log4c_category_is_priority_enabled(log4c_category_get(GIRT_LOGGER_##c), GIRT_LOGGER_##p)

#define girt_logger_printf(c, p, l, ...) \
	do { \
		if (!girt_logger_init_done) { _girt_logger_printf(c, p, l, __VA_ARGS__); break; } \
		const log4c_category_t* cat = log4c_category_get(GIRT_LOGGER_##c); \
		if (log4c_category_is_priority_enabled(cat, GIRT_LOGGER_##p)) { \
			log4c_category_log_locinfo( \
				cat, \
				&(log4c_location_info_t) { .loc_file = __FILE__, .loc_line = __LINE__, .loc_function = __FUNCTION__, .loc_data = l }, \
				GIRT_LOGGER_##p, \
				__VA_ARGS__ \
			); \
		} \
	} while(0)
#else

#define girt_logger_is_priority_enabled(c, p) (girt_logger_level_##c >= GIRT_LOGGER_##p)
#define girt_logger_printf _girt_logger_printf

#endif

#define girt_is_priority_enabled(p) girt_logger_is_priority_enabled(GIRT, p)

#define girt_logger_print(c, p, l, m) girt_logger_printf(c, p, l, "%s", m) 
#define girt_print(p, m) girt_logger_print(GIRT, p, NULL, m)
#define girt_printf(p, f, ...) girt_logger_printf(GIRT, p, NULL, f, __VA_ARGS__)
#define girt_loc_print(p, l, m) girt_logger_print(GIRT, p, l, m)
#define girt_loc_printf(p, l, f, ...) girt_logger_printf(GIRT, p, l, f, __VA_ARGS__)

#endif // _GIRT_LOGGER_H


#ifndef _GIRT_OPAQUE_H
#define _GIRT_OPAQUE_H

typedef struct girt_appender girt_appender;
typedef struct girt_appenders girt_appenders;
typedef struct girt_attrs girt_attrs;
typedef struct girt_cfg girt_cfg;
typedef struct girt_filter girt_filter;
typedef struct girt_filters girt_filters;
typedef struct girt_flow_event girt_flow_event;
typedef struct girt_flow girt_flow;
typedef struct girt_flw girt_flw;
typedef struct girt_formatter girt_formatter;
typedef struct girt_formatters girt_formatters;
typedef struct girt_link girt_link;
typedef struct girt_listener girt_listener;
typedef struct girt_log girt_log;
typedef struct girt_logtemplate girt_logtemplate;
typedef struct girt_lsnsock girt_lsnsock;
typedef struct girt_message girt_message;
typedef struct girt_plg girt_plg;
typedef struct girt_plugin_context girt_plugin_context;
typedef struct girt_plugin girt_plugin;
typedef struct girt_prereader girt_prereader;
typedef struct girt_prereaders girt_prereaders;
typedef struct girt_proxy girt_proxy;
typedef struct girt_pxy girt_pxy;
typedef struct girt_resolverarg girt_resolverarg;
typedef struct girt_resolver girt_resolver;
typedef struct girt_root girt_root;
typedef struct girt_sig girt_sig;
typedef struct girt_worker girt_worker;
typedef struct girt_wrk girt_wrk;

#endif // _GIRT_OPAQUE_H


#ifndef _GIRT_TYPES_H
#define _GIRT_TYPES_H

#define GIRT_NAME "girt"
#define GIRT_VERSION "0.1"

typedef struct {
	const char *file;
	int line;
	const char *function;
} girt_location;

typedef kvec_t(int64_t) girt_int_vec;
typedef kvec_t(char *) girt_str_vec;

typedef void* (*girt_plugin_fns)();
typedef void (*girt_attr_free_fn)(void **);
typedef void (*girt_attr_close_fn)(int *);
typedef void (*girt_attrs_notifier_fn)(void *arg, struct timeval *tmo, event_callback_fn callback, char *attr_name, girt_location *loc);
typedef void (*girt_resolver_result_fn)(int errcode, struct addrinfo *ai, void *arg);

#define GIRT_LOCATION(c) (&(girt_location){ .file = __FILE__, .line = __LINE__, .function = #c })

typedef enum {
	GIRT_ATTR_FREE = 0,
	GIRT_ATTR_TAG,
	GIRT_ATTR_PTR,
	GIRT_ATTR_KSTR,
	GIRT_ATTR_U64,
	GIRT_ATTR_I64,
} girt_attr_type;

#define GIRT_ATTR_PTR_STR -1
#define GIRT_ATTR_PTR_ALIAS -2
#define GIRT_ATTR_PTR_HANDLE -3
#define GIRT_ATTR_PTR_SOCKADDR -4

#define	GIRT_ATTR_NOTIFY_ENABLED 1
#define GIRT_ATTR_NOTIFY_PERSIST 2
#define GIRT_ATTR_NOTIFY_DIRECT  4

#define GIRT_FLOW_EVENT_PERSIST	1
#define GIRT_FLOW_EVENT_REUSE	2

typedef enum {
									// > 0 means skip bytes on input
	GIRT_PREREADER_AGAIN = 0,		// Try again later
	GIRT_PREREADER_DONE = -1,		// Don't come back
	GIRT_PREREADER_FINAL = -2,		// Like 'done' but more so
	GIRT_PREREADER_CLOSED = -3,		// Client connection closed
	GIRT_PREREADER_ERROR = -4,		// Non-recoverable error
} girt_prereader_state;

typedef struct evbuffer girt_datagram;

typedef enum {
	GIRT_MESSAGE_LIBNET = 1,
	GIRT_MESSAGE_MSGHDR = 2,
} girt_message_method;

#endif // _GIRT_TYPES_H

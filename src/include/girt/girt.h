#ifndef _GIRT_GIRT_H
#define _GIRT_GIRT_H

#include <girt/extern.h>

#include <girt/khash.h>
#include <girt/kvec.h>
#include <girt/kstring.h>
#include <girt/ulid.h>
#include <girt/defer.h>

#include <girt/logger.h>
#include <girt/opaque.h>
#include <girt/types.h>
#include <girt/funcs.h>

#endif // _GIRT_FUNCS_H


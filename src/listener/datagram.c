
#include <girt/girt.h>

typedef struct {
	struct sockaddr *orig;
	socklen_t		origlen;
	struct sockaddr *dest;
	socklen_t		destlen;
} local_flow_name, *local_flow_name_ptr;

typedef struct {
	girt_flow		*flow;
	unsigned int	doomed:1;
	unsigned int	dead:1;
} local_flow;

static khint_t local_flow_name_hash(local_flow_name_ptr key);
static int local_flow_name_equal(local_flow_name_ptr a, local_flow_name_ptr b);

KHASH_INIT(flows, local_flow_name_ptr, local_flow *, 1, local_flow_name_hash, local_flow_name_equal)

static void local_flow_free(khash_t(flows) *flows, khiter_t k);

typedef struct {
	girt_listener *listener;
	girt_message *message;
	char buf[0x10000];
	khash_t(flows) *flows;
	struct event *clean;
	struct event *read;
	uint64_t max_idle;
	uint64_t max_free_wait;
	girt_message_method spoof_method;
	struct evbuffer *evbuf;
} girt_local;

void _girt_listener_free(girt_local *local);

#ifdef IP_RECVORIGDSTADDR
static void local_read_callback(evutil_socket_t sock, short int what, void *arg);
static void local_clean_flows_callback(evutil_socket_t sock, short int what, void *arg);
static local_flow *local_flow_get_or_init(girt_local *local, int fd,
	struct sockaddr *orig, socklen_t origlen, struct sockaddr *dest, socklen_t destlen,
	int pktinfo_type, void *pktinfo, size_t pktinfo_len);
static void local_free_data(const void *data, size_t datalen, void *extra);

girt_local *
_girt_listener_init(girt_listener *listener, UNUSED girt_cfg *listener_config)
{
	girt_enter();
	girt_local *local = girt_malloc(sizeof(*local));
	defer { girt_if_fail { _girt_listener_free(local); } }

	local->listener = listener;
	girt_lsnsock *s = girt_listener_socket(listener);
	char *addrname = girt_lsnsock_addrname(s);
	char *name = girt_listener_name(listener);

	local->message = girt_message_init();

	local->flows = girt_check_oom_ptr(kh_init(flows));
	local->evbuf = evbuffer_new();

	int one = 1;
	if (setsockopt(girt_lsnsock_fd(s), SOL_IP, IP_RECVORIGDSTADDR, (void *)&one, sizeof(one)) < 0) {
		girt_loc_printf(ERROR, name, "Failed to set 'ip_recvorigdestaddr' for socket (%s): %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	// https://stackoverflow.com/questions/3062205/setting-the-source-ip-for-a-udp-socket
	if (setsockopt(girt_lsnsock_fd(s), IPPROTO_IP, IP_PKTINFO, &one, sizeof(one)) < 0) {
		girt_loc_printf(ERROR, name, "Failed to set 'ip_pktinfo' for socket (%s): %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	if (girt_lsnsock_family(s) == AF_INET6) {
		if (setsockopt(girt_lsnsock_fd(s), IPPROTO_IPV6, IPV6_RECVPKTINFO, &one, sizeof(one)) < 0) {
			girt_loc_printf(ERROR, name, "Failed to set 'ipv6_recvpktinfo' for socket (%s): %s\n", addrname, strerror(errno));
			girt_fail(NULL);
		}
		int zero = 0;
		if (setsockopt(girt_lsnsock_fd(s), IPPROTO_IPV6, IPV6_V6ONLY, &zero, sizeof(zero)) < 0) {
			girt_loc_printf(ERROR, name, "Failed to set 'ipv6_v6only' for socket (%s): %s\n", addrname, strerror(errno));
			girt_fail(NULL);
		}
	}

	if (bind(girt_lsnsock_fd(s), girt_lsnsock_addr(s), girt_lsnsock_addrlen(s)) < 0) {
		girt_loc_printf(ERROR, name, "Failed to bind socket (%s) : %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	local->read = girt_check_oom_ptr(
		event_new(
			girt_listener_event_base(listener),
			girt_lsnsock_fd(s),
			EV_READ | EV_PERSIST,
			local_read_callback,
			local
		)
	);
	event_add(local->read, NULL);

	local->clean = event_new(girt_listener_event_base(listener), -1, EV_PERSIST, local_clean_flows_callback, local);
	// @config <listener:datagram> flow-clean-interval period 1m
	// @config-desc Interval for checking for idle flows
	struct timeval *interval = girt_config_get_timeval(listener_config, "flow-clean-interval", "1m");
	event_add(local->clean, interval);
	free(interval);

	// @config <listener:datagram> flow-max-idle period 1m
	// @config-desc Max idle time for flows before they are dropped. The actual allowed idle time can be up to "flow-max-idle" plus "flow-clean-interval".
	local->max_idle = girt_config_get_millis(listener_config, "flow-max-idle", "1m");

	// @config <listener:datagram> flow-max-free-wait period 5s
	// @config-desc Maximum time to wait for all flows to be freed when listener is exiting.
	local->max_free_wait = girt_config_get_millis(listener_config, "flow-max-free-wait", "1m");

	// @config <listener:datagram> spoof-method string "libnet"
	// @config-desc Method to use for spoofing source addresses on UDP replies. Either "libnet" or "msghdr".
	char *method_name = girt_config_get_string(listener_config, "spoof-method", "libnet");
	defer { free(method_name); }
	if (!strcmp(method_name, "libnet")) {
		local->spoof_method = GIRT_MESSAGE_LIBNET;
	} else
	if (!strcmp(method_name, "msghdr")) {
		local->spoof_method = GIRT_MESSAGE_MSGHDR;
	} else {
		girt_loc_printf(INFO, name, "Unknown UDP address spoof method '%s'\n", method_name);
	}

	girt_loc_printf(INFO, name, "Listening on %s/%s", addrname, girt_lsnsock_protoname(s));

	return local;
}

void *
_girt_listener_listen(void *arg)
{
	girt_local *local = arg;

	event_base_loop(girt_listener_event_base(local->listener), EVLOOP_NO_EXIT_ON_EMPTY);

	return NULL;
}

void
_girt_listener_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	if (local->read != NULL) {
		event_free(local->read);
		local->read = NULL;
	}
	if (local->clean != NULL) {
		event_free(local->clean);
		local->clean = NULL;
	}
	if (local->evbuf != NULL) {
		evbuffer_free(local->evbuf);
	}

	girt_message_free(local->message);

	for (khiter_t k = kh_begin(local->flows); k != kh_end(local->flows); ++k) {
		if (kh_exist(local->flows, k)) {
			local_flow *lflow = kh_value(local->flows, k);
			if (!lflow->doomed) {
				lflow->doomed = 1;
				girt_listener_flow_free_async(local->listener, lflow->flow);
			}
		}
	}

	uint64_t end_time = girt_current_millis() + local->max_free_wait;
	while (kh_size(local->flows) > 0) {
		int timed_out = (girt_current_millis() >= end_time);
		for (khiter_t k = kh_begin(local->flows); k != kh_end(local->flows); ++k) {
			if (kh_exist(local->flows, k)) {
				if (kh_value(local->flows, k)->dead || timed_out) {
					local_flow_free(local->flows, k);
				}
			}
		}
		if (kh_size(local->flows) > 0) {
			girt_millisleep(200);
		}
		if (timed_out) {
			girt_loc_printf(ERROR, girt_listener_name(local->listener), "Timed out waiting for %d flows to die\n", kh_size(local->flows));
			break;
		}
	}

	kh_destroy(flows, local->flows);

	memset(local, 0, sizeof(*local));

	free(local);
}

static void
local_read_callback(evutil_socket_t fd, UNUSED short int what, void *arg)
{
	girt_local *local = arg;
	girt_lsnsock *s = girt_listener_socket(local->listener);
	char *name = girt_listener_name(local->listener);

	struct sockaddr *destaddr = NULL;
	socklen_t destaddrlen = 0;
	void *pktinfo = NULL;
	size_t pktinfolen = 0;
	int pktinfotype = 0;
	ssize_t pktlen;

	pktlen = girt_message_receive(girt_message_set_data(local->message, local->buf, sizeof(local->buf)), fd, 0);

	if (pktlen == -1) {
		girt_loc_printf(ERROR, name, "Failed to receive packet: %s\n", strerror(errno));
		return;
	}
	if (pktlen >= (ssize_t)sizeof(local->buf)) {
		girt_loc_printf(ERROR, name, "Truncated packet, buffer size %lu, packet size %lu\n", pktlen, sizeof(local->buf));
		return;
	}

	if (girt_lsnsock_transparent(s)) {
		for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(girt_message_msghdr(local->message)); cmsg; cmsg = CMSG_NXTHDR(girt_message_msghdr(local->message), cmsg)) {
			if ((cmsg->cmsg_level == IPPROTO_IP && cmsg->cmsg_type == IP_PKTINFO) ||
				(cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_PKTINFO)) {
				girt_loc_printf(INFO, name, "Received pktinfo: cmsg level %d, type %d\n", cmsg->cmsg_level, cmsg->cmsg_type);
				if (pktinfo == NULL) {
					pktinfotype = cmsg->cmsg_type;
					pktinfolen = pktinfotype == IP_PKTINFO ? sizeof(struct in_pktinfo) : sizeof(struct in6_pktinfo);
					if (cmsg->cmsg_len != CMSG_LEN(pktinfolen)) {
						girt_loc_printf(ERROR, name, "Unexpected pktinfo cmsg length %lu, expected %lu or %lu\n", cmsg->cmsg_len,
							CMSG_LEN(sizeof(struct in_pktinfo)), CMSG_LEN(sizeof(struct in6_pktinfo)));
					} else {
						pktinfo = CMSG_DATA(cmsg);
					}
				}
			} else
			if (cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_ORIGDSTADDR) {
				if (cmsg->cmsg_len != CMSG_LEN(sizeof(struct sockaddr_in)) &&
						cmsg->cmsg_len != CMSG_LEN(sizeof(struct sockaddr_in6))) {
					girt_loc_printf(ERROR, name, "Unexpected dstaddr cmsg length %lu, expected %lu or %lu\n", cmsg->cmsg_len,
						CMSG_LEN(sizeof(struct sockaddr_in)), CMSG_LEN(sizeof(struct sockaddr_in6)));
				} else {
					destaddrlen = cmsg->cmsg_len - CMSG_LEN(0);
					destaddr = (struct sockaddr *)CMSG_DATA(cmsg);
				}
			} else {
				girt_loc_printf(ERROR, name, "Unexpected cmsg level %d, type %d\n", cmsg->cmsg_level, cmsg->cmsg_type);
			}
		}
	}

	if (destaddr == NULL || destaddrlen == 0) {
		girt_loc_printf(ERROR, name, "No dest address for %ld byte packet\n", pktlen);
	} else {
		socklen_t origaddrlen = 0;
		struct sockaddr *origaddr = girt_message_sockaddr(local->message, &origaddrlen);
		local_flow *lflow = local_flow_get_or_init(local, fd, origaddr, origaddrlen, destaddr, destaddrlen, pktinfotype, pktinfo, pktinfolen);
		if (lflow != NULL) {
			evbuffer_add_reference(local->evbuf, girt_memdup(local->buf, pktlen), pktlen, local_free_data, lflow->flow);

			// girt_loc_printf(ERROR, name, "%s -> %s\n", girt_flow_attrs(get_str, lflow->flow, "orig.addr.name"), girt_flow_attrs(get_str, lflow->flow, "dest.addr.name"));
			girt_listener_flow_send(local->listener, lflow->flow, local->evbuf);
		}
	}
}

static void
local_free_data(const void *data, UNUSED size_t datalen, UNUSED void *extra)
{
	free((void *)data);
}

static void
local_flow_free(khash_t(flows) *flows, khiter_t k)
{
	local_flow *lflow = kh_value(flows, k);
	free(lflow);

	local_flow_name *key = kh_key(flows, k);
	free(key->orig);
	free(key->dest);
	free(key);

	kh_del(flows, flows, k); 
}

static void
local_mark_dead_pp(void **ptr)
{
	local_flow **lflow_pp = (local_flow **)ptr;

	if (lflow_pp != NULL && *lflow_pp != NULL) {
		(*lflow_pp)->dead = 1;
	}
}

static local_flow *local_flow_get_or_init(girt_local *local, int orig_fd,
	struct sockaddr *orig, socklen_t origlen, struct sockaddr *dest, socklen_t destlen,
	int pktinfotype, void *pktinfo, UNUSED size_t pktinfolen)
{
	girt_enter();
	local_flow *lflow = NULL;
	local_flow_name key[1] = {{
		.orig = orig,
		.origlen = origlen,
		.dest = dest,
		.destlen = destlen
	}};

	int ret = 0;
	khiter_t k = kh_put(flows, local->flows, key, &ret);
	defer {	girt_if_fail { kh_del(flows, local->flows, k); } }
	if (ret) {
		girt_lsnsock *s = girt_listener_socket(local->listener);
		int dest_fd = girt_socket(girt_lsnsock_family(s), girt_lsnsock_type(s), girt_lsnsock_proto(s));
		if (dest_fd < 0) {
			girt_fail(NULL);
		}
		defer {	girt_if_fail { close(dest_fd); } }
		if (connect(dest_fd, dest, destlen) != 0) {
			girt_loc_printf(ERROR, girt_listener_name(local->listener), "Failed to 'connect' UDP socket: %s\n", strerror(errno));
			girt_fail(NULL);
		}

		// new, set a permanent key
		key->orig = girt_memdup(key->orig, key->origlen);
		key->dest = girt_memdup(key->dest, key->destlen);
		kh_key(local->flows, k) = girt_memdup(key, sizeof(*key));

		kh_value(local->flows, k) = lflow = girt_malloc(sizeof(local_flow));
		lflow->flow = girt_listener_flow_init(local->listener, dup(orig_fd), orig, origlen);
		girt_flow_attrs(put_u64, lflow->flow, "time.connect", girt_current_millis());
		girt_flow_attrs(put_ptr, lflow->flow, "datagram.backpointer", lflow, local_mark_dead_pp);

		// Set up reply message, just add actual data pointer when sending
		if (pktinfo != NULL) {
			girt_message *reply = NULL;
			switch (local->spoof_method) {
				case GIRT_MESSAGE_MSGHDR: {
					reply = girt_message_spoof_init(GIRT_MESSAGE_MSGHDR, orig, origlen, dest, destlen, 0);
					break;
				}
				case GIRT_MESSAGE_LIBNET: {
					int ifidx = 0;
					if (pktinfotype == IP_PKTINFO) {
						struct in_pktinfo *ipi = pktinfo;
						ifidx = ipi->ipi_ifindex;
					} else {
						struct in6_pktinfo *ipi = pktinfo;
						ifidx = (int)ipi->ipi6_ifindex;
					}
					reply = girt_message_spoof_init(GIRT_MESSAGE_LIBNET, orig, origlen, dest, destlen, ifidx);
					break;
				}
			}

			girt_flow_attrs(put_ptr, lflow->flow, "message.reply", reply, girt_message_free_pp);
		}

		girt_flow_set_sockaddr_fd(lflow->flow, "dest.addr", dest, destlen, dest_fd);
		girt_listener_flow_start(local->listener, lflow->flow);
	} else {
		lflow = kh_value(local->flows, k);
	}

	girt_flow_attrs(put_u64, lflow->flow, "time.latest", girt_current_millis());

	girt_return(lflow);
}

static void
local_clean_flows_callback(UNUSED evutil_socket_t sock, UNUSED short int what, void *arg)
{
	girt_local *local = arg;
	uint64_t now = girt_current_millis();

	girt_loc_print(DEBUG, girt_listener_name(local->listener), "Flow cleaning\n");

	for (khiter_t k = kh_begin(local->flows); k != kh_end(local->flows); ++k) {
		if (kh_exist(local->flows, k)) {
			local_flow *lflow = kh_value(local->flows, k);

			if (lflow->dead) {
				local_flow_free(local->flows, k);
			} else
			if (!lflow->doomed) {
				uint64_t age = now - girt_flow_attrs(get_u64, lflow->flow, "time.latest");
				lflow->doomed = (age > local->max_idle);

				if (girt_flow_is_priority_enabled(DEBUG)) {
					char *orig = girt_flow_attrs(get_str, lflow->flow, "orig.addr.name");
					char *dest = girt_flow_attrs(get_str, lflow->flow, "dest.addr.name");
					girt_loc_printf(DEBUG, girt_listener_name(local->listener), "%s %s -> %s %lums\n", lflow->doomed ? "!" : " ", orig, dest, age);
				}

				if (lflow->doomed) {
					girt_listener_flow_free_async(local->listener, lflow->flow);
				}
			}
		}
	}
}

static khint_t
local_flow_name_hash(local_flow_name_ptr key)
{
	return crc32(crc32(0, key->orig, key->origlen), key->dest, key->destlen);
}

static int
local_flow_name_equal(local_flow_name_ptr a, local_flow_name_ptr b)
{
	return a->origlen == b->origlen && a->destlen == b->destlen &&
			!memcmp(a->orig, b->orig, a->origlen) && !memcmp(a->dest, b->dest, a->destlen);
}

#else
girt_local *
_girt_listener_init(girt_listener *listener, UNUSED girt_cfg *listener_config)
{
	girt_loc_printf(ERROR, girt_listener_name(local->listener), "'IP_RECVORIGDSTADDR' not supported\n");

	return NULL;
}

void *
_girt_listener_listen(void *arg)
{
	return NULL
}

void
_girt_listener_free(UNUSED girt_local *local)
{
}
#endif


#include <girt/girt.h>

typedef struct {
	struct evconnlistener *conn;
	girt_listener *listener;
} girt_local;

static void local_accept_callback(struct evconnlistener *conn, evutil_socket_t fd, struct sockaddr *addr, int len, void *arg);
static void local_accept_error_callback(struct evconnlistener *conn, void *arg);
void _girt_listener_free(girt_local *local);

girt_local *
_girt_listener_init(girt_listener *listener, girt_cfg *listener_config)
{
	girt_enter();
	girt_local *local = girt_malloc(sizeof(girt_local));
	defer { girt_if_fail { _girt_listener_free(local); } }

	local->listener = listener;
	girt_lsnsock *s = girt_listener_socket(listener);
	char *addrname = girt_lsnsock_addrname(s);
	char *name = girt_listener_name(listener);

	int one = 1;
	if (setsockopt(girt_lsnsock_fd(s), SOL_SOCKET, SO_KEEPALIVE, (void *)&one, sizeof(one)) < 0) {
		girt_loc_printf(ERROR, name, "Failed to set keepalive for socket (%s): %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	if (bind(girt_lsnsock_fd(s), girt_lsnsock_addr(s), girt_lsnsock_addrlen(s)) < 0) {
		girt_loc_printf(ERROR, name, "Failed to bind socket (%s): %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	// @config <listener:stream> backlog i64 The OS limit from /proc/sys/net/core/somaxconn
	// @config-desc The backlog parameter passed to the listen() system call. See the listen(2) man page for details.
	uint64_t backlog = girt_config_get_i64(listener_config, "backlog", girt_limits_get_max_backlog());

	local->conn = evconnlistener_new(
		girt_listener_event_base(listener),
		local_accept_callback,
		local,
		LEV_OPT_CLOSE_ON_FREE | LEV_OPT_THREADSAFE,
		backlog,
		girt_lsnsock_fd(s)
	);
	if (local->conn == NULL) {
		girt_loc_printf(ERROR, name, "Failed to listen on socket (%s): %s\n", addrname, strerror(errno));
		girt_fail(NULL);
	}

	evconnlistener_set_error_cb(local->conn, local_accept_error_callback);

	girt_loc_printf(INFO, name, "Listening on %s/%s (backlog %lu)", addrname, girt_lsnsock_protoname(s), backlog);

	return local;
}

void *
_girt_listener_listen(void *arg)
{
	girt_local *local = arg;

	event_base_loop(girt_listener_event_base(local->listener), EVLOOP_NO_EXIT_ON_EMPTY);

	return NULL;
}

void
_girt_listener_free(girt_local *local)
{
	if (local == NULL || local->conn == NULL) {
		return;
	}

	evconnlistener_free(local->conn);
	local->conn = NULL;

	memset(local, 0, sizeof(*local));
	free(local);
}

static void
local_accept_callback(UNUSED struct evconnlistener *conn, evutil_socket_t fd, struct sockaddr *addr, int len, void *arg)
{
	girt_local *local = arg;
	girt_flow *flow = girt_listener_flow_init(local->listener, fd, addr, len);
	girt_lsnsock *s = girt_listener_socket(local->listener);

	if (girt_lsnsock_transparent(s)) {
		struct sockaddr_storage _destaddr = {0};
		struct sockaddr *destaddr = (struct sockaddr *)&_destaddr;
		socklen_t destaddrlen = sizeof(_destaddr);
		if (getsockname(fd, destaddr, &destaddrlen) != 0) {
			girt_flow_printf(ERROR, flow, "Failed to get dest address: %s", strerror(errno));
			girt_listener_flow_free(local->listener, flow);
			return;
		}
		girt_flow_set_sockaddr_fd(flow, "dest.addr", destaddr, destaddrlen, -1);
	}

	girt_listener_flow_start(local->listener, flow);
}

static void
local_accept_error_callback(struct evconnlistener *conn, void *arg)
{
	girt_local *local = arg;

	int err = EVUTIL_SOCKET_ERROR();
	girt_printf(ERROR, "%s: (%d) %s\n", girt_listener_name(local->listener), err, evutil_socket_error_to_string(err));

	event_base_loopexit(evconnlistener_get_base(conn), NULL);
}


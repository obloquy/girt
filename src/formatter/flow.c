#include <girt/girt.h>

typedef enum {
	TOKEN_UNKNOWN,
	TOKEN_FLOW_IDSTR,
	TOKEN_FLOW_WORKER_IDX,
	TOKEN_FLOW_PROXY_NAME,
} template_token_type;

typedef struct {
	girt_logtemplate *template;
	size_t max_header_len;
	pthread_key_t bufkey;
} girt_local;

static size_t template_render(char *buf, size_t buflen, int token_type, char *format, const log4c_logging_event_t *event);
static int template_tokenize(char *name);

girt_local *
_girt_formatter_init(UNUSED girt_formatter *formatter, girt_cfg *formatter_config, log4c_layout_t *layout)
{
	girt_local *local = girt_malloc(sizeof(*local));

	// @config <formatter> template string "{localtime:%F %T}.{msec} {priority-name:%1.1s} {flow.proxy.name:%4.4s} {flow.idstr} {flow.worker.idx} {message}"
	char *template_str = girt_config_get_string(formatter_config, "template",
		"{localtime:%F %T}.{msec} {priority-name:%1.1s} {flow.proxy.name:%4.4s} {flow.idstr} {flow.worker.idx} {message}"
	);
	local->template = girt_logtemplate_init(template_str, template_render, template_tokenize);
	free(template_str);
	// @config <formatter> max-header-len i64 100
	local->max_header_len = girt_config_get_i64(formatter_config, "max-header-len", 100);

	log4c_layout_set_udata(layout, local);

	girt_threadbuf_make(&local->bufkey);

	return local;
}

void
_girt_formatter_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_logtemplate_free(local->template);

	memset(local, 0, sizeof(*local));

	free(local);
}


const char *
_girt_formatter_format(const log4c_layout_t *layout, const log4c_logging_event_t *event)
{
	girt_local *local = log4c_layout_get_udata(layout);

	size_t buflen = local->max_header_len + strlen(event->evt_msg);
	char *buf = girt_threadbuf_get(local->bufkey, buflen);
	size_t len = girt_logtemplate_render_event(local->template, buf, buflen, event);

	if (len > 0) {
		if (len < buflen-1 && buf[len-1] != '\n') {
			buf[len++] = '\n';
			buf[len] = '\0';
		}
		return buf;
	}

	return event->evt_msg;
}

static int
template_tokenize(char *name)
{
	if (!strcmp(name, "flow.idstr")) {
		return TOKEN_FLOW_IDSTR;
	}
	if (!strcmp(name, "flow.worker.idx")) {
		return TOKEN_FLOW_WORKER_IDX;
	}

	if (!strcmp(name, "flow.proxy.name")) {
		return TOKEN_FLOW_PROXY_NAME;
	}

	return TOKEN_UNKNOWN;
}

static char *
template_format(int type)
{
	switch (type) {
	case TOKEN_UNKNOWN:
	case TOKEN_FLOW_IDSTR:
	case TOKEN_FLOW_PROXY_NAME:
		return "%s";
	case TOKEN_FLOW_WORKER_IDX:
		return "%3d";
		break;
	}

	return "???";
}

static size_t
template_render(char *buf, size_t buflen, int token_type, char *format, const log4c_logging_event_t *event)
{
	girt_flow *flow = event->evt_loc->loc_data;

	format = (format ? format : template_format(token_type));

	switch (token_type) {
	case TOKEN_FLOW_IDSTR:
		return snprintf(buf, buflen, format, girt_flow_idstr(flow));
	case TOKEN_FLOW_WORKER_IDX:
		return snprintf(buf, buflen, format, girt_worker_idx(girt_flow_worker(flow)));
	case TOKEN_FLOW_PROXY_NAME:
		return snprintf(buf, buflen, format, girt_flow_attrs(get_str, flow, "proxy.name"));
	}

	return 0;
}

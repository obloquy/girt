
#include <girt/girt.h>

typedef struct {
	girt_logtemplate *template;
	size_t max_header_len;
	pthread_key_t bufkey;
} girt_local;

girt_local *
_girt_formatter_init(UNUSED girt_formatter *formatter, girt_cfg *formatter_config, log4c_layout_t *layout)
{
	girt_local *local = girt_malloc(sizeof(*local));

	// @config <formatter:girt> template string "{localtime:%F %T}.{msec} {priority-name:%1.1s} {location-name:%4.4s} {file:%8.8s}:{line:%4d} {message}"
	char *template_str = girt_config_get_string(formatter_config, "template",
		"{localtime:%F %T}.{msec} {priority-name:%1.1s} {location-name:%4.4s} {file:%8.8s}:{line:%4d} {message}"
	);
	local->template = girt_logtemplate_init(template_str, NULL, NULL);
	free(template_str);
	// @config <formatter:girt> max-header-len i64 100
	local->max_header_len = girt_config_get_i64(formatter_config, "max-header-len", 100);

	log4c_layout_set_udata(layout, local);

	girt_threadbuf_make(&local->bufkey);

	return local;
}

void
_girt_formatter_free(girt_local *local)
{
	if (local == NULL) {
		return;
	}

	girt_logtemplate_free(local->template);

	memset(local, 0, sizeof(*local));

	free(local);
}

const char *
_girt_formatter_format(const log4c_layout_t *layout, const log4c_logging_event_t *event)
{
	girt_local *local = log4c_layout_get_udata(layout);

	size_t buflen = local->max_header_len + strlen(event->evt_msg);
	char *buf = girt_threadbuf_get(local->bufkey, buflen);
	size_t len = girt_logtemplate_render_event(local->template, buf, buflen, event);

	if (len > 0) {
		if (len < buflen-1 && buf[len-1] != '\n') {
			buf[len++] = '\n';
			buf[len] = '\0';
		}
		return buf;
	}

	return event->evt_msg;
}


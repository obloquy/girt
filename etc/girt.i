daemon daemon/girt {
  need = system/bootmisc;
  require_network;
  exec daemon = /usr/bin/girt /etc/girt.d;
  respawn;
}

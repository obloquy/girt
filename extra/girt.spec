
Summary: girt proxy
Name: girt
Version: 0.1
Release: 1%{?tnt_dist}
License: Tesserent
Group: Applications/Network
URL: https://www.tesserent.com
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
girt proxy.

%build
%{__make} -C src %{?_smp_mflags}

%install
%{__make} -C src DESTDIR=%{buildroot} install

%{__install} -D -pm 644 etc/proxy.conf %{buildroot}/opt/tnt/girt/etc/conf.d/proxy.conf

%if 0%{?centos}
%{__install} -D -pm 644 etc/girt.service %{buildroot}%{_unitdir}/girt.service
%else
%define initng_script initng/daemon/girt.i
%{__install} -D -pm 644 etc/girt.i  %{buildroot}%{_sysconfdir}/%{initng_script}
%endif

%{__install} -D -pm 644 etc/girt.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/girt

%{_shift_configs} %{buildroot} \
	%{?initng_script} sysconfig/girt
%{_shift_configs} -s /opt/tnt/girt/etc %{buildroot} \
	conf.d/proxy.conf

%clean
%{__rm} -rf %{buildroot}
cd src
%{__make} clean

%pre
%{_remove_configs} \
	%{?initng_script} sysconfig/girt
%{_remove_configs} -d /opt/tnt/girt/etc \
	conf.d/proxy.conf

%post
%{_place_configs} \
	%{?initng_script} sysconfig/girt
%{_place_configs} -d /opt/tnt/girt/etc \
	conf.d/proxy.conf

%preun
if [ $1 = 0 ] ; then
  %{_remove_configs} \
	%{?initng_script} sysconfig/girt
  %{_remove_configs} -d /opt/tnt/girt/etc \
	conf.d/proxy.conf
fi

%files
%defattr(-, root, root, 0755)
/opt/tnt/girt/bin/%{name}
/opt/tnt/girt/lib/*
%attr(0644,root,root) %{__nbrs_configs_dir}/conf.d/proxy.conf
%attr(0644,root,root) %{__nbrs_configs_dir}/sysconfig/girt

%if 0%{?centos}
%{_unitdir}/girt.service
%else
%attr(0644,root,root) %{__nbrs_configs_dir}/initng/daemon/girt.i
%endif

%changelog
* Fri Apr 5 2019 Jesper Peterson <jesper.peterson@tesserent.com>
- Initial package


#include <girt/extern.h>

#define have_girt_http
#include <girt/types.h>

typedef struct {
	girt_lsnsock *socket;
	pthread_key_t bufkey;
	size_t buflen;
} girt_http;

#include <girt/funcs.h>

#include <girt/http_parser.h>

#if 0
girt_http *
_girt_addresser_init(UNUSED girt_cfg *global, girt_cfg *config, UNUSED girt_plg *plugins, girt_lsnsock *s)
{
	girt_http *dest;
	CHECK_OOM(dest = malloc(sizeof(girt_static)));
	memset(dest, 0, sizeof(girt_static));

	dest->lsnsock = s;
	girt_threadbuf_make(&dest->key);
	dest->buflen = girt_config_get_i64(config, "buffer-size", 0x4000);

	return dest;
}

void
_girt_addresser_free(girt_static *dest)
{
	if (dest == NULL) {
		return;
	}

	memset(dest, 0, sizeof(*dest));

	free(dest);
}

int
_girt_addresser_getdest(girt_static *dest, girt_flow *flow)
{
	char *buf = girt_threadbuf_get(dest->bufkey, dest->buflen);
	ssize_t n;

    // (re-)peek all client data
    if ((n = recv(girt_lsnsock_fd(dest->socket), buf, dest->buflen, MSG_PEEK)) == 0) {
		return GIRT_ADDRESSER_CLOSED;
	} else
	if (n < 0) {
		return GIRT_ADDRESSER_AGAIN;
	} else {
		
	}

	girt_printf(DEBUG, "%s: Sending to %s", girt_lsnsock_logname(dest->lsnsock), dest->addrport);

	return girt_flow_setdest(flow, dest->addrinfo->ai_addr, dest->addrinfo->ai_addrlen);
}

#endif

static int on_proxy_header_field(http_parser*, const char *at, size_t length);
static int on_proxy_header_value(http_parser*, const char *at, size_t length);
static int on_proxy_headers_complete(http_parser*);

static http_parser_settings proxy_parser_settings[1] = {{
	.on_header_field = on_proxy_header_field,
	.on_header_value = on_proxy_header_value,
	.on_headers_complete = on_proxy_headers_complete,
}};

/*
 * Try to parse a block of data as a complete proxy request
 * which could be either a CONNECT (to get x-forwarded-for)
 * or a regular HTTP request (to get the host header).
 * Return number of bytes of data used.
 *
 * The buffer is assumed to contain at least the start line
 * and all headers including he double crlf. 
 */
int main()
{
	size_t used = 0;

	http_parser parser = {};
	http_parser_init(&parser, HTTP_REQUEST);
	char buf[80];
	ssize_t n;
	while ((n = read(0, buf, sizeof(buf))) > 0) {
		used = http_parser_execute(&parser, proxy_parser_settings, buf, n);

		if (HTTP_PARSER_ERRNO(&parser) != HPE_OK) {
			printf ("%d %s\n", HTTP_PARSER_ERRNO(&parser), http_errno_name(HTTP_PARSER_ERRNO(&parser)));
			exit(1);
		}
	}

	printf("ok used %lu\n", used);
}


/*
 *
 * HTTP Parser callbacks
 *
 */

/*
 * Call-back for HTTP parser when header parsing is complete
 */
static int on_proxy_headers_complete(http_parser* parser)
{
	printf("END OF HEADERS\n");
	return 1; // Always skip the body (if any)
}


/**
 * Call-back for HTTP parser when a header field (name) is encountered. The name ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int on_proxy_header_field(http_parser *parser, const char *at, size_t len)
{
	printf("HEADER [%*.*s]\n", (int)len, (int)len, at);
	return 0;
}

/**
 * Call-back for HTTP parser when a header value is encountered. The value ('at') is
 * not null terminated and may be a continuation from a previous call to this function
 * because the parser doesn't buffer.
 */
static int on_proxy_header_value (http_parser *parser, const char *at, size_t len)
{
	printf("VALUE [%*.*s]\n", (int)len, (int)len, at);
	return 0;
}


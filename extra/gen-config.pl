#!/bin/perl

open my $fh, '-|', 'find . -name "*.c" | xargs fgrep -h  @config | sed "s/^[ \t]*\/\/ *@config *//"';

my $wiki = 1;

my %config; # config{from}->[]->{desc,name,to,def};

if ($wiki) {
	print	'{| class="wikitable"'."\n".
			'! scope="col" style="width: 300px;" | Name'."\n".
			'! scope="col" style="width: 50px;" | Type'."\n".
			'! scope="col" style="width: 200px;" | Default'."\n".
			'! | Description'."\n";
}

my $curr;
while (my $line = <$fh>) {
	$line =~ s/^\s+|\s+$//g;

	if ($line =~ /^-desc(?:ription)?\s+(.*)$/) {
		$curr->{desc} .= " $1";
	} elsif ($line =~ /^-def(?:ault)?\s+(\S+)\s+(.*)$/) {
		$curr->{def}->{$1} = $2;
	} else {
		my ($from, $name, $type, $def) = split ' ', $line, 4;
		$type = 'integer' if $type eq 'i64';
		$curr = { name => $name, type => $type, def => $def };
		if ($from =~ /^<([^:]+):([^>]+)>$/) {
			$from = "<$1>";
			$curr->{instance} = "($2)";
		}
		push @{$config{$from}}, $curr;
	}
}

follow('', '<root>');

if ($wiki) {
	print '|}'."\n";
}

sub follow {
	my $prefix = shift;
	my $from = shift;
	my $def = shift;

	my $curr = $config{$from};
	for my $node (@{$curr}) {
		dump_node($prefix, $from, $def, $node)
	}
}

sub dump_node {
	my $prefix = shift;
	my $from = shift;
	my $def = shift;
	my $node = shift;

	$prefix = "$prefix$node->{instance}." if $prefix;

	if ($node->{type} =~ /^(<[^>]+>)(\.\.\.)?$/) {
		follow("$prefix$node->{name}".($2?'[]':''), $1, $node->{def});
	} else {
		(my $name = "$prefix$node->{name}") =~ s/\.-\././g;
		(my $desc = $node->{desc}) =~ s/\.$//;
		$desc .= '.' if $desc;
		if ($wiki) {
			print '|-'."\n";
			print "| $name || $node->{type} || ".($node->{def}||($def||{})->{$node->{name}})." || $desc\n";
		} else {
			print "$name ($node->{type}) [".($node->{def}||($def||{})->{$node->{name}})."] $desc\n";
		}
	}
}

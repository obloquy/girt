#include <stdio.h>
#include <string.h>
#include <criterion/criterion.h>

TestSuite(suite_name);

Test(suite_name, test_1) {
    cr_expect(strlen("Test") == 4, "Expected \"Test\" to have a length of 4.");
fprintf(stderr, "HELLO FROM STDERR\n");
fprintf(stdout, "HELLO FROM STDOUT\n");
}

Test(suite_name, test_2) {
    cr_expect(strlen("Hello") == 4, "This will always fail, why did I add this?");
}

Test(suite_name, test_3) {
    cr_assert(strlen("") == 0);
}



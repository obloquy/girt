#include <criterion/criterion.h>
#include <criterion/parameterized.h>

#include <girt/girt.h>
#include <private/root.h>

TestSuite(config_bad);

Test(config_bad, bad_addrinfo) {
	girt_str_vec *args = girt_str_vec_init();
	girt_str_vec_printf(args, "test");
	girt_str_vec_printf(args, "etc/values.conf");
	girt_root *root = &(girt_root){ .args = args };

	girt_cfg *config = girt_config_init(root);
	cr_assert_not_null(config, "config is null");

	// Bad addrinfo (service name with numeric address)
	socklen_t addrlen = 0;
	struct sockaddr *sa = girt_config_get_sockaddr(config, "bad-addrinfo", 1, AF_INET, NULL, NULL, &addrlen);
	cr_expect_null(sa, "sockaddr is not null");
}

Test(config_bad, bad_thread_attr) {
	girt_str_vec *args = girt_str_vec_init();
	girt_str_vec_printf(args, "test");
	girt_str_vec_printf(args, "etc/values.conf");
	girt_root *root = &(girt_root){ .args = args };

	girt_cfg *config = girt_config_init(root);
	cr_assert_not_null(config, "config is null");

	// Bad thread attr (unknown scheduler)
	pthread_attr_t *attr = girt_config_get_pthread_attr(config, "bad-thread");
	cr_expect_null(attr, "pthread attr is not null");
}


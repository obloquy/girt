#include <criterion/criterion.h>
#include <criterion/parameterized.h>

#include <girt/girt.h>
#include <private/root.h>

TestSuite(config);

void setup(void) {
	girt_logger_level_GIRT = GIRT_LOGGER_DEBUG;
}

Test(config, empty, .init = setup) {
	girt_str_vec *args = girt_str_vec_init();
	girt_str_vec_printf(args, "test");
	girt_str_vec_printf(args, "/dev/null");
	girt_root *root = &(girt_root){ .args = args };

	girt_cfg *config = girt_config_init(root);
	cr_assert_not_null(config, "config is null");

	// Default logging table and one empty table
	int n = girt_config_root_table_count(config);
    cr_expect_eq(n, 2, "root table count is %d", n);

	// No actual kvps
	n = girt_config_root_table_nkval(config);
    cr_expect_eq(n, 0, "root nkval is %d", n);
	// No arrays
	n = girt_config_root_table_narr(config);
    cr_expect_eq(n, 0, "root narr is %d", n);
	// One table, "logger"
	n = girt_config_root_table_ntab(config);
    cr_expect_eq(n, 1, "root ntab is %d", n);

	// Non-existent table should not be null but be a whole lot of nothing
	girt_cfg *foo = girt_config_get_table(config, "foo");
	cr_assert_not_null(foo, "foo is null");
	n = girt_config_root_table_count(foo);
    cr_expect_eq(n, 0, "foo table count is %d", n);
	n = girt_config_root_table_nkval(foo);
    cr_expect_eq(n, 0, "foo nkval is %d", n);
	n = girt_config_root_table_narr(foo);
    cr_expect_eq(n, 0, "foo narr is %d", n);
	n = girt_config_root_table_ntab(foo);
    cr_expect_eq(n, 0, "foo ntab is %d", n);
	girt_config_free_table(foo);

	// Default logger config is:
	//	[logger.category.flow]
	//	[logger.category.girt]
	//	[logger.formatter.flow]
	//	[logger.formatter.girt]
	//	[logger.appender.flow]
	//	[logger.appender.girt]

	// Logger should be an empty table
	girt_cfg *logger = girt_config_get_table(config, "logger");
	cr_assert_not_null(logger, "logger is null");
	n = girt_config_root_table_count(logger);
    cr_expect_eq(n, 1, "logger table count is %d", n);
	n = girt_config_root_table_nkval(logger);
    cr_expect_eq(n, 0, "logger nkval is %d", n);

	// category should be two sub tables: "flow" and "girt"
	girt_cfg_vec *categories = girt_config_get_subtables(logger, "category");
	cr_assert_not_null(categories, "categories is null");
	n = kv_size(*categories);
    cr_assert_eq(n, 2, "categories subtable count is %d", n);
	char *name = girt_config_get_table_name(kv_A(*categories, 0));
    cr_expect_str_eq(name, "flow", "category 0 name is %s", name);
	free(name);
	name = girt_config_get_table_name(kv_A(*categories, 1));
    cr_expect_str_eq(name, "girt", "category 1 name is %s", name);
	free(name);
	girt_config_free_subtables(categories);

	// appender should be two sub tables: "flow" and "girt"
	girt_cfg_vec *appenders = girt_config_get_subtables(logger, "appender");
	cr_assert_not_null(appenders, "appenders is null");
	n = kv_size(*appenders);
    cr_assert_eq(n, 2, "appenders subtable count is %d", n);
	name = girt_config_get_table_name(kv_A(*appenders, 0));
    cr_expect_str_eq(name, "flow", "appender 0 name is %s", name);
	free(name);
	name = girt_config_get_table_name(kv_A(*appenders, 1));
    cr_expect_str_eq(name, "girt", "appender 1 name is %s", name);
	free(name);
	girt_config_free_subtables(appenders);

	// formatter should be two sub tables: "flow" and "girt"
	girt_cfg_vec *formatters = girt_config_get_subtables(logger, "formatter");
	cr_assert_not_null(formatters, "formatters is null");
	n = kv_size(*formatters);
    cr_assert_eq(n, 2, "formatters subtable count is %d", n);
	name = girt_config_get_table_name(kv_A(*formatters, 0));
    cr_expect_str_eq(name, "flow", "formatter 0 name is %s", name);
	free(name);
	name = girt_config_get_table_name(kv_A(*formatters, 1));
    cr_expect_str_eq(name, "girt", "formatter 1 name is %s", name);
	free(name);
	girt_config_free_subtables(formatters);

	girt_config_free_table(logger);

	girt_config_free(config);
	girt_str_vec_free(args);
}

Test(config, values, .init = setup) {
	girt_str_vec *args = girt_str_vec_init();
	girt_str_vec_printf(args, "test");
	girt_str_vec_printf(args, "etc/values.conf");
	girt_root *root = &(girt_root){ .args = args };

	girt_cfg *config = girt_config_init(root);
	cr_assert_not_null(config, "config is null");

	// Default logging table and one table for values
	int n = girt_config_root_table_count(config);
    cr_expect_eq(n, 2, "root table count is %d", n);

	n = girt_config_root_table_nkval(config);
    cr_expect_eq(n, 8, "root nkval is %d", n);
	// Five tables from 'etc/values.conf' and one from the default logger 
	n = girt_config_root_table_narr(config);
    cr_expect_eq(n, 3, "root narr is %d", n);
	n = girt_config_root_table_ntab(config);
    cr_expect_eq(n, 7, "root ntab is %d", n);

	// Integers
	int64_t i64 = girt_config_get_i64(config, "dec", -1);
    cr_expect_eq(i64, 10, "dec is %d", i64);
	i64 = girt_config_get_i64(config, "hex", -1);
    cr_expect_eq(i64, 16, "hex is %d", i64);
	i64 = girt_config_get_i64(config, "oct", -1);
    cr_expect_eq(i64, 8, "oct is %d", i64);
	i64 = girt_config_get_i64(config, "bin", -1);
    cr_expect_eq(i64, 2, "bin is %d", i64);
	i64 = girt_config_get_i64(config, "foo", -1);
    cr_expect_eq(i64, -1, "foo is %d", i64);

	// Periods
	uint64_t u = girt_config_get_millis(config, "period", NULL);
    cr_expect_eq(u, 90500, "period is %d", u);
	u = girt_config_get_millis(config, "foo", NULL);
    cr_expect_eq(u, 0, "NULL millis is %d", u);
	u = girt_config_get_millis(config, "foo", "1h");
    cr_expect_eq(u, 3600000, "hour is %d", u);
	struct timeval *tv = girt_config_get_timeval(config, "period", NULL);
	cr_assert_not_null(tv, "tv is null");
    cr_expect_eq(tv->tv_sec, 90, "tv_sec is %d", tv->tv_sec);
    cr_expect_eq(tv->tv_usec, 500000, "tv_usec is %d", tv->tv_usec);
	girt_config_free_timeval(tv);

	// Strings
	char *str = girt_config_get_string(config, "string", NULL);
	cr_assert_not_null(str, "str is null");
	cr_expect_str_eq(str, "Hello World!\n", "str is '%s'", str);
	free(str);
 	str = girt_config_get_string(config, "foo", "bar");
	cr_assert_not_null(str, "foo is null");
	cr_expect_str_eq(str, "bar", "bar is '%s'", str);
	free(str);
	str = girt_config_get_string(config, "foo", NULL);
	cr_expect_null(str, "NULL str is not null");
	free(str);

	// Integer as string
	str = girt_config_get_string(config, "hex", NULL);
	cr_assert_not_null(str, "hex str is null");
	cr_expect_str_eq(str, "0x10", "hex str is '%s'", str);
	free(str);

	// Booleans
	int b = girt_config_get_boolean(config, "bool-true", 0);
	cr_expect(b, "true is false");
	b = girt_config_get_boolean(config, "bool-false", 1);
	cr_expect_not(b, "false is true");
	b = girt_config_get_boolean(config, "foo", 1);
	cr_expect(b, "default is false");
	b = girt_config_get_boolean(config, "foo", 0);
	cr_expect_not(b, "default is true");

	// Tables
	girt_cfg *sec = girt_config_get_table(config, "section");
	cr_assert_not_null(sec, "sec is null");
	n = girt_config_root_table_nkval(sec);
    cr_expect_eq(n, 2, "sec nkval is %d", n);
	n = girt_config_root_table_narr(sec);
    cr_expect_eq(n, 1, "sec narr is %d", n);
	n = girt_config_root_table_ntab(sec);
    cr_expect_eq(n, 0, "sec ntab is %d", n);

	girt_cfg *hi = girt_config_get_table(config, "hostinfo");
	cr_assert_not_null(hi, "hi is null");
	n = girt_config_root_table_nkval(hi);
    cr_expect_eq(n, 2, "hi nkval is %d", n);
	n = girt_config_root_table_narr(hi);
    cr_expect_eq(n, 0, "hi narr is %d", n);
	n = girt_config_root_table_ntab(hi);
    cr_expect_eq(n, 0, "hi ntab is %d", n);

	// Active specific host
	socklen_t addrlen = 0;
	struct sockaddr *sa = girt_config_get_sockaddr(config, "hostinfo", 0, AF_INET, NULL, NULL, &addrlen);
	cr_assert_not_null(sa, "active specific sa is null");
    cr_assert_eq(addrlen, 16, "active scpecific addrlen is %d", addrlen);
	str = girt_sockaddr_to_addrname(sa, NULL, NULL);
	cr_assert_not_null(str, "active specific sa str is null");
    cr_expect_str_eq(str, "127.0.0.1:80", "sa str is '%s'", str);
	free(str);
	girt_config_free_sockaddr(sa);

	// Passive specific host (same result)
	addrlen = 0;
	sa = girt_config_get_sockaddr(config, "hostinfo", 1, AF_INET, NULL, NULL, &addrlen);
	cr_assert_not_null(sa, "passive specific sa is null");
    cr_assert_eq(addrlen, 16, "passive specific addrlen is %d", addrlen);
	str = girt_sockaddr_to_addrname(sa, NULL, NULL);
	cr_assert_not_null(str, "passive specific sa str is null");
    cr_expect_str_eq(str, "127.0.0.1:80", "passive specific sa str is '%s'", str);
	free(str);
	girt_config_free_sockaddr(sa);

	// Passive specific host from sub table (same result)
	addrlen = 0;
	sa = girt_config_get_sockaddr(hi, NULL, 1, AF_INET, NULL, NULL, &addrlen);
	cr_assert_not_null(sa, "passive specific sa from subtable is null");
    cr_assert_eq(addrlen, 16, "passive specific addrlen from subtable is %d", addrlen);
	str = girt_sockaddr_to_addrname(sa, NULL, NULL);
	cr_assert_not_null(str, "passive specific sa str from subtable is null");
    cr_expect_str_eq(str, "127.0.0.1:80", "passive specific sa str from subtable is '%s'", str);
	free(str);
	girt_config_free_sockaddr(sa);
	girt_config_free_table(hi);

	// Active default address
	addrlen = 0;
	sa = girt_config_get_sockaddr(config, "addrinfo", 0, AF_INET, NULL, NULL, &addrlen);
	cr_assert_not_null(sa, "active default sa is null");
    cr_assert_eq(addrlen, 16, "active default addrlen is %d", addrlen);
	str = girt_sockaddr_to_addrname(sa, NULL, NULL);
	cr_assert_not_null(str, "active default sa str is null");
    cr_expect_str_eq(str, "127.0.0.1:5678", "active default sa str is '%s'", str);
	free(str);
	girt_config_free_sockaddr(sa);

	// Passive default address
	addrlen = 0;
	sa = girt_config_get_sockaddr(config, "addrinfo", 1, AF_INET, NULL, NULL, &addrlen);
	cr_assert_not_null(sa, "passive default sa is null");
    cr_assert_eq(addrlen, 16, "passive default addrlen is %d", addrlen);
	str = girt_sockaddr_to_addrname(sa, NULL, NULL);
	cr_assert_not_null(str, "passive default sa str is null");
    cr_expect_str_eq(str, "0.0.0.0:5678", "passive default sa str is '%s'", str);
	free(str);
	girt_config_free_sockaddr(sa);

	girt_cfg *ta = girt_config_get_table(config, "thread");
	cr_assert_not_null(ta, "ta is null");
	n = girt_config_root_table_nkval(ta);
    cr_expect_eq(n, 3, "ta nkval is %d", n);
	n = girt_config_root_table_narr(ta);
    cr_expect_eq(n, 0, "ta narr is %d", n);
	n = girt_config_root_table_ntab(ta);
    cr_expect_eq(n, 0, "ta ntab is %d", n);

	// Thread attrs
	pthread_attr_t *attr = girt_config_get_pthread_attr(config, "thread");
	cr_assert_not_null(attr, "thread attr is null");
	int i = -1;
	int r = pthread_attr_getschedpolicy(attr, &i);
	cr_assert_eq(r, 0, "pthread_attr_getschedpolicy result is %d", r);
	cr_expect_eq(i, SCHED_FIFO, "thread attr scheduler is %d", i);
	struct sched_param sp[1] = {0};
	r = pthread_attr_getschedparam(attr, sp);
	cr_assert_eq(r, 0, "pthread_attr_getschedparam result is %d", r);
	cr_expect_eq(sp->sched_priority, 50, "thread attr priority is %d", sp->sched_priority);
	size_t s = 0;
	r = pthread_attr_getstacksize(attr, &s);
	cr_assert_eq(r, 0, "pthread_attr_getstacksize result is %d", r);
	cr_expect_eq(s, 0x4000, "thread attr stack size is %lu", s);
	girt_config_free_pthread_attr(attr);

	// Thread attrs from sub table (same result)
	attr = girt_config_get_pthread_attr(config, "thread");
	cr_assert_not_null(attr, "thread attr from subtable is null");
	i = -1;
	r = pthread_attr_getschedpolicy(attr, &i);
	cr_assert_eq(r, 0, "pthread_attr_getschedpolicy result is %d", r);
	cr_expect_eq(i, SCHED_FIFO, "thread attr from subtable scheduler is %d", i);
	sp->sched_priority = 0;
	r = pthread_attr_getschedparam(attr, sp);
	cr_assert_eq(r, 0, "pthread_attr_getschedparam result is %d", r);
	cr_expect_eq(sp->sched_priority, 50, "thread attr from subtable priority is %d", sp->sched_priority);
	s = 0;
	r = pthread_attr_getstacksize(attr, &s);
	cr_assert_eq(r, 0, "pthread_attr_getstacksize result is %d", r);
	cr_expect_eq(s, 0x4000, "thread attr from subtable stack size is %lu", s);
	girt_config_free_pthread_attr(attr);
	girt_config_free_table(ta);

	// Integer array
	girt_int_vec *ivec = girt_config_get_i64_array(config, "int.array");
	cr_assert_not_null(ivec, "ivec is null");
	cr_assert_eq(kv_size(*ivec), 4, "ivec size is %d", kv_size(*ivec));
	cr_expect_eq(kv_A(*ivec, 0), 10, "ivec dec is %d", kv_A(*ivec, 0));
	cr_expect_eq(kv_A(*ivec, 1), 16, "ivec hex is %d", kv_A(*ivec, 1));
	cr_expect_eq(kv_A(*ivec, 2), 8, "ivec oct is %d", kv_A(*ivec, 2));
	cr_expect_eq(kv_A(*ivec, 3), 2, "ivec bin is %d", kv_A(*ivec, 3));
	girt_config_free_i64_array(ivec);

	// String array
	girt_str_vec *svec = girt_config_get_string_array(config, "str array");
	cr_assert_not_null(svec, "svec is null");
	cr_assert_eq(kv_size(*svec), 2, "ivec size is %d", kv_size(*svec));
	cr_expect_str_eq(kv_A(*svec, 0), "Hell", "svec #0 is '%s'", kv_A(*svec, 0));
	cr_expect_str_eq(kv_A(*svec, 1), "oWorld", "svec #1 is '%s'", kv_A(*svec, 1));
	girt_config_free_string_array(svec);

	// Table array
	girt_cfg_vec *tbls = girt_config_get_table_array(config, "table-array");
	cr_assert_not_null(tbls, "tbls is null");
	cr_assert_eq(kv_size(*tbls), 3, "tbls size is %d", kv_size(*tbls));
	i64 = girt_config_get_i64(kv_A(*tbls, 0), "val", -1);
	cr_assert_eq(i64, 123, "tbls #0 val is %d", i64);
	i64 = girt_config_get_i64(kv_A(*tbls, 1), "val", -1);
	cr_assert_eq(i64, 456, "tbls #1 val is %d", i64);
	i64 = girt_config_get_i64(kv_A(*tbls, 2), "val", -1);
	cr_assert_eq(i64, 789, "tbls #2 val is %d", i64);
	girt_config_free_table_array(tbls);

	girt_config_free(config);
	girt_str_vec_free(args);
}

Test(config, override, .init = setup) {
	girt_str_vec *args = girt_str_vec_init();
	girt_str_vec_printf(args, "test");
	girt_str_vec_printf(args, "etc/values.conf");
	girt_str_vec_printf(args, "etc/conf");
	girt_root *root = &(girt_root){ .args = args };

	girt_cfg *config = girt_config_init(root);
	cr_assert_not_null(config, "config is null");

	// Default logging table and one table for values, two for the conf dir (a.conf and b.conf)
	int n = girt_config_root_table_count(config);
    cr_expect_eq(n, 4, "root table count is %d", n);

	girt_cfg *sec = girt_config_get_table(config, "section");
	cr_assert_not_null(sec, "sec is null");
	n = girt_config_root_table_nkval(sec);
	// 'val' and 'string' in each of three config files
    cr_expect_eq(n, 6, "sec nkval is %d", n);
	n = girt_config_root_table_narr(sec);
	// 'table-array' in three config files and 'array' in one
    cr_expect_eq(n, 4, "sec narr is %d", n);
	n = girt_config_root_table_ntab(sec);
	// 'table' in two config files
    cr_expect_eq(n, 2, "sec ntab is %d", n);

	int64_t i64 = girt_config_get_i64(sec, "val", -1);
	cr_expect_eq(i64, 56, "val is %d", i64);
	char *str = girt_config_get_string(sec, "string", NULL);
	cr_assert_not_null(str, "str is null");
	cr_expect_str_eq(str, "fox", "str is '%s'", str);
	free(str);

	girt_int_vec *ivec = girt_config_get_i64_array(sec, "array");
	cr_assert_not_null(ivec, "ivec is null");
	
	girt_cfg *tbl = girt_config_get_table(sec, "table");
	i64 = girt_config_get_i64(tbl, "val", -1);
	cr_assert_not_null(tbl, "tbl is null");
	cr_expect_eq(i64, 23, "tbl val is %d", i64);
	str = girt_config_get_string(tbl, "str", NULL);
	cr_assert_not_null(str, "table str is null");
	cr_expect_str_eq(str, "jumps", "str is '%s'", str);
	free(str);

	girt_cfg_vec *tbls = girt_config_get_table_array(sec, "table-array");
	cr_assert_eq(kv_size(*tbls), 3, "tbls size is %d", kv_size(*tbls));
	i64 = girt_config_get_i64(kv_A(*tbls, 0), "val", -1);
	cr_assert_eq(i64, 321, "tbls #0 val is %d", i64);
	i64 = girt_config_get_i64(kv_A(*tbls, 1), "val", -1);
	cr_assert_eq(i64, 654, "tbls #1 val is %d", i64);
	i64 = girt_config_get_i64(kv_A(*tbls, 2), "val", -1);
	cr_assert_eq(i64, 987, "tbls #2 val is %d", i64);
	girt_config_free_table_array(tbls);

	girt_config_free_table(sec);

	girt_config_free(config);
	girt_str_vec_free(args);
}
